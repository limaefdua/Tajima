package id.tajima.tajima.intro.register;

import android.content.Context;
import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.intro.register.model.RegisterResponse;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class RegisterPresenter implements RegisterView.Presenter {
    private Context context;
    private RegisterView.View view;
    private Api _api;
    private Subscription register;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public RegisterPresenter(Context context, RegisterView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
    }

    @Override
    public void doRegister(String username, String password) {
        register = _api.register(username, password).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<RegisterResponse>() {
                    @Override
                    public void onCompleted() {
                        if (register.isUnsubscribed()) {
                            register.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(RegisterResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onRegisterSuccess(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(register);
    }
}
