package id.tajima.tajima.intro.login;

import id.tajima.tajima.intro.login.model.Data;

public interface LoginView {
    interface View {
        void onSuccessLogin(Data data);

        void onFailedLogin(String message);
    }

    interface Presenter {
        void doLogin(String username, String password);

        void setToken(String token);
    }
}
