package id.tajima.tajima.intro.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pedro.library.AutoPermissions;

import id.tajima.tajima.R;
import id.tajima.tajima.dashboard.MainActivity;
import id.tajima.tajima.intro.login.model.Data;
import id.tajima.tajima.utility.AppPreferences;

public class LoginActivity extends AppCompatActivity implements LoginView.View, View.OnClickListener {
    LoginView.Presenter presenter;
    AppPreferences preferences;
    EditText etUsername, etPassword;
    Button btnLogin;
    RelativeLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AutoPermissions.Companion.loadAllPermissions(this, 1);
        presenter = new LoginPresenter(this, this);
        preferences = new AppPreferences(this);
        _initView();
    }

    private void _initView() {
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        progressBar = findViewById(R.id.progressbarLayout);
    }

    @Override
    public void onSuccessLogin(Data data) {
        preferences.setIsUserLogin(true);
        preferences.setUserToken(data.getToken());
        preferences.setUserId(data.getUser().getUserId());
        preferences.setUserEmail(data.getUser().getUserEmail());
        preferences.setUserFullName(data.getUser().getProfileFirstName() + " " + data.getUser().getProfileLastName());
        preferences.setUserRole(data.getUser().getUserType());
        preferences.setRoleId(data.getUser().getUserJabatanId());
        goToDashboard(data.getUser().getUserType());
    }

    @Override
    public void onFailedLogin(String message) {
        btnLogin.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (preferences.isUserLogin()) {
            goToDashboard(preferences.getUserRole());
            getToken();
        }
    }

    public void getToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("getInstanceId failed", task.getException());
                            return;
                        }
                        String token = task.getResult().getToken();
                        Log.w("new token", token);
                        presenter.setToken(token);
                    }
                });

    }

    private void goToDashboard(String userType) {
        Intent intent= new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                progressBar.setVisibility(View.VISIBLE);
                btnLogin.setEnabled(false);
                presenter.doLogin(
                        etUsername.getText().toString(),
                        etPassword.getText().toString()
                );
                break;
        }
    }
}
