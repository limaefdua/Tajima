package id.tajima.tajima.intro.login;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.intro.login.model.LoginResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class LoginPresenter implements LoginView.Presenter {
    private LoginView.View view;
    private Api _api;
    private Subscription login, st;
    private AppPreferences preferences;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    LoginPresenter(Context context, LoginView.View view) {
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void doLogin(String username, String password) {
        login = _api.login(username, password).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        if (login.isUnsubscribed()) {
                            login.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onFailedLogin("Something wrong!");
                    }

                    @Override
                    public void onNext(LoginResponse data) {
                        Log.w("asda", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            view.onSuccessLogin(data.getData());
                        } else {
                            view.onFailedLogin(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(login);
    }

    @Override
    public void setToken(String token) {
        st = _api.setToken(preferences.getUserToken(), preferences.getUserId(), token).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        if (st.isUnsubscribed()) {
                            st.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onFailedLogin(e.getMessage());
                    }

                    @Override
                    public void onNext(LoginResponse data) {
                        Log.w("sada", "success change token");
                    }
                });
        compositeSubscription.add(st);
    }
}
