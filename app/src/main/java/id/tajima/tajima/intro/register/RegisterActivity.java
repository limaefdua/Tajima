package id.tajima.tajima.intro.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.intro.login.LoginActivity;
import id.tajima.tajima.intro.register.model.Data;

public class RegisterActivity extends AppCompatActivity implements RegisterView.View, View.OnClickListener {
    RegisterView.Presenter presenter;
    Button btnRegister;
    EditText etUsername, etPassword;
    RelativeLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this, this);
        _initView();
    }

    private void _initView() {
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnRegister = findViewById(R.id.btnRegister);
        progressBar = findViewById(R.id.progressbarLayout);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onRegisterSuccess(Data data) {
        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                progressBar.setVisibility(View.VISIBLE);
                presenter.doRegister(
                        etUsername.getText().toString(),
                        etPassword.getText().toString()
                );
                break;
        }
    }
}
