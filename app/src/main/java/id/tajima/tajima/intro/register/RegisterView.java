package id.tajima.tajima.intro.register;

import id.tajima.tajima.intro.register.model.Data;

public interface RegisterView {
    interface View{
        void onRegisterSuccess(Data data);
        void onError(String message);
    }

    interface Presenter{
        void doRegister(String username,String password);
    }
}
