package id.tajima.tajima.intro.login.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_cabang_id")
    @Expose
    private String userCabangId;
    @SerializedName("user_jabatan_id")
    @Expose
    private String userJabatanId;
    @SerializedName("user_username")
    @Expose
    private String userUsername;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user_token")
    @Expose
    private String userToken;
    @SerializedName("user_last_login_at")
    @Expose
    private String userLastLoginAt;
    @SerializedName("user_created_by")
    @Expose
    private String userCreatedBy;
    @SerializedName("user_created_at")
    @Expose
    private String userCreatedAt;
    @SerializedName("user_updated_by")
    @Expose
    private String userUpdatedBy;
    @SerializedName("user_updated_at")
    @Expose
    private String userUpdatedAt;
    @SerializedName("user_status_delete")
    @Expose
    private String userStatusDelete;
    @SerializedName("user_deleted_by")
    @Expose
    private Object userDeletedBy;
    @SerializedName("user_deleted_at")
    @Expose
    private Object userDeletedAt;
    @SerializedName("user_restored_by")
    @Expose
    private Object userRestoredBy;
    @SerializedName("user_restored_at")
    @Expose
    private Object userRestoredAt;
    @SerializedName("cabang_id")
    @Expose
    private String cabangId;
    @SerializedName("cabang_code")
    @Expose
    private String cabangCode;
    @SerializedName("cabang_name")
    @Expose
    private String cabangName;
    @SerializedName("cabang_desc")
    @Expose
    private String cabangDesc;
    @SerializedName("cabang_address")
    @Expose
    private String cabangAddress;
    @SerializedName("cabang_lat")
    @Expose
    private Object cabangLat;
    @SerializedName("cabang_long")
    @Expose
    private Object cabangLong;
    @SerializedName("cabang_status")
    @Expose
    private String cabangStatus;
    @SerializedName("cabang_created_by")
    @Expose
    private String cabangCreatedBy;
    @SerializedName("cabang_created_at")
    @Expose
    private String cabangCreatedAt;
    @SerializedName("cabang_updated_by")
    @Expose
    private String cabangUpdatedBy;
    @SerializedName("cabang_updated_at")
    @Expose
    private String cabangUpdatedAt;
    @SerializedName("cabang_status_delete")
    @Expose
    private String cabangStatusDelete;
    @SerializedName("cabang_deleted_by")
    @Expose
    private Object cabangDeletedBy;
    @SerializedName("cabang_deleted_at")
    @Expose
    private Object cabangDeletedAt;
    @SerializedName("cabang_restored_by")
    @Expose
    private Object cabangRestoredBy;
    @SerializedName("cabang_restored_at")
    @Expose
    private Object cabangRestoredAt;
    @SerializedName("profile_id")
    @Expose
    private String profileId;
    @SerializedName("profile_user_id")
    @Expose
    private String profileUserId;
    @SerializedName("profile_nik")
    @Expose
    private Object profileNik;
    @SerializedName("profile_first_name")
    @Expose
    private String profileFirstName;
    @SerializedName("profile_last_name")
    @Expose
    private String profileLastName;
    @SerializedName("profile_jenis_kelamin")
    @Expose
    private Object profileJenisKelamin;
    @SerializedName("profile_telp")
    @Expose
    private Object profileTelp;
    @SerializedName("profile_address")
    @Expose
    private String profileAddress;
    @SerializedName("profile_tempat_lahir")
    @Expose
    private Object profileTempatLahir;
    @SerializedName("profile_dob")
    @Expose
    private Object profileDob;
    @SerializedName("profile_sim")
    @Expose
    private Object profileSim;
    @SerializedName("profile_foto")
    @Expose
    private Object profileFoto;
    @SerializedName("profile_masuk_at")
    @Expose
    private Object profileMasukAt;
    @SerializedName("profile_created_by")
    @Expose
    private Object profileCreatedBy;
    @SerializedName("profile_created_at")
    @Expose
    private Object profileCreatedAt;
    @SerializedName("profile_updated_by")
    @Expose
    private Object profileUpdatedBy;
    @SerializedName("profile_updated_at")
    @Expose
    private Object profileUpdatedAt;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserCabangId() {
        return userCabangId;
    }

    public void setUserCabangId(String userCabangId) {
        this.userCabangId = userCabangId;
    }

    public String getUserJabatanId() {
        return userJabatanId;
    }

    public void setUserJabatanId(String userJabatanId) {
        this.userJabatanId = userJabatanId;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getUserLastLoginAt() {
        return userLastLoginAt;
    }

    public void setUserLastLoginAt(String userLastLoginAt) {
        this.userLastLoginAt = userLastLoginAt;
    }

    public String getUserCreatedBy() {
        return userCreatedBy;
    }

    public void setUserCreatedBy(String userCreatedBy) {
        this.userCreatedBy = userCreatedBy;
    }

    public String getUserCreatedAt() {
        return userCreatedAt;
    }

    public void setUserCreatedAt(String userCreatedAt) {
        this.userCreatedAt = userCreatedAt;
    }

    public String getUserUpdatedBy() {
        return userUpdatedBy;
    }

    public void setUserUpdatedBy(String userUpdatedBy) {
        this.userUpdatedBy = userUpdatedBy;
    }

    public String getUserUpdatedAt() {
        return userUpdatedAt;
    }

    public void setUserUpdatedAt(String userUpdatedAt) {
        this.userUpdatedAt = userUpdatedAt;
    }

    public String getUserStatusDelete() {
        return userStatusDelete;
    }

    public void setUserStatusDelete(String userStatusDelete) {
        this.userStatusDelete = userStatusDelete;
    }

    public Object getUserDeletedBy() {
        return userDeletedBy;
    }

    public void setUserDeletedBy(Object userDeletedBy) {
        this.userDeletedBy = userDeletedBy;
    }

    public Object getUserDeletedAt() {
        return userDeletedAt;
    }

    public void setUserDeletedAt(Object userDeletedAt) {
        this.userDeletedAt = userDeletedAt;
    }

    public Object getUserRestoredBy() {
        return userRestoredBy;
    }

    public void setUserRestoredBy(Object userRestoredBy) {
        this.userRestoredBy = userRestoredBy;
    }

    public Object getUserRestoredAt() {
        return userRestoredAt;
    }

    public void setUserRestoredAt(Object userRestoredAt) {
        this.userRestoredAt = userRestoredAt;
    }

    public String getCabangId() {
        return cabangId;
    }

    public void setCabangId(String cabangId) {
        this.cabangId = cabangId;
    }

    public String getCabangCode() {
        return cabangCode;
    }

    public void setCabangCode(String cabangCode) {
        this.cabangCode = cabangCode;
    }

    public String getCabangName() {
        return cabangName;
    }

    public void setCabangName(String cabangName) {
        this.cabangName = cabangName;
    }

    public String getCabangDesc() {
        return cabangDesc;
    }

    public void setCabangDesc(String cabangDesc) {
        this.cabangDesc = cabangDesc;
    }

    public String getCabangAddress() {
        return cabangAddress;
    }

    public void setCabangAddress(String cabangAddress) {
        this.cabangAddress = cabangAddress;
    }

    public Object getCabangLat() {
        return cabangLat;
    }

    public void setCabangLat(Object cabangLat) {
        this.cabangLat = cabangLat;
    }

    public Object getCabangLong() {
        return cabangLong;
    }

    public void setCabangLong(Object cabangLong) {
        this.cabangLong = cabangLong;
    }

    public String getCabangStatus() {
        return cabangStatus;
    }

    public void setCabangStatus(String cabangStatus) {
        this.cabangStatus = cabangStatus;
    }

    public String getCabangCreatedBy() {
        return cabangCreatedBy;
    }

    public void setCabangCreatedBy(String cabangCreatedBy) {
        this.cabangCreatedBy = cabangCreatedBy;
    }

    public String getCabangCreatedAt() {
        return cabangCreatedAt;
    }

    public void setCabangCreatedAt(String cabangCreatedAt) {
        this.cabangCreatedAt = cabangCreatedAt;
    }

    public String getCabangUpdatedBy() {
        return cabangUpdatedBy;
    }

    public void setCabangUpdatedBy(String cabangUpdatedBy) {
        this.cabangUpdatedBy = cabangUpdatedBy;
    }

    public String getCabangUpdatedAt() {
        return cabangUpdatedAt;
    }

    public void setCabangUpdatedAt(String cabangUpdatedAt) {
        this.cabangUpdatedAt = cabangUpdatedAt;
    }

    public String getCabangStatusDelete() {
        return cabangStatusDelete;
    }

    public void setCabangStatusDelete(String cabangStatusDelete) {
        this.cabangStatusDelete = cabangStatusDelete;
    }

    public Object getCabangDeletedBy() {
        return cabangDeletedBy;
    }

    public void setCabangDeletedBy(Object cabangDeletedBy) {
        this.cabangDeletedBy = cabangDeletedBy;
    }

    public Object getCabangDeletedAt() {
        return cabangDeletedAt;
    }

    public void setCabangDeletedAt(Object cabangDeletedAt) {
        this.cabangDeletedAt = cabangDeletedAt;
    }

    public Object getCabangRestoredBy() {
        return cabangRestoredBy;
    }

    public void setCabangRestoredBy(Object cabangRestoredBy) {
        this.cabangRestoredBy = cabangRestoredBy;
    }

    public Object getCabangRestoredAt() {
        return cabangRestoredAt;
    }

    public void setCabangRestoredAt(Object cabangRestoredAt) {
        this.cabangRestoredAt = cabangRestoredAt;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getProfileUserId() {
        return profileUserId;
    }

    public void setProfileUserId(String profileUserId) {
        this.profileUserId = profileUserId;
    }

    public Object getProfileNik() {
        return profileNik;
    }

    public void setProfileNik(Object profileNik) {
        this.profileNik = profileNik;
    }

    public String getProfileFirstName() {
        return profileFirstName;
    }

    public void setProfileFirstName(String profileFirstName) {
        this.profileFirstName = profileFirstName;
    }

    public String getProfileLastName() {
        return profileLastName;
    }

    public void setProfileLastName(String profileLastName) {
        this.profileLastName = profileLastName;
    }

    public Object getProfileJenisKelamin() {
        return profileJenisKelamin;
    }

    public void setProfileJenisKelamin(Object profileJenisKelamin) {
        this.profileJenisKelamin = profileJenisKelamin;
    }

    public Object getProfileTelp() {
        return profileTelp;
    }

    public void setProfileTelp(Object profileTelp) {
        this.profileTelp = profileTelp;
    }

    public String getProfileAddress() {
        return profileAddress;
    }

    public void setProfileAddress(String profileAddress) {
        this.profileAddress = profileAddress;
    }

    public Object getProfileTempatLahir() {
        return profileTempatLahir;
    }

    public void setProfileTempatLahir(Object profileTempatLahir) {
        this.profileTempatLahir = profileTempatLahir;
    }

    public Object getProfileDob() {
        return profileDob;
    }

    public void setProfileDob(Object profileDob) {
        this.profileDob = profileDob;
    }

    public Object getProfileSim() {
        return profileSim;
    }

    public void setProfileSim(Object profileSim) {
        this.profileSim = profileSim;
    }

    public Object getProfileFoto() {
        return profileFoto;
    }

    public void setProfileFoto(Object profileFoto) {
        this.profileFoto = profileFoto;
    }

    public Object getProfileMasukAt() {
        return profileMasukAt;
    }

    public void setProfileMasukAt(Object profileMasukAt) {
        this.profileMasukAt = profileMasukAt;
    }

    public Object getProfileCreatedBy() {
        return profileCreatedBy;
    }

    public void setProfileCreatedBy(Object profileCreatedBy) {
        this.profileCreatedBy = profileCreatedBy;
    }

    public Object getProfileCreatedAt() {
        return profileCreatedAt;
    }

    public void setProfileCreatedAt(Object profileCreatedAt) {
        this.profileCreatedAt = profileCreatedAt;
    }

    public Object getProfileUpdatedBy() {
        return profileUpdatedBy;
    }

    public void setProfileUpdatedBy(Object profileUpdatedBy) {
        this.profileUpdatedBy = profileUpdatedBy;
    }

    public Object getProfileUpdatedAt() {
        return profileUpdatedAt;
    }

    public void setProfileUpdatedAt(Object profileUpdatedAt) {
        this.profileUpdatedAt = profileUpdatedAt;
    }
}
