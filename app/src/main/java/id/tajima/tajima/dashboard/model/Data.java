package id.tajima.tajima.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("notif")
    @Expose
    private Integer notif;

    public Integer getNotif() {
        return notif;
    }
}
