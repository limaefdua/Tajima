package id.tajima.tajima.dashboard;

interface MainView {
    interface View{
        void notificationCount(int count);
        void onError(String message);
    }

    interface  Presenter{
        void getNotificationCount(String userId);
    }
}
