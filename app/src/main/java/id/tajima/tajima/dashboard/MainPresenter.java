package id.tajima.tajima.dashboard;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.dashboard.model.MainResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MainPresenter implements MainView.Presenter {
    Context context;
    MainView.View view;
    Api api;
    CompositeSubscription cs = new CompositeSubscription();
    Subscription count;
    AppPreferences preferences;

    public MainPresenter(Context context, MainView.View view) {
        this.context = context;
        this.view = view;
        preferences = new AppPreferences(context);
        api = TajimaApp.createService(Api.class);
    }

    @Override
    public void getNotificationCount(String userId) {
        count = api.countNotification(preferences.getUserToken(), userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MainResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(MainResponse response) {
                        if (response.getStatus() == 200) {
                            view.notificationCount(response.getData().getNotif());
                        } else {
                            view.onError(response.getMessage());
                        }
                    }
                });
        cs.add(count);
    }
}
