package id.tajima.tajima.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.absent.AbsentFragment;
import id.tajima.tajima.intro.login.LoginActivity;
import id.tajima.tajima.notif.NotificationActivity;
import id.tajima.tajima.menu.parts.BarangFragment;
import id.tajima.tajima.menu.presensi.PresenceFragment;
import id.tajima.tajima.menu.service.ServiceFragment;
import id.tajima.tajima.utility.AppPreferences;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, MainView.View {
    DrawerLayout drawer;
    ImageButton menu_navigation;
    LinearLayout layoutHeader;
    TextView tvTitle, tvSubtitle, badgeText, tvUsername, tvRole;
    AppPreferences preferences;
    NavigationView navigationView;
    FragmentManager fm;
    String title = "Tajima";
    ImageButton menu_notification;
    MainView.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = new AppPreferences(this);
        fm = getSupportFragmentManager();
        presenter = new MainPresenter(this, this);
        initView();
    }

    private void initView() {
        menu_navigation = findViewById(R.id.menu_navigation);
        menu_navigation.setOnClickListener(this);
        layoutHeader = findViewById(R.id.layoutHeader);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        menu_notification = findViewById(R.id.menu_notification);
        menu_notification.setOnClickListener(this);

        tvTitle = findViewById(R.id.tvTitle);
        tvSubtitle = findViewById(R.id.tvSubTitle);
        badgeText = findViewById(R.id.badgeText);
        tvUsername = findViewById(R.id.tvUsername);
        tvRole = findViewById(R.id.tvRole);

        drawer = findViewById(R.id.menu_drawer);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                layoutHeader.setX(slideOffset * layoutHeader.getWidth() / 1.22f);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });
        tvSubtitle.setText(preferences.getUserFullName());
        tvTitle.setText(title);
        menuManagement();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getNotificationCount(preferences.getUserId());
    }

    private void menuManagement() {
        tvUsername.setText(preferences.getUserEmail());
        tvRole.setText(preferences.getUserRole());
        fm.beginTransaction().replace(R.id.frame_main, new PresenceFragment()).commit();
        switch (preferences.getUserRole()) {
            case "admin":
                navigationView.getMenu().findItem(R.id.menu_sparepart).setVisible(true);
                navigationView.getMenu().findItem(R.id.menu_services).setVisible(true);
                break;
            case "part_staff":
                navigationView.getMenu().findItem(R.id.menu_sparepart).setVisible(true);
                break;
            case "teknisi":
                navigationView.getMenu().findItem(R.id.menu_services).setVisible(true);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_navigation:
                drawer.openDrawer(Gravity.START);
                break;
            case R.id.menu_notification:
                startActivity(new Intent(this, NotificationActivity.class));
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.menu_presence:
                title = "Presence";
                fragment = new PresenceFragment();
                break;
            case R.id.menu_absent:
                title = "Absence";
                fragment = new AbsentFragment();
                break;
            case R.id.menu_sparepart:
                title = "Sparepart";
                fragment = new BarangFragment();
                break;
            case R.id.menu_services:
                title = "Services";
                fragment = new ServiceFragment();
                break;
            case R.id.menu_out:
                finish();
                preferences.setIsUserLogin(false);
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
        if (fragment != null) {
            fm.beginTransaction().replace(R.id.frame_main, fragment).commit();
        }
        tvTitle.setText(title);
        drawer.closeDrawers();
        return true;
    }

    @Override
    public void notificationCount(int count) {
        if (count > 0) {
            badgeText.setVisibility(View.VISIBLE);
            badgeText.setText(String.valueOf(count));
        } else {
            badgeText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
