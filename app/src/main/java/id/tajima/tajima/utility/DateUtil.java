package id.tajima.tajima.utility;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Maftuhin on 01/11/2017.
 */

public class DateUtil {

    private final static Calendar publicCalendar = Calendar.getInstance();
    private static String myFormat = "dd/MM/yyyy";
    private static SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ROOT);

    private static String defFormat = "yyyy-MM-dd";
    private static SimpleDateFormat defSdf = new SimpleDateFormat(defFormat, Locale.ROOT);

    public static String getStringTimeNow() {
        return sdf.format(publicCalendar.getTime());
    }

    public static String timeDefault() {
        return defSdf.format(publicCalendar.getTime());
    }

    public static String dateTime() {
        String a = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat b = new SimpleDateFormat(a, Locale.ROOT);
        return b.format(publicCalendar.getTime());
    }

    public static DatePickerDialog.OnDateSetListener datePickerLabel(final EditText et) {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                publicCalendar.set(Calendar.YEAR, year);
                publicCalendar.set(Calendar.MONTH, monthOfYear);
                publicCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateDateLabel(et);
                defUpdateLabel(et);
            }
        };
    }

    public static void datePickerShow(Context c, String date, DatePickerDialog.OnDateSetListener dateListener) {
        try {
            Calendar cal = Calendar.getInstance();
            Date dat = sdf.parse(date);
            cal.setTime(dat);
            new DatePickerDialog(c, dateListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                    .show();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void defDatePickerShow(Context c, String date, DatePickerDialog.OnDateSetListener dateListener) {
        try {
            Calendar cal = Calendar.getInstance();
            Date dat = defSdf.parse(date);
            cal.setTime(dat);
            new DatePickerDialog(c, dateListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH))
                    .show();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void updateDateLabel(EditText et) {
        et.setText(sdf.format(publicCalendar.getTime()));
    }

    private static void defUpdateLabel(EditText et) {
        et.setText(defSdf.format(publicCalendar.getTime()));
    }

    public static String change_format(String dateInString, String ff, String cf) {
        SimpleDateFormat formatter, result = null;
        Date time = null;
        String tgl = "";
        try {
            formatter = new SimpleDateFormat(ff, Locale.getDefault());
            time = formatter.parse(dateInString);
            result = new SimpleDateFormat(cf, Locale.getDefault());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tgl = result.format(time);
        return tgl;
    }

    public static String getTime(String dateInString) {
        SimpleDateFormat formatter, result = null;
        Date time = null;
        String tgl;
        if (dateInString == null) {
            return "";
        } else {
            try {
                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                time = formatter.parse(dateInString);
                result = new SimpleDateFormat("HH:mm", Locale.getDefault());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tgl = result.format(time);
            return tgl;
        }
    }

    public static String getDate(String dateInString) {
        SimpleDateFormat formatter, result = null;
        Date time = null;
        String tgl;
        if (dateInString == null) {
            return "";
        } else {
            try {
                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                time = formatter.parse(dateInString);
                result = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tgl = result.format(time);
            return tgl;
        }
    }
}
