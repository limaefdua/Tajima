package id.tajima.tajima.utility;

public class General {
    public static int progressAt(int code) {
        int progressAt = 0;
        switch (code) {
            case 6:
                progressAt = 0;
                break;
            case 8:
                progressAt = 1;
                break;
            case 9:
                progressAt = 2;
                break;
            case 10:
                progressAt = 3;
                break;
            case 3:
                progressAt = 4;
                break;
            case 4:
                progressAt = 5;
                break;
            case 17:
                progressAt = 6;
                break;
            case 11:
                progressAt = 7;
                break;
            case 12:
                progressAt = 8;
                break;
            default:
                break;
        }
        return progressAt;
    }
}
