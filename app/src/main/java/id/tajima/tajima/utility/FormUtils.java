package id.tajima.tajima.utility;

import java.util.Locale;

public class FormUtils {

    public static String toRpString(int value) {
        return "Rp " + String.format(Locale.US, "%,d", Long.parseLong(String.valueOf(value)));
    }

    public static String toRpString(String string) {
        String text = string.replace(" ", "");
        if (text.equals("")) {
            return "Rp. 0";
        }
        return "Rp " + String.format(Locale.US, "%,d", Long.parseLong(text));
    }
}

