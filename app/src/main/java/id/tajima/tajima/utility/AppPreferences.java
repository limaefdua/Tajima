package id.tajima.tajima.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by maftuhin on 15/04/18. 18:37
 */
public class AppPreferences {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static final String PREFERENCES_NAME = "tajima";
    private static final String IS_USER_LOGIN = "isUserLogin";
    private static final String USER_TOKEN = "userToken";
    private static final String USER_ID = "userId";
    private static final String USER_EMAIL = "userEmail";
    private static final String USER_FULL_NAME = "userFullName";
    private static final String USER_ROLE = "userRole";
    private static final String ROLE_ID = "roleId";

    public AppPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void clearPreferences() {
        sharedPreferences.edit().clear().apply();
        editor.clear().commit();
    }

    /**
     * set user login or not
     *
     * @param isUserLogin
     */
    public void setIsUserLogin(Boolean isUserLogin) {
        editor.putBoolean(IS_USER_LOGIN, isUserLogin);
        editor.commit();
    }

    public void setUserToken(String token) {
        editor.putString(USER_TOKEN, token);
        editor.commit();
    }

    public void setUserId(String token) {
        editor.putString(USER_ID, token);
        editor.commit();
    }

    public void setUserEmail(String email) {
        editor.putString(USER_EMAIL, email);
        editor.commit();
    }

    public void setUserFullName(String fullName) {
        editor.putString(USER_FULL_NAME, fullName);
        editor.commit();
    }

    public void setUserRole(String userRole) {
        editor.putString(USER_ROLE, userRole);
        editor.commit();
    }

    public void setRoleId(String roleId) {
        editor.putString(ROLE_ID, roleId);
        editor.commit();
    }

    /**
     * get value user login or not
     *
     * @return
     */
    public Boolean isUserLogin() {
        return sharedPreferences.getBoolean(IS_USER_LOGIN, false);
    }

    public String getUserToken() {
        return sharedPreferences.getString(USER_TOKEN, null);
    }

    public String getUserId() {
        return sharedPreferences.getString(USER_ID, null);
    }

    public String getUserEmail() {
        return sharedPreferences.getString(USER_EMAIL, "");
    }

    public String getUserFullName() {
        return sharedPreferences.getString(USER_FULL_NAME, "");
    }

    public String getUserRole() {
        return sharedPreferences.getString(USER_ROLE, "tech");
    }

    public String getRoleId() {
        return sharedPreferences.getString(ROLE_ID, "3");
    }
}
