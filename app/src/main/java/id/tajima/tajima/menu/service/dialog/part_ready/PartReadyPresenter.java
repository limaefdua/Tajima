package id.tajima.tajima.menu.service.dialog.part_ready;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PartReadyPresenter implements PartReadyView.Presenter {
    private Context context;
    private PartReadyView.View view;
    private Api _api;
    private AppPreferences preferences;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Subscription partReady;

    PartReadyPresenter(Context context, PartReadyView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void partReady(String serviceId, Boolean isReady, String message) {
        partReady = _api.accPartIsReady(preferences.getUserToken(), preferences.getUserId(), serviceId, isReady, message)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (partReady.isUnsubscribed()) {
                            partReady.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onPartReady();
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(partReady);
    }
}
