package id.tajima.tajima.menu.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyDuration {
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("daily_duration")
    @Expose
    private String dailyDuration;

    public String getTotal() {
        return total;
    }

    public String getDailyDuration() {
        return dailyDuration;
    }

    public void setDailyDuration(String dailyDuration) {
        this.dailyDuration = dailyDuration;
    }
}
