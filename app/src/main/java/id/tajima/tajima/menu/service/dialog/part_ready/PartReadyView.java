package id.tajima.tajima.menu.service.dialog.part_ready;

public interface PartReadyView {
    interface View {
        void onPartReady();

        void onError(String message);
    }

    interface Presenter {
        void partReady(String serviceId, Boolean isReady, String message);
    }
}
