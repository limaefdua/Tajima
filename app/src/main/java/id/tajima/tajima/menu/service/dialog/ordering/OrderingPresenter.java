package id.tajima.tajima.menu.service.dialog.ordering;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class OrderingPresenter implements OrderingView.Presenter {
    private Context context;
    private OrderingView.View view;
    private Api _api;
    private CompositeSubscription cs = new CompositeSubscription();
    private Subscription order;
    private AppPreferences preferences;

    OrderingPresenter(Context context, OrderingView.View view) {
        this.context = context;
        this.view = view;
        preferences = new AppPreferences(context);
        _api = TajimaApp.createService(Api.class);
    }

    @Override
    public void orderPart(String serviceId, String ordering) {
        view.onLoading();
        order = _api.orderPart(preferences.getUserToken(), preferences.getUserId(), serviceId, ordering)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (order.isUnsubscribed()) {
                            order.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.doneLoading();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        view.doneLoading();
                        if (data.getStatus() == 200) {
                            if (data.getData().getSuccessMessage()) {
                                view.onSuccess(data.getData());
                            } else {
                                view.onError(data.getMessage());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        cs.add(order);
    }
}
