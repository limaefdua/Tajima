package id.tajima.tajima.menu.service.dialog.finish;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.custom.ClearEdit;
import id.tajima.tajima.menu.service.dialog.depart.DriverAdapter;
import id.tajima.tajima.menu.service.dialog.depart.DriverModel;
import id.tajima.tajima.menu.service.step.model.LogStep;
import id.tajima.tajima.utility.GpsUtils;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class FinishDialog extends Dialog implements View.OnClickListener, FinishView.View {
    private Context context;
    private RecyclerView rvDriver;
    private DriverAdapter adapter;
    private Button btnCancel, btnSubmit;
    private RadioGroup radioGroup;
    private RadioButton radioCar, radioMotor, radioOther;
    private LinearLayout lytCar, lytOther, lytFooter;
    private ClearEdit ceOther, ceCar;
    private OnMyDialogResult mDialogResult;
    private EditText clear_edit;
    private String driverId = "";
    private String serviceId;
    private FinishView.Presenter presenter;

    public FinishDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_finish);
        adapter = new DriverAdapter(context);
        initView();
        presenter = new FinishPresenter(context, this);
    }

    private void initView() {
        final int lightGreen = Color.parseColor("#8bc34a");
        btnCancel = findViewById(R.id.btnRefuse);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        rvDriver = findViewById(R.id.rvDriver);
        rvDriver.setLayoutManager(new LinearLayoutManager(context));
        rvDriver.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        radioGroup = findViewById(R.id.radioGroupDriver);
        lytCar = findViewById(R.id.lytCar);
        lytOther = findViewById(R.id.lytOther);
        lytFooter = findViewById(R.id.lytFooter);
        radioCar = findViewById(R.id.radioCar);
        radioMotor = findViewById(R.id.radioMotor);
        radioOther = findViewById(R.id.radioOther);
        ceOther = findViewById(R.id.ceOther);
        ceCar = findViewById(R.id.ceCar);
        ceOther.setButtonVisibility(false);
        ceCar.setButtonVisibility(false);
        ceCar.setHintText("Other");
        ceOther.setHintText("Other");
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()) {
                    case R.id.radioCar:
                        lytCar.setVisibility(View.VISIBLE);
                        lytOther.setVisibility(View.GONE);
                        lytFooter.setVisibility(View.VISIBLE);
                        changeTextColor(Color.WHITE, lightGreen, lightGreen);
                        break;
                    case R.id.radioMotor:
                        lytFooter.setVisibility(View.VISIBLE);
                        lytCar.setVisibility(View.GONE);
                        lytOther.setVisibility(View.GONE);
                        changeTextColor(lightGreen, Color.WHITE, lightGreen);
                        break;
                    case R.id.radioOther:
                        lytOther.setVisibility(View.VISIBLE);
                        lytCar.setVisibility(View.GONE);
                        lytFooter.setVisibility(View.VISIBLE);
                        changeTextColor(lightGreen, lightGreen, Color.WHITE);
                        break;
                }
            }
        });
        radioGroup.check(R.id.radioCar);
        clear_edit = findViewById(R.id.clear_edit);
        clear_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    rvDriver.setVisibility(View.GONE);
                } else {
                    rvDriver.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeTextColor(int color1, int color2, int color3) {
        radioCar.setTextColor(color1);
        radioMotor.setTextColor(color2);
        radioOther.setTextColor(color3);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRefuse:
                dismiss();
                mDialogResult.onFinish("no", "");
                break;
            case R.id.btnSubmit:
                submitDepart();
                break;
        }
    }

    private void submitDepart() {
        Boolean gps = GpsUtils.checkGpsVisibility(context);
        if (gps) {
            RadioButton rb = findViewById(radioGroup.getCheckedRadioButtonId());
            if (rb.getText().toString().equals("Car")) {
                if (driverId.equals("") && ceCar.getText().toString().equals("")) {
                    Toast.makeText(context, "Please select driver first, or type manually", Toast.LENGTH_SHORT).show();
                } else {
                    presenter.finishStep(serviceId, rb.getText().toString().toLowerCase(), driverId);
                }
            } else {
                presenter.finishStep(serviceId, rb.getText().toString().toLowerCase(), "");
            }
        } else {
            Toast.makeText(context, "Please Turn On Your GPS", Toast.LENGTH_LONG).show();
        }
    }

    public void setFinishResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDriverExist(List<DriverModel> driver) {
        adapter.setDriverData(driver);
        rvDriver.setAdapter(adapter);
        adapter.setDialogResult(new DriverAdapter.OnMyDialogResult() {
            @Override
            public void setDriverId(String idDriver) {
                driverId = idDriver;
            }
        });
    }

    @Override
    public void onFinishSuccess(LogStep logStep) {
        mDialogResult.onFinish("yes", logStep.getLogstep6());
        dismiss();
    }

    public interface OnMyDialogResult {
        void onFinish(String result, String logStep);
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
        presenter.getDriver(serviceId);
    }
}
