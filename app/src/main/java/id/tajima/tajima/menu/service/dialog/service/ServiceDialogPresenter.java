package id.tajima.tajima.menu.service.dialog.service;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ServiceDialogPresenter implements ServiceDialogView.Presenter {
    private Context context;
    private ServiceDialogView.View view;
    private Api _api;
    private AppPreferences preferences;
    private Subscription serviceComplete, serviceInComplete, listMcn, amount;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    ServiceDialogPresenter(Context context, ServiceDialogView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void listMcn() {
        listMcn = _api.listMcn(preferences.getUserToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listMcn.isUnsubscribed()) {
                            listMcn.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onMcnExist(data.getData().getMcnData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(listMcn);
    }

    @Override
    public void getServiceAmount(String serviceId) {
        amount = _api.getServiceAmount(preferences.getUserToken(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (amount.isUnsubscribed()) {
                            amount.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse response) {
                        if (response.getStatus() == 200) {
                            view.onAmountExist(response.getData().getServiceAmount());
                        } else {
                            view.onError("Can't get service amount");
                        }
                    }
                });
        compositeSubscription.add(amount);
    }

    @Override
    public void serviceIncomplete(String serviceId, String reason, String continue_at) {
        serviceInComplete = _api.serviceIncomplete(preferences.getUserToken(), preferences.getUserId(), serviceId, false, reason, continue_at)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (serviceInComplete.isUnsubscribed()) {
                            serviceInComplete.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            if (data.getData().getSuccessMessage()) {
                                view.onServiceInComplete(data.getData().getLogStep());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(serviceInComplete);
    }

    @Override
    public void serviceComplete(String serviceId, String garansi, boolean foc, String total_charge, String payment_method) {
        serviceComplete = _api.servicingComplete(preferences.getUserToken(), preferences.getUserId(),
                serviceId, true, garansi, foc, total_charge, payment_method)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (serviceComplete.isUnsubscribed()) {
                            serviceComplete.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            if (data.getData().getSuccessMessage()) {
                                view.onServiceComplete(data.getData().getLogStep());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(serviceComplete);
    }
}
