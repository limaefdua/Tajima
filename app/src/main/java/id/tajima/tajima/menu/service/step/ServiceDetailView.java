package id.tajima.tajima.menu.service.step;

import id.tajima.tajima.menu.service.model.Data;
import id.tajima.tajima.menu.service.model.ServiceRe;

public class ServiceDetailView {
    interface View {
        void onError(String message);

        void onPartReady(String message, Boolean ready);

        void onDataService(ServiceRe data);

        void onLogStep(Data data);
    }

    interface Presenter {
        void checkPartReady(String serviceId);

        void setServiceData(String json);

        void getLogStep(String serviceId, String customer);
    }
}
