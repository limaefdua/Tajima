package id.tajima.tajima.menu.service.dialog.payment;

import id.tajima.tajima.menu.service.step.model.Data;

public interface PaymentView {
    interface View {
        void onPayment(Data data);

        void onPaymentStep(Data data);

        void onError(String message);
    }

    interface Presenter {
        void paymentStep(String serviceId, String message);

        void checkIsPaid(String serviceId);
    }
}
