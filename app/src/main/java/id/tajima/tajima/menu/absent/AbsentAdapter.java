package id.tajima.tajima.menu.absent;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.custom.InfoDialog;
import id.tajima.tajima.menu.absent.model.Listing;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by Maftuhin on 25/10/2017.
 */

public class AbsentAdapter extends Adapter<AbsentAdapter.AbsentViewHolder> {
    private List<Listing> data;
    private Context context;

    AbsentAdapter(Context c, List<Listing> data) {
        this.data = data;
        this.context = c;
    }

    @NonNull
    @Override
    public AbsentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_absence, parent, false);
        return new AbsentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AbsentViewHolder holder, int position) {
        holder.tvDate.setText(data.get(position).getDate());
        holder.tvIn.setText(DateUtil.getDate(data.get(position).getStartIjin()));
        holder.tvOut.setText(DateUtil.getDate(data.get(position).getEndIjin()));
        holder.tvStatus.setText(data.get(position).getStatus());
        switch (data.get(position).getStatus()) {
            case "deny":
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.redPresensi));
                break;
            case "pending":
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.absen));
                break;
            case "approve":
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_green_600));
                break;
            case "Expired":
                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.nliveo_orange_colorPrimary));
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailDialog(data.get(holder.getAdapterPosition()));
            }
        });
    }

    private void showDetailDialog(Listing absence) {
        InfoDialog dialog = new InfoDialog(context);
        dialog.show();
        dialog.setTitle("ABSENCE");
        dialog.setMessage("Reason : " + absence.getReason() + "\n" + "Status : " + absence.getStatus());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class AbsentViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvIn, tvOut, tvTotal, tvStatus;

        AbsentViewHolder(View view) {
            super(view);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvDate = view.findViewById(R.id.tvTanggalPresensi);
            tvIn = view.findViewById(R.id.tvJamIn);
            tvOut = view.findViewById(R.id.tvJamOut);
            tvTotal = view.findViewById(R.id.tvTotal);
        }
    }
}
