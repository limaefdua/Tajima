package id.tajima.tajima.menu.service.dialog.part_return;

import id.tajima.tajima.menu.service.step.model.Data;

public interface PartReturnView {
    interface View {
        void onReturn(Data data);

        void onReturnStep();

        void onError(String message);
    }

    interface Presenter {
        void returnStep(String serviceId);

        void checkPartReturn(String serviceId);
    }
}
