package id.tajima.tajima.menu.service.dialog.payment;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PaymentPresenter implements PaymentView.Presenter {
    private Context context;
    private PaymentView.View view;
    Subscription checkIsPaid, serviceStep;
    Api _api;
    AppPreferences preferences;
    CompositeSubscription compositeSubscription = new CompositeSubscription();

    public PaymentPresenter(Context context, PaymentView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void paymentStep(String serviceId, String message) {
        serviceStep = _api.paymentStep(preferences.getUserToken(), preferences.getUserId(), serviceId, message)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (serviceStep.isUnsubscribed()) {
                            serviceStep.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onPaymentStep(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(serviceStep);
    }

    @Override
    public void checkIsPaid(String serviceId) {
        checkIsPaid = _api.checkPayment(preferences.getUserToken(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (checkIsPaid.isUnsubscribed()) {
                            checkIsPaid.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onPayment(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(checkIsPaid);
    }
}
