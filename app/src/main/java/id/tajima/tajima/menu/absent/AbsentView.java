package id.tajima.tajima.menu.absent;

import id.tajima.tajima.menu.absent.model.Data;

public interface AbsentView {
    interface View {
        void onDataExist(Data data);

        void onError(String message);
    }

    interface Presenter {
        void showAllAbsent();
    }
}
