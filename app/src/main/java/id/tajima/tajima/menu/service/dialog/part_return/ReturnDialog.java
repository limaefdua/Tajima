package id.tajima.tajima.menu.service.dialog.part_return;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.step.model.Data;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class ReturnDialog extends Dialog implements View.OnClickListener, PartReturnView.View {
    private Context context;
    private OnMyDialogResult mDialogResult;
    private LinearLayout btnSubmitted;
    private TextView tvDateReturn, tvPartReturn;
    private Button btnSubmit, btnReset;
    private PartReturnView.Presenter presenter;
    private Boolean partReturn = false;
    private String serviceId;


    public ReturnDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_return);
        initView();
        presenter = new PartReturnPresenter(context, this);
    }

    private void initView() {
        tvDateReturn = findViewById(R.id.tvDateReturn);
        tvPartReturn = findViewById(R.id.tvPartReturn);
        btnSubmitted = findViewById(R.id.btnSubmitted);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        btnSubmitted.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmitted:
                //checkReturn();
                break;
            case R.id.btnSubmit:
                submitReturn();
                break;
        }
    }

    private void submitReturn() {
        if (partReturn) {
            presenter.returnStep(serviceId);
        } else {
            Toast.makeText(context, "Part not return yet", Toast.LENGTH_SHORT).show();
        }
    }

    public void setPartReturnResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    @Override
    public void onReturn(Data data) {
        this.partReturn = data.getSuccessMessage();
        if (data.getSuccessMessage()) {
//            partReturned();
            tvPartReturn.setText("Part Return Completely");
            tvDateReturn.setVisibility(View.VISIBLE);
            tvPartReturn.setTextColor(Color.GREEN);
            tvDateReturn.setTextColor(Color.GREEN);
        }else{
            tvDateReturn.setVisibility(View.GONE);
            tvPartReturn.setText("Part Not Return Yet");
            tvPartReturn.setTextColor(Color.GRAY);
        }
    }

    @Override
    public void onReturnStep() {
        dismiss();
        mDialogResult.onPartReturn("yes");
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public interface OnMyDialogResult {
        void onPartReturn(String result);
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
        presenter.checkPartReturn(serviceId);
    }
}
