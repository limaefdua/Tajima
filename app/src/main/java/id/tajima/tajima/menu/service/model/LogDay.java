package id.tajima.tajima.menu.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogDay {

    @SerializedName("dateLog")
    @Expose
    private String dateLog;

    public String getDateLog() {
        return dateLog;
    }

    public void setDateLog(String dateLog) {
        this.dateLog = dateLog;
    }
}
