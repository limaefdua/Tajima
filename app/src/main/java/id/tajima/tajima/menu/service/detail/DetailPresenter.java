package id.tajima.tajima.menu.service.detail;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.model.ServiceResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DetailPresenter implements DetailView.Presenter {
    private Context context;
    private DetailView.View view;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Subscription detail;
    private Api api;
    private AppPreferences preferences;

    public DetailPresenter(Context context, DetailView.View view) {
        this.context = context;
        this.view = view;
        api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void getDetailDays(String serviceId, String date) {
        detail = api.detailLogDays(preferences.getUserToken(), serviceId, date).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (detail.isUnsubscribed()) {
                            detail.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onSuccess(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(detail);
    }
}
