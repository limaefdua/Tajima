package id.tajima.tajima.menu.absent.add;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.absent.model.AbsentResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class IjinPresenter implements IjinView.Presenter {
    private Context context;
    private IjinView.View view;
    private Api _api;
    private AppPreferences preferences;
    private Subscription absent;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public IjinPresenter(Context context, IjinView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void doAbsent(String start, String end, String reason) {
        absent = _api.addAbsent(preferences.getUserToken(), preferences.getUserId(), start, end, reason, "pending").
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<AbsentResponse>() {
                    @Override
                    public void onCompleted() {
                        if (absent.isUnsubscribed()) {
                            absent.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onAbsentFailed(e.getMessage());
                    }

                    @Override
                    public void onNext(AbsentResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onAbsentSuccess(data.getData().getSuccessMessage());
                        } else {
                            view.onAbsentFailed(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(absent);
    }
}
