package id.tajima.tajima.menu.service.dialog.depart;

import java.util.List;

import id.tajima.tajima.menu.service.step.model.LogStep;

public interface DepartView {
    interface View {
        void onError(String message);

        void onDriverExist(List<DriverModel> driver);

        void onDepartSuccess(LogStep logStep);

        void onLoading();

        void doneLoading();
    }

    interface Presenter {
        void getDriver(String serviceId);

        void departStep(String serviceId, String vehicle, String driverId);
    }
}
