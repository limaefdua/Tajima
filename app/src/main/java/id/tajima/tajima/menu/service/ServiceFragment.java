package id.tajima.tajima.menu.service;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import id.tajima.tajima.R;
import id.tajima.tajima.custom.ClearEdit;
import id.tajima.tajima.menu.service.dialog.filter.DialogFilter;
import id.tajima.tajima.menu.service.dialog.filter.FilterModel;
import id.tajima.tajima.menu.service.model.Data;
import id.tajima.tajima.utility.AppPreferences;

/**
 * Created by Maftuhin on 30/10/2017.
 */

public class ServiceFragment extends Fragment implements ServiceView.View, View.OnClickListener, ClearEdit.SearchListener, ServiceAdapter.onAcceptDialog, AdapterView.OnItemSelectedListener {
    ServiceView.Presenter presenter;
    ServiceAdapter adapter;
    RecyclerView rv;
    LinearLayout btnFilter;
    ClearEdit ceService;
    TextView tvCount, tvInfo;
    Spinner s;
    RelativeLayout progressBar;
    AppPreferences preferences;
    Button btnAll;
    boolean mSpinnerInitialized = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_service, container, false);
        presenter = new ServicePresenter(getActivity(), this);
        preferences = new AppPreferences(getActivity());
        getActivity().setTitle("Service");
        initView(v);

        rv = v.findViewById(R.id.rvHistoryService);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        s = v.findViewById(R.id.spFilter);
        ArrayAdapter<String> a = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, getActivity().getResources().getStringArray(R.array.sort));
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setOnItemSelectedListener(this);
        s.setAdapter(a);
        adapter = new ServiceAdapter(getContext());
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initView(View v) {
        btnAll = v.findViewById(R.id.btnAll);
        btnFilter = v.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        btnAll.setOnClickListener(this);
        ceService = v.findViewById(R.id.ceService);
        ceService.searchButton(this);
        tvCount = v.findViewById(R.id.tvCount);
        tvInfo = v.findViewById(R.id.tvInfo);
        progressBar = v.findViewById(R.id.progressbarLayout);
    }

    @Override
    public void onDataExist(Data data) {
        rv.setVisibility(View.VISIBLE);
        tvCount.setVisibility(View.VISIBLE);
        tvInfo.setVisibility(View.GONE);
        tvCount.setText("Found :" + data.getService().size());
        adapter.setServiceData(data.getService());
        adapter.onAcceptDialog(this);
        rv.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        tvCount.setVisibility(View.GONE);
        rv.setVisibility(View.GONE);
        tvInfo.setVisibility(View.VISIBLE);
        tvInfo.setText(message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFilter:
                showFilterDialog();
                break;
            case R.id.btnAll:
                ceService.clearTextQuery();
                progressBar.setVisibility(View.VISIBLE);
                if (preferences.getUserRole().equals("part_staff")) {
                    presenter.getAllServices();
                } else {
                    presenter.showServiceByEngineer();
                }
                break;
        }
    }

    private void allService(boolean b) {
        if (b) {
            btnAll.setBackground(getActivity().getResources().getDrawable(R.drawable.btn_filter_selected));
            btnAll.setTextColor(Color.WHITE);
        } else {
            btnAll.setBackground(getActivity().getResources().getDrawable(R.drawable.btn_border_grey));
            btnAll.setTextColor(Color.BLACK);
        }
    }

    private void showFilterDialog() {
        DialogFilter df = new DialogFilter(getActivity());
        df.show();
        df.setFilterResult(new DialogFilter.OnDialogFilter() {
            @Override
            public void filter(FilterModel fm) {
                progressBar.setVisibility(View.VISIBLE);
                presenter.filter(fm);
            }
        });
    }

    @Override
    public void searchListener(String text) {
        if (text.length() > 0) {
            allService(false);
            progressBar.setVisibility(View.VISIBLE);
            if (preferences.getUserRole().equals("part_staff")) {
                presenter.searchAsPart(text);
            } else {
                presenter.searchService(text, s.getSelectedItem().toString().toLowerCase(), "ASC");
            }
        } else {
            ceService.setError("Type something");
        }
    }

    private void loadMain() {
        progressBar.setVisibility(View.VISIBLE);
        allService(true);
        //ceService.clearTextQuery();
        if (preferences.getUserRole().equals("part_staff")) {
            presenter.getAllServices();
        } else {
            presenter.showServiceByEngineer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMain();
    }

    @Override
    public void setAccept(Boolean acc) {
        loadMain();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!mSpinnerInitialized) {
            mSpinnerInitialized = true;
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        String text = ceService.getText().toString();
        if (text.length() > 0) {
            allService(false);
        } else {
            allService(true);
        }
        if (preferences.getUserRole().equals("part_staff")) {
            presenter.searchAsPart(text);
        } else {
            presenter.searchService(text, s.getSelectedItem().toString().toLowerCase(), "ASC");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        return;
    }
}
