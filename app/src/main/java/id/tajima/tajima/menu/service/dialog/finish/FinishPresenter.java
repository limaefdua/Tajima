package id.tajima.tajima.menu.service.dialog.finish;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class FinishPresenter implements FinishView.Presenter {
    private Context context;
    private FinishView.View view;
    private Api _api;
    private AppPreferences preferences;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Subscription driver, finish;

    FinishPresenter(Context context, FinishView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void getDriver(String serviceId) {
        driver = _api.getDriver(preferences.getUserToken(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (driver.isUnsubscribed()) {
                            driver.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onDriverExist(data.getData().getDriver());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(driver);
    }

    @Override
    public void finishStep(String serviceId, String vehicle, String driverId) {
        finish = _api.finishAndReturn(preferences.getUserToken(), preferences.getUserId(), vehicle, serviceId, driverId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (finish.isUnsubscribed()) {
                            finish.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onFinishSuccess(data.getData().getLogStep());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(finish);
    }
}
