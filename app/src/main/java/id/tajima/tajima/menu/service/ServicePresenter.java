package id.tajima.tajima.menu.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.dialog.filter.FilterModel;
import id.tajima.tajima.menu.service.model.ServiceResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ServicePresenter implements ServiceView.Presenter {
    private Context context;
    private ServiceView.View view;
    private Subscription service, search, filter;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Api _api;
    private AppPreferences preferences;

    ServicePresenter(Context context, ServiceView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }


    @Override
    public void showServiceByEngineer() {
        Log.w("service","showbyenginer");
        service = _api.serviceByEngineer(preferences.getUserToken(), preferences.getUserId()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (service.isUnsubscribed()) {
                            service.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        Log.w("data_service", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            if (data.getData() != null) {
                                view.onDataExist(data.getData());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(service);
    }

    @Override
    public void getAllServices() {
        Log.w("service","all service");
        service = _api.getAllService(preferences.getUserToken()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (service.isUnsubscribed()) {
                            service.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        Log.w("asda", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            if (data.getData() != null) {
                                view.onDataExist(data.getData());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(service);
    }

    @Override
    public void searchService(String query, String sortBy, String sort) {
        Log.w("service","search service");
        search = _api.serviceSearch(preferences.getUserToken(), preferences.getUserId(), query, sortBy, sort).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (search.isUnsubscribed()) {
                            search.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(search);
    }

    @Override
    public void searchAsPart(String query) {
        Log.w("service","search as part");
        search = _api.serviceSearchAsPart(preferences.getUserToken(), preferences.getUserId(), query).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (search.isUnsubscribed()) {
                            search.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(search);
    }

    @Override
    public void filter(FilterModel fm) {
        filter = _api.filterService(
                preferences.getUserToken(),
                preferences.getUserId(),
                fm.getBranchId(),
                fm.getPayment(),
                fm.getStatus(),
                fm.getDateStart(),
                fm.getDateEnd()
        ).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (filter.isUnsubscribed()) {
                            filter.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(filter);
    }
}
