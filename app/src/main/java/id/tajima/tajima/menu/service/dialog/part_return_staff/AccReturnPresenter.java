package id.tajima.tajima.menu.service.dialog.part_return_staff;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AccReturnPresenter implements AccReturnView.Presenter {
    private Context context;
    private AccReturnView.View view;
    private AppPreferences preferences;
    private Api _api;
    private Subscription partReturn;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public AccReturnPresenter(Context context, AccReturnView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void accPartReturn(String serviceId) {
        partReturn = _api.partReturnStaff(preferences.getUserToken(), preferences.getUserId(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (partReturn.isUnsubscribed()) {
                            partReturn.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.getMessage();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onPartReturn();
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(partReturn);
    }
}
