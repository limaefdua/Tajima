package id.tajima.tajima.menu.presensi;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.presensi.dialog.CheckInDialog;
import id.tajima.tajima.menu.presensi.model.Data;

/**
 * A simple {@link Fragment} subclass.
 */
public class PresenceFragment extends Fragment implements PresenceView.View, View.OnClickListener {
    TableAdapter adapter;
    PresenceView.Presenter presenter;
    Button btnCheckIn, btnCheckOut;
    TextView tvDate;
    String currentDateTimeString;
    TextClock clock;
    RecyclerView rec;
    RelativeLayout progressBar;
    Snackbar snackbar;

    public PresenceFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_presensi, container, false);
        presenter = new PresencePresenter(getActivity(), this);
        getActivity().setTitle("PRESENCE");
        _initView(view);
        progressBar.setVisibility(View.VISIBLE);
        presenter.showAllPresence();
        return view;
    }

    private void _initView(View view) {
        progressBar = view.findViewById(R.id.progressbarLayout);
        clock = view.findViewById(R.id.textClock);
        btnCheckIn = view.findViewById(R.id.btnCheckIn);
        btnCheckOut = view.findViewById(R.id.btnCheckOut);
        btnCheckIn.setOnClickListener(this);
        btnCheckOut.setOnClickListener(this);
        tvDate = view.findViewById(R.id.tvCurrentDate);

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        currentDateTimeString = df.format(new Date());
        tvDate.setText(currentDateTimeString);

        rec = view.findViewById(R.id.rvPresensi);
        rec.setLayoutManager(new LinearLayoutManager(getContext()));
        rec.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onDataExist(Data data) {
        progressBar.setVisibility(View.GONE);
        adapter = new TableAdapter(getContext(), data.getListing());
        rec.setAdapter(adapter);
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        snackbar = Snackbar.make(progressBar, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onCheckIn(String successMessage) {
        progressBar.setVisibility(View.GONE);
        presenter.showAllPresence();
        snackbar = Snackbar.make(progressBar, successMessage, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onCheckOut(String successMessage) {
        progressBar.setVisibility(View.GONE);
        presenter.showAllPresence();
        snackbar = Snackbar.make(progressBar, successMessage, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCheckIn:
                dialogPresence(true);
                break;
            case R.id.btnCheckOut:
                dialogPresence(false);
                break;
        }
    }

    private void dialogPresence(boolean type) {
        CheckInDialog dialog = new CheckInDialog(getContext());
        dialog.show();
        dialog.setType(type);
        dialog.onSubmit(new CheckInDialog.OnSubmit() {
            @Override
            public void presence(boolean check_in, String task) {
                progressBar.setVisibility(View.VISIBLE);
                if (check_in) {
                    presenter.checkIn(task);
                } else {
                    presenter.checkOut();
                }
            }
        });
    }
}
