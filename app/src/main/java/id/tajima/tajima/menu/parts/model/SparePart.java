package id.tajima.tajima.menu.parts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SparePart {
    @SerializedName("part_id")
    @Expose
    private String partId;
    @SerializedName("part_mesin_code")
    @Expose
    private String partMesinCode;
    @SerializedName("part_no")
    @Expose
    private String partNo;
    @SerializedName("part_revised_no")
    @Expose
    private String partRevisedNo;
    @SerializedName("part_latest_no")
    @Expose
    private String partLatestNo;
    @SerializedName("part_desc")
    @Expose
    private String partDesc;
    @SerializedName("part_incoming_date")
    @Expose
    private String partIncomingDate;
    @SerializedName("part_fob_japan")
    @Expose
    private String partFobJapan;
    @SerializedName("part_min_order")
    @Expose
    private String partMinOrder;
    @SerializedName("part_master_key1")
    @Expose
    private String partMasterKey1;
    @SerializedName("part_total_stock")
    @Expose
    private String partTotalStock;
    @SerializedName("part_stock_j")
    @Expose
    private String partStockJ;
    @SerializedName("part_stock_b")
    @Expose
    private String partStockB;
    @SerializedName("part_stock_s")
    @Expose
    private String partStockS;
    @SerializedName("part_stock_m")
    @Expose
    private String partStockM;
    @SerializedName("part_rak")
    @Expose
    private Object partRak;
    @SerializedName("part_last_opname")
    @Expose
    private String partLastOpname;
    @SerializedName("part_discontinued")
    @Expose
    private String partDiscontinued;
    @SerializedName("part_srp")
    @Expose
    private String partSrp;
    @SerializedName("part_srp_cod")
    @Expose
    private String partSrpCod;
    @SerializedName("part_supv_price")
    @Expose
    private String partSupvPrice;
    @SerializedName("part_mgr_price")
    @Expose
    private String partMgrPrice;
    @SerializedName("part_dtr_price")
    @Expose
    private String partDtrPrice;
    @SerializedName("part_keterangan")
    @Expose
    private String partKeterangan;
    @SerializedName("part_is_delete")
    @Expose
    private Object partIsDelete;
    @SerializedName("part_location")
    @Expose
    private String partLocation;

    public String getPartId() {
        return partId;
    }

    public String getPartMesinCode() {
        return partMesinCode;
    }

    public String getPartNo() {
        return partNo;
    }

    public String getPartRevisedNo() {
        return partRevisedNo;
    }

    public String getPartLatestNo() {
        return partLatestNo;
    }

    public String getPartDesc() {
        return partDesc;
    }

    public String getPartIncomingDate() {
        return partIncomingDate;
    }

    public String getPartFobJapan() {
        return partFobJapan;
    }

    public String getPartMinOrder() {
        return partMinOrder;
    }

    public String getPartMasterKey1() {
        return partMasterKey1;
    }

    public String getPartTotalStock() {
        return partTotalStock;
    }

    public String getPartStockJ() {
        return partStockJ;
    }

    public String getPartStockB() {
        return partStockB;
    }

    public String getPartStockS() {
        return partStockS;
    }

    public String getPartStockM() {
        return partStockM;
    }

    public Object getPartRak() {
        return partRak;
    }

    public String getPartLastOpname() {
        return partLastOpname;
    }

    public String getPartDiscontinued() {
        return partDiscontinued;
    }

    public String getPartSrp() {
        return partSrp;
    }

    public String getPartSrpCod() {
        return partSrpCod;
    }

    public String getPartSupvPrice() {
        return partSupvPrice;
    }

    public String getPartMgrPrice() {
        return partMgrPrice;
    }

    public String getPartDtrPrice() {
        return partDtrPrice;
    }

    public String getPartKeterangan() {
        return partKeterangan;
    }

    public Object getPartIsDelete() {
        return partIsDelete;
    }

    public String getPartLocation() {
        return partLocation;
    }
}
