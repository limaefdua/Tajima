package id.tajima.tajima.menu.parts.request;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.parts.model.PartResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class RequestPresenter implements RequestView.Presenter {
    Context context;
    RequestView.View view;
    private Subscription request;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Api _api;
    private AppPreferences preferences;

    public RequestPresenter(Context context, RequestView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void requestSparePart(String qty, String customer, String desc, String req_to, String req_from, String part_id) {
        request = _api.requestSparePart(preferences.getUserToken(), qty, customer, desc, req_to, req_from, part_id).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<PartResponse>() {
                    @Override
                    public void onCompleted() {
                        if (request.isUnsubscribed()) {
                            request.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(PartResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onRequestSuccess();
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(request);
    }
}
