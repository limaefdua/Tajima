package id.tajima.tajima.menu.service.dialog.arrive;

import id.tajima.tajima.menu.service.step.model.LogStep;

public interface ArriveView {
    interface View {
        void onArrive(LogStep logStep);

        void onArriveAtOffice(LogStep logStep);

        void onError(String message);
    }

    interface Presenter {
        void arriveStep(String serviceId);

        void arriveAtOffice(String serviceId);
    }
}
