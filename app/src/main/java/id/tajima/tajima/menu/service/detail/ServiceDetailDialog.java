package id.tajima.tajima.menu.service.detail;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.model.Data;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class ServiceDetailDialog extends Dialog implements View.OnClickListener, DetailView.View {
    private Context context;
    private RecyclerView rvStep;
    private StepAdapter adapter;
    private List<StepModel> data = new ArrayList<>();
    private DetailView.Presenter presenter;
    private TextView tvLogDate;
    RelativeLayout progressBar;

    public ServiceDetailDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_service_detail);
        initView();
        //setStepData();
        presenter = new DetailPresenter(context, this);
    }

    private void initView() {
        adapter = new StepAdapter(context);
        rvStep = findViewById(R.id.rvStep);
        tvLogDate = findViewById(R.id.tvLogDate);
        rvStep.setLayoutManager(new LinearLayoutManager(context));

        progressBar = findViewById(R.id.progressbarLayout);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onSuccess(Data step) {
        data.add(new StepModel("Parts Ordering", DateUtil.getTime(step.getListingStep().getLogstep1())));
        data.add(new StepModel("Parts Ready", DateUtil.getTime(step.getListingStep().getLogstep2())));
        data.add(new StepModel("Depart", DateUtil.getTime(step.getListingStep().getLogstep3())));
        data.add(new StepModel("Arrive", DateUtil.getTime(step.getListingStep().getLogstep4())));
        data.add(new StepModel("End Service", DateUtil.getTime(step.getListingStep().getLogstep5())));
        data.add(new StepModel("Finish & Return", DateUtil.getTime(step.getListingStep().getLogstep6())));
        data.add(new StepModel("Arrive", DateUtil.getTime(step.getListingStep().getLogstep7())));
        data.add(new StepModel("Payment", DateUtil.getTime(step.getListingStep().getLogstep8())));
        data.add(new StepModel("Parts Return", DateUtil.getTime(step.getListingStep().getLogstep9())));
        data.add(new StepModel("Complete", DateUtil.getTime(step.getListingStep().getLogstep9())));
        data.add(new StepModel("Status", step.getStatusService()));
        data.add(new StepModel("Daily Service Duration", step.getDailyDuration().getDailyDuration()));
        data.add(new StepModel("Daily Duration", step.getDailyDuration().getDailyDuration()));
        data.add(new StepModel("All Day Total Duration", step.getDailyDuration().getTotal()));
        data.add(new StepModel("Remark", step.getRemarks()));
        adapter.setStepData(data);
        rvStep.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        data.add(new StepModel("Parts Ordering", ""));
        data.add(new StepModel("Parts Ready", ""));
        data.add(new StepModel("Depart", ""));
        data.add(new StepModel("Arrive", ""));
        data.add(new StepModel("End Service", ""));
        data.add(new StepModel("Finish & Return", ""));
        data.add(new StepModel("Arrive", ""));
        data.add(new StepModel("Payment", ""));
        data.add(new StepModel("Parts Return", ""));
        data.add(new StepModel("Complete", ""));
        data.add(new StepModel("Status", ""));
        data.add(new StepModel("Daily Service Duration", ""));
        data.add(new StepModel("Daily Duration", ""));
        data.add(new StepModel("All Day Total Duration", ""));
        data.add(new StepModel("Remark", ""));
        adapter.setStepData(data);
        rvStep.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void getData(int day, String serviceId, String date) {
        progressBar.setVisibility(View.VISIBLE);
        presenter.getDetailDays(serviceId, date);
        tvLogDate.setText("Day " + day + "\n" + date);
    }
}
