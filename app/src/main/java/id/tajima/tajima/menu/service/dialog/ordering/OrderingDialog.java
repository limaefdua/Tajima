package id.tajima.tajima.menu.service.dialog.ordering;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.step.model.Data;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class OrderingDialog extends Dialog implements View.OnClickListener, OrderingView.View {
    private Context context;
    private Button btnYes, btnNo, btnInt, btnReady;
    private LinearLayout lytNext;
    private OnMyDialogResult mDialogResult;
    private OrderingView.Presenter presenter;
    private String serviceId;
    private String finishing;
    private ProgressBar progressBar;

    public OrderingDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_part_ordering);
        initView();
        presenter = new OrderingPresenter(context, this);
    }

    private void initView() {
        btnYes = findViewById(R.id.btYa);
        btnNo = findViewById(R.id.btNo);
        btnReady = findViewById(R.id.btnReady);
        btnInt = findViewById(R.id.btnInt);
        progressBar = findViewById(R.id.progressBar);
        btnReady.setOnClickListener(this);
        btnInt.setOnClickListener(this);
        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);

        lytNext = findViewById(R.id.lytNextProgress);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btYa:
                lytNext.setVisibility(View.VISIBLE);
                break;
            case R.id.btNo:
                lytNext.setVisibility(View.GONE);
                presenter.orderPart(serviceId, "0");
                finishing = "no";
                break;
            case R.id.btnInt:
                lytNext.setVisibility(View.GONE);
                presenter.orderPart(serviceId, "1");
                finishing = "yes";
                break;
            case R.id.btnReady:
                lytNext.setVisibility(View.GONE);
                presenter.orderPart(serviceId, "1");
                finishing = "yes";
                break;
        }
    }

    @Override
    public void onSuccess(Data data) {
        mDialogResult.partOrdering(finishing, data.getLogStep().getLogstep1());
        dismiss();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
        btnNo.setEnabled(false);
        btnYes.setEnabled(false);
    }

    @Override
    public void doneLoading() {
        progressBar.setVisibility(View.GONE);
        btnNo.setEnabled(true);
        btnYes.setEnabled(true);
    }

    public interface OnMyDialogResult {
        void partOrdering(String finishing, String logStep);
    }

    public void setPartOrderingResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
