package id.tajima.tajima.menu.service;

import id.tajima.tajima.menu.service.dialog.filter.FilterModel;
import id.tajima.tajima.menu.service.model.Data;

public interface ServiceView {
    interface View {
        void onDataExist(Data data);

        void onError(String message);
    }

    interface Presenter {
        void showServiceByEngineer();

        void getAllServices();

        void searchService(String query, String sortBy, String sort);

        void searchAsPart(String query);

        void filter(FilterModel fm);
    }
}
