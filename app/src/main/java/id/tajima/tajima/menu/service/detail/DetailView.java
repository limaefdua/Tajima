package id.tajima.tajima.menu.service.detail;

import id.tajima.tajima.menu.service.model.Data;

public interface DetailView {
    interface View {
        void onSuccess(Data listingStep);

        void onError(String message);
    }

    interface Presenter {
        void getDetailDays(String serviceId, String date);
    }
}
