package id.tajima.tajima.menu.absent.add;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.utility.DateUtil;

public class IjinActivity extends AppCompatActivity implements IjinView.View, View.OnClickListener {
    IjinView.Presenter presenter;
    EditText etReason, etDateStart, etDateEnd;
    Button btnAbsent;
    RelativeLayout progressBar;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ijin);
        presenter = new IjinPresenter(this, this);
        initView();
    }

    private void initView() {
        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Permohonan Ijin");
        etDateStart = findViewById(R.id.etDateStart);
        etDateEnd = findViewById(R.id.etDateEnd);
        etReason = findViewById(R.id.etReason);
        btnAbsent = findViewById(R.id.btnAbsent);
        progressBar = findViewById(R.id.progressbarLayout);
        btnAbsent.setOnClickListener(this);

        etDateStart.setText(DateUtil.timeDefault());
        etDateEnd.setText(DateUtil.timeDefault());
        etDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateUtil.defDatePickerShow(IjinActivity.this, etDateStart.getText().toString(), DateUtil.datePickerLabel(etDateStart));
            }
        });
        etDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateUtil.defDatePickerShow(IjinActivity.this, etDateEnd.getText().toString(), DateUtil.datePickerLabel(etDateEnd));
            }
        });
    }

    @Override
    public void onAbsentSuccess(String successMessage) {
        Toast.makeText(this, successMessage, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onAbsentFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAbsent:
                progressBar.setVisibility(View.VISIBLE);
                presenter.doAbsent(
                        etDateStart.getText().toString(),
                        etDateEnd.getText().toString(),
                        etReason.getText().toString()
                );
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
