package id.tajima.tajima.menu.service.dialog.ordering;

import id.tajima.tajima.menu.service.step.model.Data;

public interface OrderingView {
    interface View {
        void onSuccess(Data data);

        void onError(String message);

        void onLoading();
        void doneLoading();
    }

    interface Presenter {
        void orderPart(String serviceId, String ordering);
    }
}
