package id.tajima.tajima.menu.presensi.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import id.tajima.tajima.R;

public class CheckInDialog extends Dialog implements View.OnClickListener {
    private OnSubmit onSubmit;
    private Button btnPresence;
    private boolean type = true;
    private TextView tvTitle, tvCheckOut;
    private EditText etTask;

    public CheckInDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_check_in);
        initView();
    }

    private void initView() {
        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
        tvTitle = findViewById(R.id.tvTitle);
        etTask = findViewById(R.id.etTask);
        btnPresence = findViewById(R.id.btnPresence);
        btnPresence.setOnClickListener(this);
        tvCheckOut = findViewById(R.id.tvCheckOut);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnPresence:
                onSubmit.presence(type, etTask.getText().toString());
                dismiss();
                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

    public interface OnSubmit {
        void presence(boolean check_in, String task);
    }

    public void onSubmit(OnSubmit onSubmit) {
        this.onSubmit = onSubmit;
    }

    public void setType(boolean type) {
        this.type = type;
        if (type) {
            tvTitle.setText("CHECK IN");
            btnPresence.setText("Check In");
            etTask.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setText("CHECK OUT");
            btnPresence.setText("Check Out");
            tvCheckOut.setVisibility(View.VISIBLE);
        }
    }
}
