package id.tajima.tajima.menu.service.step.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.tajima.tajima.menu.service.dialog.depart.DriverModel;

public class Data {
    @SerializedName("success_message")
    @Expose
    private Boolean successMessage;

    @SerializedName("log_step")
    @Expose
    private LogStep logStep;

    @SerializedName("paid_log")
    @Expose
    private String paidLog;

    @SerializedName("service_amount")
    @Expose
    private int serviceAmount;

    @SerializedName("driver")
    @Expose
    private List<DriverModel> driver = null;

    @SerializedName("mcn_data")
    @Expose
    private List<McnData> mcnData = null;

    public List<DriverModel> getDriver() {
        return driver;
    }

    public Boolean getSuccessMessage() {
        return successMessage;
    }

    public String getPaidLog() {
        return paidLog;
    }

    public List<McnData> getMcnData() {
        return mcnData;
    }

    public LogStep getLogStep() {
        return logStep;
    }

    public int getServiceAmount() {
        return serviceAmount;
    }
}
