package id.tajima.tajima.menu.service.dialog.filter;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.model.ServiceResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class FilterPresenter implements FilterView.Presenter {
    private Context context;
    private FilterView.View view;
    private Api api;
    private Subscription branch;
    private CompositeSubscription cs = new CompositeSubscription();
    private AppPreferences preferences;

    FilterPresenter(Context context, FilterView.View view) {
        this.context = context;
        this.view = view;
        api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void getBranch() {
        branch = api.getBranch(preferences.getUserToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (branch.isUnsubscribed()) {
                            branch.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        if (data.getStatus() == 200) {
                            view.onBranchExist(data.getData().getBranch());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        cs.add(branch);
    }
}
