package id.tajima.tajima.menu.presensi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("listing")
    @Expose
    private List<Listing> listing = null;

    @SerializedName("success_message")
    @Expose
    private String successMessage;

    public List<Listing> getListing() {
        return listing;
    }

    public void setListing(List<Listing> listing) {
        this.listing = listing;
    }

    public String getSuccessMessage() {
        return successMessage;
    }
}
