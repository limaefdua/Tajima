package id.tajima.tajima.menu.service.dialog.progress;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.model.ServiceResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DaysPresenter implements DaysView.Presenter {
    Context context;
    DaysView.View view;
    CompositeSubscription compositeSubscription = new CompositeSubscription();
    Subscription days;
    Api api;
    AppPreferences preferences;

    public DaysPresenter(Context context, DaysView.View view) {
        this.context = context;
        this.view = view;
        api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void getLogDays(String serviceId) {
        days = api.getLogDays(preferences.getUserToken(), preferences.getUserId(), serviceId).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (days.isUnsubscribed()) {
                            days.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData().getLogDay());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(days);
    }
}
