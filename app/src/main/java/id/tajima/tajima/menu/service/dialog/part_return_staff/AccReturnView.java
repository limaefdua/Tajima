package id.tajima.tajima.menu.service.dialog.part_return_staff;

public interface AccReturnView {
    interface View {
        void onPartReturn();

        void onError(String message);
    }

    interface Presenter {
        void accPartReturn(String serviceId);
    }
}
