package id.tajima.tajima.menu.service.dialog.finish;

import java.util.List;

import id.tajima.tajima.menu.service.dialog.depart.DriverModel;
import id.tajima.tajima.menu.service.step.model.LogStep;

public interface FinishView {
    interface View {
        void onError(String message);

        void onDriverExist(List<DriverModel> driver);

        void onFinishSuccess(LogStep logStep);
    }

    interface Presenter {
        void getDriver(String serviceId);

        void finishStep(String serviceId, String vehicle, String driverId);
    }
}
