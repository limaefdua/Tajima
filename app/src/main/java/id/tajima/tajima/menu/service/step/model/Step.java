package id.tajima.tajima.menu.service.step.model;

public class Step {
    private String stepName;
    private String stepSubName;
    private String stepDate;
    private String stepTime;
    private int stepStatus;

    public Step() {
    }

    public Step(String stepName, String stepSubName, String stepDate, String stepTime, int stepStatus) {
        this.stepName = stepName;
        this.stepSubName = stepSubName;
        this.stepDate = stepDate;
        this.stepTime = stepTime;
        this.stepStatus = stepStatus;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public String getStepSubName() {
        return stepSubName;
    }

    public void setStepSubName(String stepSubName) {
        this.stepSubName = stepSubName;
    }

    public String getStepDate() {
        return stepDate;
    }

    public void setStepDate(String stepDate) {
        this.stepDate = stepDate;
    }

    public String getStepTime() {
        return stepTime;
    }

    public void setStepTime(String stepTime) {
        this.stepTime = stepTime;
    }

    public int getStepStatus() {
        return stepStatus;
    }

    public void setStepStatus(int stepStatus) {
        this.stepStatus = stepStatus;
    }
}
