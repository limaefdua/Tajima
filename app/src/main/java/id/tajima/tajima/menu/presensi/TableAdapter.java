package id.tajima.tajima.menu.presensi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.presensi.model.Listing;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by Maftuhin on 25/10/2017.
 */

public class TableAdapter extends Adapter<TableAdapter.TableViewHolder> {
    private List<Listing> data;
    private Context c;

    TableAdapter(Context c, List<Listing> data) {
        this.data = data;
        this.c = c;
    }

    @Override
    public TableViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_presensi, parent, false);
        return new TableViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TableViewHolder holder, int position) {
        if (data.get(position).getTanggal() != null) {
            holder.itemView.setBackgroundColor(c.getResources().getColor(R.color.absen));
        } else {
            holder.itemView.setBackgroundColor(c.getResources().getColor(R.color.redPresensi));
        }
        holder.tvDate.setText(data.get(position).getTanggal());
        holder.tvIn.setText(DateUtil.getTime(data.get(position).getPresensiStart()));
        holder.tvOut.setText(DateUtil.getTime(data.get(position).getPresensiEnd()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class TableViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate, tvIn, tvOut;

        TableViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvTanggalPresensi);
            tvIn = itemView.findViewById(R.id.tvJamIn);
            tvOut = itemView.findViewById(R.id.tvJamOut);
        }
    }
}
