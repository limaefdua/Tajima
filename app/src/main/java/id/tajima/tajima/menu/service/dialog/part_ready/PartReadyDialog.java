package id.tajima.tajima.menu.service.dialog.part_ready;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import id.tajima.tajima.R;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class PartReadyDialog extends Dialog implements View.OnClickListener, PartReadyView.View {
    private Context context;
    private Button btnYes, btnNotYet;
    private LinearLayout lyNotYet;
    private PartReadyView.Presenter presenter;
    private Button btnSubmit;
    private Boolean ready = false;
    private EditText etMessage;
    private String serviceId;

    public PartReadyDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_part_ready);
        initView();
        presenter = new PartReadyPresenter(context, this);
    }

    private void initView() {
        btnYes = findViewById(R.id.btnYes);
        btnNotYet = findViewById(R.id.btnNotYet);
        lyNotYet = findViewById(R.id.lyNotYet);
        btnSubmit = findViewById(R.id.btnSubmit);
        etMessage = findViewById(R.id.etMessage);
        btnSubmit.setOnClickListener(this);
        btnYes.setOnClickListener(this);
        btnNotYet.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnYes:
                changeButton("yes");
                break;
            case R.id.btnNotYet:
                changeButton("no");
                break;
            case R.id.btnSubmit:
                presenter.partReady(serviceId, ready, etMessage.getText().toString());
                break;
        }
    }

    private void changeButton(String choice) {
        if (choice.equals("yes")) {
            lyNotYet.setVisibility(View.GONE);
            btnYes.setBackground(context.getResources().getDrawable(R.drawable.btn_border_selected));
            btnNotYet.setBackground(context.getResources().getDrawable(R.drawable.btn_border_grey));
            btnYes.setTextColor(Color.WHITE);
            btnNotYet.setTextColor(Color.BLACK);
            this.ready = true;
        } else {
            this.ready = false;
            lyNotYet.setVisibility(View.VISIBLE);
            btnYes.setBackground(context.getResources().getDrawable(R.drawable.btn_border_grey));
            btnNotYet.setBackground(context.getResources().getDrawable(R.drawable.btn_border_selected));
            btnYes.setTextColor(Color.BLACK);
            btnNotYet.setTextColor(Color.WHITE);
        }
    }

    @Override
    public void onPartReady() {
        dismiss();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
