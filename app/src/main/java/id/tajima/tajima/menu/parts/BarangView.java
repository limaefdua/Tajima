package id.tajima.tajima.menu.parts;

import id.tajima.tajima.menu.parts.model.Data;

public interface BarangView {
    interface View {
        void onDataExist(Data data);

        void onError(String message);
    }

    interface Presenter {
        void showSparePart(String sort);

        void searchSparePart(String query);
    }
}
