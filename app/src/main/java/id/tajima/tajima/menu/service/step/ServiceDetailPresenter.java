package id.tajima.tajima.menu.service.step;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.model.ServiceRe;
import id.tajima.tajima.menu.service.model.ServiceResponse;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ServiceDetailPresenter implements ServiceDetailView.Presenter {
    private Context context;
    private ServiceDetailView.View view;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Subscription ready, detail;
    private AppPreferences preferences;
    private Api _api;

    ServiceDetailPresenter(Context context, ServiceDetailView.View view) {
        this.context = context;
        this.view = view;
        preferences = new AppPreferences(context);
        _api = TajimaApp.createService(Api.class);
    }

    @Override
    public void checkPartReady(String serviceId) {
        ready = _api.checkPartReady(preferences.getUserToken(), preferences.getUserId(), serviceId).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (ready.isUnsubscribed()) {
                            ready.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        Log.w("da", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            if (data.getData().getSuccessMessage()) {
                                view.onPartReady(data.getMessage(), true);
                            } else {
                                view.onPartReady(data.getMessage(), false);
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(ready);
    }

    @Override
    public void setServiceData(String json) {
        Gson gs = new Gson();
        ServiceRe sr = gs.fromJson(json, ServiceRe.class);
        view.onDataService(sr);
    }

    @Override
    public void getLogStep(String serviceId, String customer) {
        Log.w("asda", serviceId+"--"+customer);
        detail = _api.getLogStep(preferences.getUserToken(), customer, serviceId).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<ServiceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (detail.isUnsubscribed()) {
                            detail.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceResponse data) {
                        Log.w("asda_service", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            view.onLogStep(data.getData());
                        } else {
                            if (data.getData().getListingStep() != null) {
                                view.onLogStep(data.getData());
                            } else {
                                view.onError(data.getMessage());
                            }
                        }
                    }
                });
        compositeSubscription.add(detail);
    }
}
