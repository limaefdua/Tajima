package id.tajima.tajima.menu.service.step;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.step.model.Step;

/**
 * Created by Maftuhin on 25/10/2017.
 */

public class LogStepAdapter extends Adapter<LogStepAdapter.StepViewHolder> {
    private List<Step> data;
    private Context context;
    private onStepClicked mDialogResult;

    LogStepAdapter(Context c) {
        this.context = c;
    }

    void setStepData(List<Step> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public StepViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_log_step, parent, false);
        return new StepViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final StepViewHolder holder, int position) {
        holder.tvStepNumber.setText(String.valueOf(position + 1));
        holder.tvStepName.setText(data.get(position).getStepName());
        switch (data.get(position).getStepStatus()) {
            case 1:
                holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.shape_round_service_white));
                holder.itemView.setOnClickListener(null);
                holder.tvSubStep.setText("");
                holder.tvDate.setText("");
                holder.tvTime.setText("");
                holder.imgDone.setVisibility(View.GONE);
                break;
            case 2:
                holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.shape_round_servicegreen));
                holder.imgDone.setVisibility(View.GONE);
                holder.tvSubStep.setText("");
                holder.tvDate.setText("");
                holder.tvTime.setText("");
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialogResult.clickStep(holder.getAdapterPosition());
                    }
                });
                break;
            case 3:
                holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.shape_round_servicegray));
                holder.itemView.setOnClickListener(null);
                holder.tvSubStep.setText(data.get(position).getStepSubName());
                holder.tvDate.setText(data.get(position).getStepDate());
                holder.tvTime.setText(data.get(position).getStepTime());
                holder.imgDone.setVisibility(View.VISIBLE);
                break;
            case 4:
                holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.shape_round_servicegreen));
                holder.imgDone.setVisibility(View.VISIBLE);
                holder.tvSubStep.setText(data.get(position).getStepSubName());
                holder.tvDate.setText("");
                holder.tvTime.setText("");
                holder.itemView.setOnClickListener(null);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class StepViewHolder extends RecyclerView.ViewHolder {
        TextView tvStepNumber, tvStepName, tvSubStep, tvDate, tvTime;
        ImageView imgDone;

        StepViewHolder(View view) {
            super(view);
            tvStepNumber = view.findViewById(R.id.tvStepNumber);
            tvStepName = view.findViewById(R.id.tvStepName);
            imgDone = view.findViewById(R.id.imgDone);
            tvSubStep = view.findViewById(R.id.tvSubStep);
            tvDate = view.findViewById(R.id.tvDate);
            tvTime = view.findViewById(R.id.tvTime);
        }
    }

    public interface onStepClicked {
        void clickStep(int step);
    }

    void setOnStepClicked(onStepClicked stepClick) {
        mDialogResult = stepClick;
    }
}
