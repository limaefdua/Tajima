package id.tajima.tajima.menu.service.detail;

public class StepModel {
    String stepName;
    String stepValue;

    public StepModel(String stepName, String stepValue) {
        this.stepName = stepName;
        this.stepValue = stepValue;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public String getStepValue() {
        return stepValue;
    }

    public void setStepValue(String stepValue) {
        this.stepValue = stepValue;
    }
}
