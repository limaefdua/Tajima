package id.tajima.tajima.menu.service.dialog.service;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.dialog.service.model.Reasons;
import id.tajima.tajima.menu.service.step.model.LogStep;
import id.tajima.tajima.menu.service.step.model.McnData;
import id.tajima.tajima.utility.FormUtils;
import lib.kingja.switchbutton.SwitchMultiButton;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class ServiceDialog extends Dialog implements View.OnClickListener, ServiceDialogView.View {
    private Context context;
    private OnMyDialogResult mDialogResult;
    private Button btnSubmit, btnReset;
    private Button btnTne, btnPun, btnAsn, btnPns;
    private EditText etOtherReason, etOtherDay;
    private EditText etAmount, etTotal, etCharge;
    private LinearLayout lytComplete, lytIncomplete;
    private Boolean complete;
    private Reasons reason;
    private ServiceDialogView.Presenter presenter;
    private String serviceId;
    private Spinner s, spPayment;
    private int amount, charge = 0;
    private SwitchMultiButton switchMultiButton, switchMultiFoc, switchMultiWtc;

    public ServiceDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_service);
        initView();
        presenter = new ServiceDialogPresenter(context, this);
        presenter.listMcn();
    }

    private void initView() {
        s = findViewById(R.id.spMcn);
        spPayment = findViewById(R.id.spPayment);
        String[] mt = {"Cash", "Transfer"};
        ArrayAdapter<String> a = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item, mt);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPayment.setAdapter(a);

        btnSubmit = findViewById(R.id.btnSubmit);
        lytIncomplete = findViewById(R.id.lytIncomplete);
        lytComplete = findViewById(R.id.lytComplete);
        btnReset = findViewById(R.id.btnReset);

        btnPns = findViewById(R.id.btnPns);
        btnPun = findViewById(R.id.btnPun);
        btnAsn = findViewById(R.id.btnAsn);
        btnTne = findViewById(R.id.btnTne);
        etOtherReason = findViewById(R.id.etOtherReason);
        etOtherDay = findViewById(R.id.etOtherDay);
        etAmount = findViewById(R.id.etAmount);
        etTotal = findViewById(R.id.etTotal);

        btnReset.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        btnPns.setOnClickListener(this);
        btnPun.setOnClickListener(this);
        btnAsn.setOnClickListener(this);
        btnTne.setOnClickListener(this);

        reason = new Reasons();
        etCharge = findViewById(R.id.etCharge);

        switchMultiButton = findViewById(R.id.switchMultiButton);
        switchMultiFoc = findViewById(R.id.switchFoc);
        switchMultiWtc = findViewById(R.id.switchWtc);
        switchMultiButton.setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
            @Override
            public void onSwitch(int position, String tabText) {
                switch (position) {
                    case 0:
                        complete = true;
                        lytIncomplete.setVisibility(View.GONE);
                        lytComplete.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        complete = false;
                        lytIncomplete.setVisibility(View.VISIBLE);
                        lytComplete.setVisibility(View.GONE);
                        break;
                }
            }
        });
        switchMultiButton.setSelectedTab(0);

        etCharge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                changeTotal(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void changeTotal(String s) {
        charge = s.equals("") ? 0 : Integer.valueOf(s);
        etTotal.setText(FormUtils.toRpString(charge + amount));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmitted:
                checkPayment();
                break;
            case R.id.btnSubmit:
                submitService();
                break;
            case R.id.btnReset:
                dismiss();
                break;
            case R.id.btnAsn:
                if (reason.getbAssistanceNeeded()) {
                    reason.setbAssistanceNeeded(false);
                    reason.setsAssistanceNeeded("");
                    btnAsn.setBackground(context.getResources().getDrawable(R.drawable.btn_border_grey));
                } else {
                    reason.setbAssistanceNeeded(true);
                    reason.setsAssistanceNeeded("Assistance Needed,");
                    btnAsn.setBackground(context.getResources().getDrawable(R.drawable.btn_filter_selected));
                }
                break;
            case R.id.btnPns:
                if (reason.getbPartNoStock()) {
                    reason.setbPartNoStock(false);
                    reason.setsPartNoStock("");
                    btnPns.setBackground(context.getResources().getDrawable(R.drawable.btn_border_grey));
                } else {
                    reason.setbPartNoStock(true);
                    reason.setsPartNoStock("Part No Stock,");
                    btnPns.setBackground(context.getResources().getDrawable(R.drawable.btn_filter_selected));
                }
                break;
            case R.id.btnPun:
                if (reason.getbPartUncarried()) {
                    reason.setbPartUncarried(false);
                    reason.setsPartUncarried("");
                    btnPun.setBackground(context.getResources().getDrawable(R.drawable.btn_border_grey));
                } else {
                    reason.setbPartUncarried(true);
                    reason.setsPartUncarried("Part Uncarried,");
                    btnPun.setBackground(context.getResources().getDrawable(R.drawable.btn_filter_selected));
                }
                break;
            case R.id.btnTne:
                if (reason.getbTimeNotEnough()) {
                    reason.setbTimeNotEnough(false);
                    reason.setsTimeNotEnough("");
                    btnTne.setBackground(context.getResources().getDrawable(R.drawable.btn_border_grey));
                } else {
                    reason.setbTimeNotEnough(true);
                    reason.setsTimeNotEnough("Time Not Enough,");
                    btnTne.setBackground(context.getResources().getDrawable(R.drawable.btn_filter_selected));
                }
                break;
        }
    }

    private void submitService() {
        if (complete) {
            String mcn = s.getSelectedItem().toString();
            //String charge = etCharge.getText().toString();
            String method = spPayment.getSelectedItem().toString();
            presenter.serviceComplete(serviceId, mcn, foc(), String.valueOf(charge), method);
        } else {
            String r = reason.getsAssistanceNeeded() + reason.getsTimeNotEnough() + reason.getsPartNoStock() + reason.getsPartUncarried();
            String continue_at = context.getResources().getStringArray(R.array.wtc_tabs)[switchMultiWtc.getSelectedTab()];
            if (etOtherDay.getText().toString().length() > 0) {
                continue_at = etOtherDay.getText().toString();
            }
            presenter.serviceIncomplete(serviceId, r, continue_at);
        }
    }

    private boolean foc() {
        return switchMultiFoc.getSelectedTab() == 0;
    }

    private void checkPayment() {

    }

    public void setServiceResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    @Override
    public void onServiceComplete(LogStep logStep) {
        dismiss();
        mDialogResult.onService("yes", logStep.getLogstep5());
    }

    @Override
    public void onServiceInComplete(LogStep logStep) {
        dismiss();
        mDialogResult.onService("yes", logStep.getLogstep5());
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMcnExist(List<McnData> data) {
        List<String> mcnList = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            mcnList.add(data.get(i).getMesinName());
        }

        ArrayAdapter<String> a = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_dropdown_item, mcnList);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(a);
    }

    @Override
    public void onAmountExist(int amount) {
        this.amount = amount;
        etAmount.setText(FormUtils.toRpString(amount));
        etTotal.setText(FormUtils.toRpString(amount));
    }

    public interface OnMyDialogResult {
        void onService(String result, String logStep);
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
        presenter.getServiceAmount(serviceId);
    }
}
