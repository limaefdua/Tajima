package id.tajima.tajima.menu.service.dialog.arrive;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ArrivePresenter implements ArriveView.Presenter {
    private Context context;
    private ArriveView.View view;
    private Api _api;
    private AppPreferences preferences;
    private CompositeSubscription cs = new CompositeSubscription();
    private Subscription arrive, office;

    ArrivePresenter(Context context, ArriveView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void arriveStep(String serviceId) {
        arrive = _api.arriveStep(preferences.getUserToken(), preferences.getUserId(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (arrive.isUnsubscribed()) {
                            arrive.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.getCause();
                        e.getMessage();
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        Log.w("office", new Gson().toJson(data));
                        if (data.getStatus() == 200) {
                            if (data.getData().getSuccessMessage()) {
                                view.onArrive(data.getData().getLogStep());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        cs.add(arrive);
    }

    @Override
    public void arriveAtOffice(String serviceId) {
        office = _api.arriveAtOffice(preferences.getUserToken(), preferences.getUserId(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (office.isUnsubscribed()) {
                            office.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.getCause();
                        e.getMessage();
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        Log.w("office", new Gson().toJson(data));
                        if (data.getStatus() == 200) {
                            if (data.getData().getSuccessMessage()) {
                                view.onArriveAtOffice(data.getData().getLogStep());
                            }
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        cs.add(office);
    }
}
