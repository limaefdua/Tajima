package id.tajima.tajima.menu.service.dialog.depart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverModel {
    @SerializedName("servenginer_id")
    @Expose
    private String servenginerId;
    @SerializedName("servenginer_service_id")
    @Expose
    private String servenginerServiceId;
    @SerializedName("servenginer_enginer_id")
    @Expose
    private String servenginerEnginerId;
    @SerializedName("vehicle_pergi")
    @Expose
    private Object vehiclePergi;
    @SerializedName("vehicle_pulang")
    @Expose
    private Object vehiclePulang;
    @SerializedName("driver_pergi")
    @Expose
    private Object driverPergi;
    @SerializedName("driver_pulang")
    @Expose
    private Object driverPulang;
    @SerializedName("servenginer_created_by")
    @Expose
    private String servenginerCreatedBy;
    @SerializedName("servenginer_created_at")
    @Expose
    private String servenginerCreatedAt;
    @SerializedName("servenginer_updated_by")
    @Expose
    private Object servenginerUpdatedBy;
    @SerializedName("servenginer_updated_at")
    @Expose
    private Object servenginerUpdatedAt;
    @SerializedName("nama_teknisi")
    @Expose
    private String namaTeknisi;

    public String getServenginerId() {
        return servenginerId;
    }

    public void setServenginerId(String servenginerId) {
        this.servenginerId = servenginerId;
    }

    public String getServenginerServiceId() {
        return servenginerServiceId;
    }

    public void setServenginerServiceId(String servenginerServiceId) {
        this.servenginerServiceId = servenginerServiceId;
    }

    public String getServenginerEnginerId() {
        return servenginerEnginerId;
    }

    public void setServenginerEnginerId(String servenginerEnginerId) {
        this.servenginerEnginerId = servenginerEnginerId;
    }

    public Object getVehiclePergi() {
        return vehiclePergi;
    }

    public void setVehiclePergi(Object vehiclePergi) {
        this.vehiclePergi = vehiclePergi;
    }

    public Object getVehiclePulang() {
        return vehiclePulang;
    }

    public void setVehiclePulang(Object vehiclePulang) {
        this.vehiclePulang = vehiclePulang;
    }

    public Object getDriverPergi() {
        return driverPergi;
    }

    public void setDriverPergi(Object driverPergi) {
        this.driverPergi = driverPergi;
    }

    public Object getDriverPulang() {
        return driverPulang;
    }

    public void setDriverPulang(Object driverPulang) {
        this.driverPulang = driverPulang;
    }

    public String getServenginerCreatedBy() {
        return servenginerCreatedBy;
    }

    public void setServenginerCreatedBy(String servenginerCreatedBy) {
        this.servenginerCreatedBy = servenginerCreatedBy;
    }

    public String getServenginerCreatedAt() {
        return servenginerCreatedAt;
    }

    public void setServenginerCreatedAt(String servenginerCreatedAt) {
        this.servenginerCreatedAt = servenginerCreatedAt;
    }

    public Object getServenginerUpdatedBy() {
        return servenginerUpdatedBy;
    }

    public void setServenginerUpdatedBy(Object servenginerUpdatedBy) {
        this.servenginerUpdatedBy = servenginerUpdatedBy;
    }

    public Object getServenginerUpdatedAt() {
        return servenginerUpdatedAt;
    }

    public void setServenginerUpdatedAt(Object servenginerUpdatedAt) {
        this.servenginerUpdatedAt = servenginerUpdatedAt;
    }

    public String getNamaTeknisi() {
        return namaTeknisi;
    }

    public void setNamaTeknisi(String namaTeknisi) {
        this.namaTeknisi = namaTeknisi;
    }

}
