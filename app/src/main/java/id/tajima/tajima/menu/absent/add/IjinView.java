package id.tajima.tajima.menu.absent.add;

public interface IjinView {
    interface View {
        void onAbsentSuccess(String successMessage);

        void onAbsentFailed(String message);
    }

    interface Presenter {
        void doAbsent(String start, String end, String alasan);
    }
}
