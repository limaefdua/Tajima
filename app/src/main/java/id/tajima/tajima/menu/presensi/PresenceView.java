package id.tajima.tajima.menu.presensi;

import id.tajima.tajima.menu.presensi.model.Data;

public interface PresenceView {
    interface View {
        void onDataExist(Data data);

        void onError(String message);

        void onCheckIn(String successMessage);

        void onCheckOut(String successMessage);
    }

    interface Presenter {
        void showAllPresence();

        void checkIn(String task);

        void checkOut();
    }
}
