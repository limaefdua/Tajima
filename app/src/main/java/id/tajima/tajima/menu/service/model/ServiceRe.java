package id.tajima.tajima.menu.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceRe {

    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("service_status_id")
    @Expose
    private String serviceStatusId;
    @SerializedName("service_customer_id")
    @Expose
    private String serviceCustomerId;
    @SerializedName("service_code")
    @Expose
    private String serviceCode;
    @SerializedName("service_ref_number")
    @Expose
    private String serviceRefNumber;
    @SerializedName("service_title")
    @Expose
    private String serviceTitle;
    @SerializedName("service_desc")
    @Expose
    private String serviceDesc;
    @SerializedName("service_task_price")
    @Expose
    private String serviceTaskPrice;
    @SerializedName("service_net_rev")
    @Expose
    private String serviceNetRev;
    @SerializedName("service_part_price")
    @Expose
    private String servicePartPrice;
    @SerializedName("service_charge")
    @Expose
    private String serviceCharge;
    @SerializedName("service_status_payment")
    @Expose
    private String serviceStatusPayment;
    @SerializedName("service_created_by")
    @Expose
    private String serviceCreatedBy;
    @SerializedName("service_created_at")
    @Expose
    private String serviceCreatedAt;
    @SerializedName("service_updated_by")
    @Expose
    private String serviceUpdatedBy;
    @SerializedName("service_updated_at")
    @Expose
    private String serviceUpdatedAt;
    @SerializedName("service_status_delete")
    @Expose
    private String serviceStatusDelete;
    @SerializedName("service_deleted_by")
    @Expose
    private Object serviceDeletedBy;
    @SerializedName("service_deleted_at")
    @Expose
    private Object serviceDeletedAt;
    @SerializedName("service_restored_by")
    @Expose
    private Object serviceRestoredBy;
    @SerializedName("service_restored_at")
    @Expose
    private Object serviceRestoredAt;
    @SerializedName("waktu_kerja")
    @Expose
    private String waktuKerja;
    @SerializedName("log_day")
    @Expose
    private String logDay;
    @SerializedName("servenginer_enginer_id")
    @Expose
    private String servenginerEnginerId;
    @SerializedName("serstat_status_id")
    @Expose
    private String serstatStatusId;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_location")
    @Expose
    private String customerLocation;
    @SerializedName("servenginer_fullname")
    @Expose
    private String servenginerFullname;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceStatusId() {
        return serviceStatusId;
    }

    public void setServiceStatusId(String serviceStatusId) {
        this.serviceStatusId = serviceStatusId;
    }

    public String getServiceCustomerId() {
        return serviceCustomerId;
    }

    public void setServiceCustomerId(String serviceCustomerId) {
        this.serviceCustomerId = serviceCustomerId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceRefNumber() {
        return serviceRefNumber;
    }

    public void setServiceRefNumber(String serviceRefNumber) {
        this.serviceRefNumber = serviceRefNumber;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServiceTaskPrice() {
        return serviceTaskPrice;
    }

    public void setServiceTaskPrice(String serviceTaskPrice) {
        this.serviceTaskPrice = serviceTaskPrice;
    }

    public String getServiceNetRev() {
        return serviceNetRev;
    }

    public void setServiceNetRev(String serviceNetRev) {
        this.serviceNetRev = serviceNetRev;
    }

    public String getServicePartPrice() {
        return servicePartPrice;
    }

    public void setServicePartPrice(String servicePartPrice) {
        this.servicePartPrice = servicePartPrice;
    }

    public String getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(String serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public String getServiceStatusPayment() {
        return serviceStatusPayment;
    }

    public void setServiceStatusPayment(String serviceStatusPayment) {
        this.serviceStatusPayment = serviceStatusPayment;
    }

    public String getServiceCreatedBy() {
        return serviceCreatedBy;
    }

    public void setServiceCreatedBy(String serviceCreatedBy) {
        this.serviceCreatedBy = serviceCreatedBy;
    }

    public String getServiceCreatedAt() {
        return serviceCreatedAt;
    }

    public void setServiceCreatedAt(String serviceCreatedAt) {
        this.serviceCreatedAt = serviceCreatedAt;
    }

    public String getServiceUpdatedBy() {
        return serviceUpdatedBy;
    }

    public void setServiceUpdatedBy(String serviceUpdatedBy) {
        this.serviceUpdatedBy = serviceUpdatedBy;
    }

    public String getServiceUpdatedAt() {
        return serviceUpdatedAt;
    }

    public void setServiceUpdatedAt(String serviceUpdatedAt) {
        this.serviceUpdatedAt = serviceUpdatedAt;
    }

    public String getServiceStatusDelete() {
        return serviceStatusDelete;
    }

    public void setServiceStatusDelete(String serviceStatusDelete) {
        this.serviceStatusDelete = serviceStatusDelete;
    }

    public Object getServiceDeletedBy() {
        return serviceDeletedBy;
    }

    public void setServiceDeletedBy(Object serviceDeletedBy) {
        this.serviceDeletedBy = serviceDeletedBy;
    }

    public Object getServiceDeletedAt() {
        return serviceDeletedAt;
    }

    public void setServiceDeletedAt(Object serviceDeletedAt) {
        this.serviceDeletedAt = serviceDeletedAt;
    }

    public Object getServiceRestoredBy() {
        return serviceRestoredBy;
    }

    public void setServiceRestoredBy(Object serviceRestoredBy) {
        this.serviceRestoredBy = serviceRestoredBy;
    }

    public Object getServiceRestoredAt() {
        return serviceRestoredAt;
    }

    public void setServiceRestoredAt(Object serviceRestoredAt) {
        this.serviceRestoredAt = serviceRestoredAt;
    }

    public String getWaktuKerja() {
        return waktuKerja;
    }

    public void setWaktuKerja(String waktuKerja) {
        this.waktuKerja = waktuKerja;
    }

    public String getLogDay() {
        return logDay;
    }

    public void setLogDay(String logDay) {
        this.logDay = logDay;
    }

    public String getServenginerEnginerId() {
        return servenginerEnginerId;
    }

    public void setServenginerEnginerId(String servenginerEnginerId) {
        this.servenginerEnginerId = servenginerEnginerId;
    }

    public String getSerstatStatusId() {
        return serstatStatusId;
    }

    public void setSerstatStatusId(String serstatStatusId) {
        this.serstatStatusId = serstatStatusId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerLocation() {
        return customerLocation;
    }

    public void setCustomerLocation(String customerLocation) {
        this.customerLocation = customerLocation;
    }

    public String getServenginerFullname() {
        return servenginerFullname;
    }

    public void setServenginerFullname(String servenginerFullname) {
        this.servenginerFullname = servenginerFullname;
    }
}
