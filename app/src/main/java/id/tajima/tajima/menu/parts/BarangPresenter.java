package id.tajima.tajima.menu.parts;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.parts.model.PartResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class BarangPresenter implements BarangView.Presenter {
    private Context context;
    private BarangView.View view;
    private Api _api;
    private AppPreferences preferences;
    private Subscription part, search;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public BarangPresenter(Context context, BarangView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }


    @Override
    public void showSparePart(String sort) {
        part = _api.showAllSparePart(preferences.getUserToken(), sort).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<PartResponse>() {
                    @Override
                    public void onCompleted() {
                        if (part.isUnsubscribed()) {
                            part.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(PartResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(part);
    }

    @Override
    public void searchSparePart(String query) {
        search = _api.searchSparepart(preferences.getUserToken(), query).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<PartResponse>() {
                    @Override
                    public void onCompleted() {
                        if (search.isUnsubscribed()) {
                            search.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(PartResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(part);
    }
}
