package id.tajima.tajima.menu.parts.request;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.parts.model.SparePart;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class RequestDialog extends Dialog implements View.OnClickListener, RequestView.View {
    private Context context;
    private TextView tvPartName, tvDateTime;
    private RequestView.Presenter presenter;
    private EditText etQuantity, etCustomer, etReason;
    private String partId;
    private RelativeLayout progress;

    public RequestDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_prompt_suggest);
        presenter = new RequestPresenter(context, this);
        initView();
    }

    private void initView() {
        tvPartName = findViewById(R.id.tvPartName);
        tvDateTime = findViewById(R.id.tvDateTime);
        Button btCancel = findViewById(R.id.btCancel);
        Button btSubmit = findViewById(R.id.btSubmit);
        btCancel.setOnClickListener(this);
        btSubmit.setOnClickListener(this);

        etQuantity = findViewById(R.id.etQuantity);
        etCustomer = findViewById(R.id.etCustomer);
        etReason = findViewById(R.id.etReason);

        progress = findViewById(R.id.progressbarLayout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btCancel:
                dismiss();
                break;
            case R.id.btSubmit:
                progress.setVisibility(View.VISIBLE);
                presenter.requestSparePart(
                        etQuantity.getText().toString(),
                        etCustomer.getText().toString(),
                        etReason.getText().toString(),
                        "", "",
                        partId
                );
                break;
        }
    }

    public void setDataRequest(String ca, SparePart data) {
        tvPartName.setText(data.getPartDesc());
        tvDateTime.setText(DateUtil.dateTime());
        partId = data.getPartId();
    }

    @Override
    public void onRequestSuccess() {
        progress.setVisibility(View.GONE);
        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void onError(String message) {
        progress.setVisibility(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
