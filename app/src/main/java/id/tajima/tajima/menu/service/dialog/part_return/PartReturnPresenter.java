package id.tajima.tajima.menu.service.dialog.part_return;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PartReturnPresenter implements PartReturnView.Presenter {
    private Context context;
    private PartReturnView.View view;
    private Api _api;
    private AppPreferences preferences;
    private Subscription partReturn, checkPartReturn;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    PartReturnPresenter(Context context, PartReturnView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void returnStep(String serviceId) {
        partReturn = _api.partReturnStep(preferences.getUserToken(), preferences.getUserId(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (partReturn.isUnsubscribed()) {
                            partReturn.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onReturnStep();
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(partReturn);
    }

    @Override
    public void checkPartReturn(String serviceId) {
        checkPartReturn = _api.checkPartReturn(preferences.getUserToken(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (checkPartReturn.isUnsubscribed()) {
                            checkPartReturn.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        if (data.getStatus() == 200) {
                            view.onReturn(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(checkPartReturn);
    }
}
