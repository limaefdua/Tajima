package id.tajima.tajima.menu.service.dialog.part_return_staff;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import id.tajima.tajima.R;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class PartReturnDialog extends Dialog implements View.OnClickListener, AccReturnView.View {
    private Context context;
    private AccReturnView.Presenter presenter;
    private Button btnComplete, btnSubmit, btnAlert;
    private String serviceId;

    public PartReturnDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_part_return_staff);
        initView();
        presenter = new AccReturnPresenter(context, this);
    }

    private void initView() {
        btnAlert = findViewById(R.id.btnAlert);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnComplete = findViewById(R.id.btnComplete);

        btnAlert.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnComplete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnComplete:
                break;
            case R.id.btnAlert:
                break;
            case R.id.btnSubmit:
                presenter.accPartReturn(serviceId);
                break;
        }
    }

    @Override
    public void onPartReturn() {

    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
