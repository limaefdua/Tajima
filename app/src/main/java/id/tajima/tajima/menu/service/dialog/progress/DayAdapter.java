package id.tajima.tajima.menu.service.dialog.progress;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.detail.ServiceDetailDialog;
import id.tajima.tajima.menu.service.model.LogDay;

public class DayAdapter extends RecyclerView.Adapter<DayAdapter.DayViewHolder> {
    private Context c;
    private List<LogDay> data = new ArrayList<>();
    private String serviceId;

    DayAdapter(Context c) {
        this.c = c;
    }

    public void setLogDay(List<LogDay> data) {
        this.data = data;
    }

    @Override
    public DayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_progress, null);
        return new DayViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DayViewHolder holder, final int position) {
        holder.tvDays.setText(String.valueOf(position + 1));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceDetailDialog sd = new ServiceDetailDialog(c);
                sd.show();
                sd.getData(position + 1, serviceId, data.get(position).getDateLog());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    class DayViewHolder extends RecyclerView.ViewHolder {
        TextView tvDays;

        DayViewHolder(View itemView) {
            super(itemView);
            tvDays = itemView.findViewById(R.id.tvDay);
        }
    }

}