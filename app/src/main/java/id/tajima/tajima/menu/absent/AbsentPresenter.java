package id.tajima.tajima.menu.absent;

import android.content.Context;

import id.tajima.tajima.menu.absent.model.AbsentResponse;
import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AbsentPresenter implements AbsentView.Presenter {
    private Context context;
    private AbsentView.View view;
    private Api _api;
    private AppPreferences preferences;
    private Subscription absent;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    AbsentPresenter(Context context, AbsentView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void showAllAbsent() {
        absent = _api.showAllAbsent(preferences.getUserToken(), preferences.getUserId()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<AbsentResponse>() {
                    @Override
                    public void onCompleted() {
                        if (absent.isUnsubscribed()) {
                            absent.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(AbsentResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(absent);
    }
}
