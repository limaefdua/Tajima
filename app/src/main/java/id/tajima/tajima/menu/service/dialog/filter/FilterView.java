package id.tajima.tajima.menu.service.dialog.filter;

import java.util.List;

import id.tajima.tajima.menu.service.model.Branch;

public interface FilterView {
    interface View {
        void onBranchExist(List<Branch> branch);

        void onError(String message);
    }

    interface Presenter {
        void getBranch();
    }
}
