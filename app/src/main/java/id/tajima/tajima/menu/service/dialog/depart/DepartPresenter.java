package id.tajima.tajima.menu.service.dialog.depart;

import android.content.Context;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DepartPresenter implements DepartView.Presenter {
    private Context context;
    private DepartView.View view;
    private Api _api;
    private AppPreferences preferences;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Subscription driver, depart;

    DepartPresenter(Context context, DepartView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void getDriver(String serviceId) {
        view.onLoading();
        driver = _api.getDriver(preferences.getUserToken(), serviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (driver.isUnsubscribed()) {
                            driver.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.doneLoading();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        view.doneLoading();
                        if (data.getStatus() == 200) {
                            view.onDriverExist(data.getData().getDriver());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(driver);
    }

    @Override
    public void departStep(String serviceId, final String vehicle, String driverId) {
        view.onLoading();
        depart = _api.departStep(preferences.getUserToken(), vehicle, serviceId, driverId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ServiceDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        if (depart.isUnsubscribed()) {
                            depart.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                        view.doneLoading();
                    }

                    @Override
                    public void onNext(ServiceDetailResponse data) {
                        view.doneLoading();
                        if (data.getStatus() == 200) {
                            view.onDepartSuccess(data.getData().getLogStep());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(depart);
    }
}
