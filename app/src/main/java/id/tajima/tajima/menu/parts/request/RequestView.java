package id.tajima.tajima.menu.parts.request;

public class RequestView {
    interface View {
        void onRequestSuccess();

        void onError(String message);
    }

    interface Presenter {
        void requestSparePart(String qty,String customer, String desc, String req_to, String req_from, String part_id);
    }
}
