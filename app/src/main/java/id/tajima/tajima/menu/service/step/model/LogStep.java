package id.tajima.tajima.menu.service.step.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogStep {
    @SerializedName("logstep_1")
    @Expose
    private String logstep1;
    @SerializedName("logstep_2")
    @Expose
    private String logstep2;
    @SerializedName("logstep_3")
    @Expose
    private String logstep3;
    @SerializedName("logstep_4")
    @Expose
    private String logstep4;
    @SerializedName("logstep_5")
    @Expose
    private String logstep5;
    @SerializedName("logstep_6")
    @Expose
    private String logstep6;
    @SerializedName("logstep_7")
    @Expose
    private String logstep7;
    @SerializedName("logstep_8")
    @Expose
    private String logstep8;
    @SerializedName("logstep_9")
    @Expose
    private String logstep9;

    public String getLogstep1() {
        return logstep1;
    }

    public String getLogstep2() {
        return logstep2;
    }

    public String getLogstep3() {
        return logstep3;
    }

    public String getLogstep4() {
        return logstep4;
    }

    public String getLogstep5() {
        return logstep5;
    }

    public String getLogstep6() {
        return logstep6;
    }

    public String getLogstep7() {
        return logstep7;
    }

    public String getLogstep8() {
        return logstep8;
    }

    public String getLogstep9() {
        return logstep9;
    }

    public void setLogstep1(String logstep1) {
        this.logstep1 = logstep1;
    }

    public void setLogstep2(String logstep2) {
        this.logstep2 = logstep2;
    }

    public void setLogstep3(String logstep3) {
        this.logstep3 = logstep3;
    }

    public void setLogstep4(String logstep4) {
        this.logstep4 = logstep4;
    }

    public void setLogstep5(String logstep5) {
        this.logstep5 = logstep5;
    }

    public void setLogstep6(String logstep6) {
        this.logstep6 = logstep6;
    }

    public void setLogstep7(String logstep7) {
        this.logstep7 = logstep7;
    }

    public void setLogstep8(String logstep8) {
        this.logstep8 = logstep8;
    }

    public void setLogstep9(String logstep9) {
        this.logstep9 = logstep9;
    }
}
