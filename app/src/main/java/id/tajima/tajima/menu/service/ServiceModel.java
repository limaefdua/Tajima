package id.tajima.tajima.menu.service;

/**
 * Created by Maftuhin on 30/10/2017.
 */

public class ServiceModel {

    String namaCust;
    String requestDate;
    String requestTime;
    String doneStatus;

    public ServiceModel(String namaCust, String requestDate, String requestTime, String doneStatus) {
        this.namaCust = namaCust;
        this.requestDate = requestDate;
        this.requestTime = requestTime;
        this.doneStatus = doneStatus;
    }
}
