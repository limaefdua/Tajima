package id.tajima.tajima.menu.service.dialog.payment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.step.model.Data;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class PaymentDialog extends Dialog implements View.OnClickListener, PaymentView.View {
    private Context context;
    private OnMyDialogResult mDialogResult;
    private LinearLayout btnSubmitted;
    private TextView tvDateSubmitted, tvPaymentSubmitted;
    private Button btnSubmit, btnReset;
    private PaymentView.Presenter presenter;
    private Boolean payment = false;
    private String serviceId;
    private EditText etMessage;
    private ProgressBar progressBar;

    public PaymentDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_payment);
        initView();
        presenter = new PaymentPresenter(context, this);
    }

    private void initView() {
        tvDateSubmitted = findViewById(R.id.tvDateSubmitted);
        tvPaymentSubmitted = findViewById(R.id.tvPaymentSubmitted);
        btnSubmitted = findViewById(R.id.btnSubmitted);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnReset = findViewById(R.id.btnReset);
        btnSubmit.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnSubmitted.setOnClickListener(this);
        etMessage = findViewById(R.id.etMessage);
        progressBar = findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmitted:
                //checkPayment("2018-07-30 09:31:12");
                break;
            case R.id.btnSubmit:
                submitPayment();
                break;
            case R.id.btnReset:
                presenter.checkIsPaid(serviceId);
                progressBar.setVisibility(View.VISIBLE);
                btnSubmit.setEnabled(false);
                break;
        }
    }

    private void submitPayment() {
        if (payment) {
            presenter.paymentStep(serviceId, etMessage.getText().toString());
        } else {
            Toast.makeText(context, "Your payment not submitted yet.", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkPayment(String paidLog) {
        tvDateSubmitted.setText(paidLog);
        tvDateSubmitted.setVisibility(View.VISIBLE);
        tvPaymentSubmitted.setTextColor(Color.GREEN);
        tvDateSubmitted.setTextColor(Color.GREEN);
    }

    public void setPaymentResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    @Override
    public void onPayment(Data data) {
        progressBar.setVisibility(View.GONE);
        if (data.getSuccessMessage()) {
            this.payment = true;
            checkPayment(data.getPaidLog());
            btnSubmit.setEnabled(true);
            tvPaymentSubmitted.setText("Payment Submitted");
        } else {
            this.payment = false;
            tvPaymentSubmitted.setText("Payment Not Submitted");
        }
    }

    @Override
    public void onPaymentStep(Data data) {
        dismiss();
        mDialogResult.onPayment("yes");
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public interface OnMyDialogResult {
        void onPayment(String result);
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
        presenter.checkIsPaid(serviceId);
        progressBar.setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(false);
    }
}
