package id.tajima.tajima.menu.service.dialog.filter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.model.Branch;
import id.tajima.tajima.utility.AppPreferences;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class DialogFilter extends Dialog implements View.OnClickListener, BranchAdapter.OnMyDialogResult, RadioGroup.OnCheckedChangeListener, FilterView.View {
    private Context context;
    private AppPreferences preferences;
    private BranchAdapter adapter;
    private RecyclerView rvBranch;
    private FilterModel fm;
    private OnDialogFilter mDialogFilter;
    private RadioGroup rgPayment, rgStatus;
    private FilterView.Presenter presenter;
    EditText etStartDate, etEndDate;

    public DialogFilter(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_filter_service);
        preferences = new AppPreferences(context);
        presenter = new FilterPresenter(context, this);
        initView();
        fm = new FilterModel();
    }

    private void initView() {
        Button btnCancel = findViewById(R.id.btnCancel);
        Button btnFilter = findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        adapter = new BranchAdapter(context);
        adapter.setDialogResult(this);
        rvBranch = findViewById(R.id.rvBranch);
        rvBranch.setLayoutManager(new GridLayoutManager(context, 2));

        rgPayment = findViewById(R.id.rgPayment);
        rgStatus = findViewById(R.id.rgStatus);
        rgPayment.setOnCheckedChangeListener(this);
        rgStatus.setOnCheckedChangeListener(this);

        etStartDate = findViewById(R.id.etStartDate);
        etEndDate = findViewById(R.id.etEndDate);
        etStartDate.setOnClickListener(this);
        etEndDate.setOnClickListener(this);
        //getBranch
        presenter.getBranch();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnFilter:
                dateRange();
                mDialogFilter.filter(fm);
                dismiss();
                break;
            case R.id.etEndDate:
                DateUtil.defDatePickerShow(context, DateUtil.timeDefault(), DateUtil.datePickerLabel(etEndDate));
                break;
            case R.id.etStartDate:
                DateUtil.defDatePickerShow(context, DateUtil.timeDefault(), DateUtil.datePickerLabel(etStartDate));
                break;
        }
    }

    private void dateRange() {
        if (etEndDate.getText().toString().length() > 0) {
            fm.setDateEnd(etEndDate.getText().toString());
        }
        if (etStartDate.getText().toString().length() > 0) {
            fm.setDateStart(etStartDate.getText().toString());
        }
    }

    @Override
    public void setBranchId(String branchId) {
        fm.setBranchId(branchId);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton rb = findViewById(checkedId);
        switch (group.getId()) {
            case R.id.rgPayment:
                setPayment(rb.getText().toString().toLowerCase());
                break;
            case R.id.rgStatus:
                setStatus(rb.getText().toString().toLowerCase());
                break;
        }
    }

    private void setStatus(String s) {
        if (s.equals("in progress")) {
            fm.setStatus("13");
        } else {
            fm.setStatus("0");
        }
    }

    private void setPayment(String s) {
        if (s.equals("paid")) {
            fm.setPayment("1");
        } else {
            fm.setPayment("0");
        }
    }

    @Override
    public void onBranchExist(List<Branch> branch) {
        adapter.setBranchData(branch);
        rvBranch.setAdapter(adapter);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public interface OnDialogFilter {
        void filter(FilterModel fm);
    }

    public void setFilterResult(OnDialogFilter dialogFilter) {
        mDialogFilter = dialogFilter;
    }
}
