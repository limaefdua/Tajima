package id.tajima.tajima.menu.parts;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.custom.ClearEdit;
import id.tajima.tajima.menu.parts.model.Data;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarangFragment extends Fragment implements ClearEdit.SearchListener, BarangView.View, View.OnClickListener {
    BarangAdapter adapter;
    RecyclerView rv;
    LinearLayout lv;
    RelativeLayout progressBar;
    TextView warning, tvCount;
    ClearEdit searchEdit;
    BarangView.Presenter presenter;
    ImageView imgSort;
    int sort = 0;

    public BarangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_barang, container, false);
        presenter = new BarangPresenter(getActivity(), this);
        getActivity().setTitle("Parts");
        initView(v);

        adapter = new BarangAdapter(getContext());
        rv.setAdapter(adapter);
        progressBar.setVisibility(View.VISIBLE);
        presenter.showSparePart("asc");
        return v;
    }

    private void initView(View v) {
        searchEdit = v.findViewById(R.id.cePart);
        searchEdit.searchButton(this);
        rv = v.findViewById(R.id.recyclerView);
        lv = v.findViewById(R.id.layoutHeaderParts);
        warning = v.findViewById(R.id.noData);
        progressBar = v.findViewById(R.id.progressbarLayout);

        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        tvCount = v.findViewById(R.id.tvCount);

        imgSort = v.findViewById(R.id.imgSort);
        imgSort.setOnClickListener(this);

        //Zoomy.Builder builder = new Zoomy.Builder(getActivity()).target(rv);
        //builder.register();
    }

    @Override
    public void searchListener(String text) {
        if (text.length() > 0) {
            lv.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            presenter.searchSparePart(text);
        } else {
            Toast.makeText(getActivity(), "Please type something", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDataExist(Data data) {
        rv.setVisibility(View.VISIBLE);
        warning.setVisibility(View.GONE);
        tvCount.setText("Found : " + data.getSpareParts().size());
//        List<SparePart> dd = new ArrayList<>();
//        for (int i = 0; i < 100; i++) {
//            SparePart pr = new SparePart();
//            pr.setPartDesc("Aku");
//            pr.setPartId("12");
//            pr.setStockTotalNow("12");
//            pr.setStockPrice("1000");
//            dd.add(pr);
//        }
        //adapter.setDataPart(dd);
        adapter.setDataPart(data.getSpareParts());
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        tvCount.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String message) {
        rv.setVisibility(View.GONE);
        warning.setVisibility(View.VISIBLE);
        warning.setText(message);
        tvCount.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSort:
                changeSort();
                break;
        }
    }

    private void changeSort() {
        if (sort == 0) {
            sort = 1;
            imgSort.setImageResource(R.drawable.sort_descending);
            progressBar.setVisibility(View.VISIBLE);
            presenter.showSparePart("desc");
        } else {
            sort = 0;
            imgSort.setImageResource(R.drawable.sort_ascending);
            progressBar.setVisibility(View.VISIBLE);
            presenter.showSparePart("asc");
        }
    }
}
