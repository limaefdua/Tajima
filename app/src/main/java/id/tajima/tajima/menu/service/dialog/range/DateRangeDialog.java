package id.tajima.tajima.menu.service.dialog.range;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;

import id.tajima.tajima.R;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class DateRangeDialog extends Dialog implements View.OnClickListener {
    private Context context;

    public DateRangeDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_date_range);
    }

    @Override
    public void onClick(View v) {

    }
}
