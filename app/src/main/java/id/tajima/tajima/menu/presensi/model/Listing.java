package id.tajima.tajima.menu.presensi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Listing {
    @SerializedName("presensi_id")
    @Expose
    private String presensiId;
    @SerializedName("presensi_user_id")
    @Expose
    private String presensiUserId;
    @SerializedName("presensi_start")
    @Expose
    private String presensiStart;
    @SerializedName("presensi_end")
    @Expose
    private String presensiEnd;
    @SerializedName("presensi_updated_date")
    @Expose
    private Object presensiUpdatedDate;
    @SerializedName("presensi_updated_by")
    @Expose
    private Object presensiUpdatedBy;
    @SerializedName("presensi_deleted")
    @Expose
    private String presensiDeleted;
    @SerializedName("presensi_deleted_date")
    @Expose
    private Object presensiDeletedDate;
    @SerializedName("presensi_deleted_by")
    @Expose
    private Object presensiDeletedBy;
    @SerializedName("presensi_restored_date")
    @Expose
    private Object presensiRestoredDate;
    @SerializedName("presensi_restored_by")
    @Expose
    private Object presensiRestoredBy;
    @SerializedName("cron_at")
    @Expose
    private Object cronAt;
    @SerializedName("ijin_presensi_id")
    @Expose
    private Object ijinPresensiId;
    @SerializedName("status_ijin")
    @Expose
    private Object statusIjin;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_cabang_id")
    @Expose
    private String userCabangId;
    @SerializedName("user_jabatan_id")
    @Expose
    private String userJabatanId;
    @SerializedName("user_username")
    @Expose
    private String userUsername;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user_token")
    @Expose
    private String userToken;
    @SerializedName("user_last_login_at")
    @Expose
    private String userLastLoginAt;
    @SerializedName("user_created_by")
    @Expose
    private String userCreatedBy;
    @SerializedName("user_created_at")
    @Expose
    private String userCreatedAt;
    @SerializedName("user_updated_by")
    @Expose
    private String userUpdatedBy;
    @SerializedName("user_updated_at")
    @Expose
    private String userUpdatedAt;
    @SerializedName("user_status_delete")
    @Expose
    private String userStatusDelete;
    @SerializedName("user_deleted_by")
    @Expose
    private Object userDeletedBy;
    @SerializedName("user_deleted_at")
    @Expose
    private Object userDeletedAt;
    @SerializedName("user_restored_by")
    @Expose
    private Object userRestoredBy;
    @SerializedName("user_restored_at")
    @Expose
    private Object userRestoredAt;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("jabatan_id")
    @Expose
    private String jabatanId;
    @SerializedName("jabatan_name")
    @Expose
    private String jabatanName;
    @SerializedName("jabatan_desc")
    @Expose
    private String jabatanDesc;
    @SerializedName("jabatan_created_by")
    @Expose
    private String jabatanCreatedBy;
    @SerializedName("jabatan_created_at")
    @Expose
    private String jabatanCreatedAt;
    @SerializedName("jabatan_updated_by")
    @Expose
    private String jabatanUpdatedBy;
    @SerializedName("jabatan_updated_at")
    @Expose
    private String jabatanUpdatedAt;
    @SerializedName("jabatan_status_delete")
    @Expose
    private String jabatanStatusDelete;
    @SerializedName("jabatan_deleted_by")
    @Expose
    private Object jabatanDeletedBy;
    @SerializedName("jabatan_deleted_at")
    @Expose
    private Object jabatanDeletedAt;
    @SerializedName("jabatan_restored_by")
    @Expose
    private Object jabatanRestoredBy;
    @SerializedName("jabatan_restored_at")
    @Expose
    private Object jabatanRestoredAt;
    @SerializedName("cabang_id")
    @Expose
    private String cabangId;
    @SerializedName("cabang_code")
    @Expose
    private String cabangCode;
    @SerializedName("cabang_name")
    @Expose
    private String cabangName;
    @SerializedName("cabang_desc")
    @Expose
    private String cabangDesc;
    @SerializedName("cabang_address")
    @Expose
    private String cabangAddress;
    @SerializedName("cabang_status")
    @Expose
    private String cabangStatus;
    @SerializedName("cabang_created_by")
    @Expose
    private String cabangCreatedBy;
    @SerializedName("cabang_created_at")
    @Expose
    private String cabangCreatedAt;
    @SerializedName("cabang_updated_by")
    @Expose
    private String cabangUpdatedBy;
    @SerializedName("cabang_updated_at")
    @Expose
    private String cabangUpdatedAt;
    @SerializedName("cabang_status_delete")
    @Expose
    private String cabangStatusDelete;
    @SerializedName("cabang_deleted_by")
    @Expose
    private Object cabangDeletedBy;
    @SerializedName("cabang_deleted_at")
    @Expose
    private Object cabangDeletedAt;
    @SerializedName("cabang_restored_by")
    @Expose
    private Object cabangRestoredBy;
    @SerializedName("cabang_restored_at")
    @Expose
    private Object cabangRestoredAt;

    public String getPresensiId() {
        return presensiId;
    }

    public String getPresensiUserId() {
        return presensiUserId;
    }

    public String getPresensiStart() {
        return presensiStart;
    }

    public String getPresensiEnd() {
        return presensiEnd;
    }

    public Object getPresensiUpdatedDate() {
        return presensiUpdatedDate;
    }

    public Object getPresensiUpdatedBy() {
        return presensiUpdatedBy;
    }

    public String getPresensiDeleted() {
        return presensiDeleted;
    }

    public Object getPresensiDeletedDate() {
        return presensiDeletedDate;
    }

    public Object getPresensiDeletedBy() {
        return presensiDeletedBy;
    }

    public Object getPresensiRestoredDate() {
        return presensiRestoredDate;
    }

    public Object getPresensiRestoredBy() {
        return presensiRestoredBy;
    }

    public Object getCronAt() {
        return cronAt;
    }

    public Object getIjinPresensiId() {
        return ijinPresensiId;
    }

    public Object getStatusIjin() {
        return statusIjin;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserCabangId() {
        return userCabangId;
    }

    public String getUserJabatanId() {
        return userJabatanId;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public String getUserType() {
        return userType;
    }

    public String getUserToken() {
        return userToken;
    }

    public String getUserLastLoginAt() {
        return userLastLoginAt;
    }

    public String getUserCreatedBy() {
        return userCreatedBy;
    }

    public String getUserCreatedAt() {
        return userCreatedAt;
    }

    public String getUserUpdatedBy() {
        return userUpdatedBy;
    }

    public String getUserUpdatedAt() {
        return userUpdatedAt;
    }

    public String getUserStatusDelete() {
        return userStatusDelete;
    }

    public Object getUserDeletedBy() {
        return userDeletedBy;
    }

    public Object getUserDeletedAt() {
        return userDeletedAt;
    }

    public Object getUserRestoredBy() {
        return userRestoredBy;
    }

    public Object getUserRestoredAt() {
        return userRestoredAt;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getJabatanId() {
        return jabatanId;
    }

    public String getJabatanName() {
        return jabatanName;
    }

    public String getJabatanDesc() {
        return jabatanDesc;
    }

    public String getJabatanCreatedBy() {
        return jabatanCreatedBy;
    }

    public String getJabatanCreatedAt() {
        return jabatanCreatedAt;
    }

    public String getJabatanUpdatedBy() {
        return jabatanUpdatedBy;
    }

    public String getJabatanUpdatedAt() {
        return jabatanUpdatedAt;
    }

    public String getJabatanStatusDelete() {
        return jabatanStatusDelete;
    }

    public Object getJabatanDeletedBy() {
        return jabatanDeletedBy;
    }

    public Object getJabatanDeletedAt() {
        return jabatanDeletedAt;
    }

    public Object getJabatanRestoredBy() {
        return jabatanRestoredBy;
    }

    public Object getJabatanRestoredAt() {
        return jabatanRestoredAt;
    }

    public String getCabangId() {
        return cabangId;
    }

    public String getCabangCode() {
        return cabangCode;
    }

    public String getCabangName() {
        return cabangName;
    }

    public String getCabangDesc() {
        return cabangDesc;
    }

    public String getCabangAddress() {
        return cabangAddress;
    }

    public String getCabangStatus() {
        return cabangStatus;
    }

    public String getCabangCreatedBy() {
        return cabangCreatedBy;
    }

    public String getCabangCreatedAt() {
        return cabangCreatedAt;
    }

    public String getCabangUpdatedBy() {
        return cabangUpdatedBy;
    }

    public String getCabangUpdatedAt() {
        return cabangUpdatedAt;
    }

    public String getCabangStatusDelete() {
        return cabangStatusDelete;
    }

    public Object getCabangDeletedBy() {
        return cabangDeletedBy;
    }

    public Object getCabangDeletedAt() {
        return cabangDeletedAt;
    }

    public Object getCabangRestoredBy() {
        return cabangRestoredBy;
    }

    public Object getCabangRestoredAt() {
        return cabangRestoredAt;
    }

    public void setPresensiId(String presensiId) {
        this.presensiId = presensiId;
    }

    public void setPresensiStart(String presensiStart) {
        this.presensiStart = presensiStart;
    }

    public void setPresensiEnd(String presensiEnd) {
        this.presensiEnd = presensiEnd;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
