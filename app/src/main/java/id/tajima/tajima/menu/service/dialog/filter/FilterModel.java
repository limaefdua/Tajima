package id.tajima.tajima.menu.service.dialog.filter;

public class FilterModel {
    private String branchId;
    private String payment;
    private String status;
    private String dateStart;
    private String dateEnd;

    FilterModel() {
        this.branchId = "";
        this.payment = "";
        this.status = "";
        this.dateEnd = null;
        this.dateStart = null;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateStart() {
        return dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }
}
