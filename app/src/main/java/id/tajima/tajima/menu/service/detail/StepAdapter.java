package id.tajima.tajima.menu.service.detail;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.tajima.tajima.R;

/**
 * Created by Maftuhin on 25/10/2017.
 */

public class StepAdapter extends Adapter<StepAdapter.StepViewHolder> {
    private List<StepModel> data;
    private Context context;

    public StepAdapter(Context c) {
        this.context = c;
    }

    public void setStepData(List<StepModel> data) {
        this.data = data;
    }

    @Override
    public StepViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_step, parent, false);
        return new StepViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StepViewHolder holder, int position) {
        holder.tvId.setText(String.valueOf(position + 1));
        holder.tvStepName.setText(data.get(position).getStepName());
        holder.tvStepValue.setText(data.get(position).getStepValue());
        if (position % 2 == 0) {
            holder.lytStep.setBackgroundColor(Color.WHITE);
        } else {
            holder.lytStep.setBackgroundColor(Color.LTGRAY);
        }
        if (position > 9) {
            holder.tvId.setVisibility(View.INVISIBLE);
        } else {
            holder.tvId.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class StepViewHolder extends RecyclerView.ViewHolder {
        TextView tvId, tvStepName, tvStepValue;
        LinearLayout lytStep;

        StepViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tvId);
            tvStepName = view.findViewById(R.id.tvStepName);
            tvStepValue = view.findViewById(R.id.tvStepValue);
            lytStep = view.findViewById(R.id.lytStep);
        }
    }
}
