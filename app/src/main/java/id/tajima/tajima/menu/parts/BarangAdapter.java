package id.tajima.tajima.menu.parts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.parts.detail.PartDetail;
import id.tajima.tajima.menu.parts.model.SparePart;

/**
 * Created by Maftuhin on 23/10/2017.
 */

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.BarangViewHolder> {

    private Context c;
    private List<SparePart> data = new ArrayList<>();
    private SparseBooleanArray expandState = new SparseBooleanArray();

    BarangAdapter(Context c) {
        this.c = c;
    }

    void setDataPart(List<SparePart> data) {
        this.data = data;
        for (int i = 0; i < data.size(); i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_barang, null);

        return new BarangViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BarangViewHolder holder, int position) {
        final SparePart item = data.get(position);
        holder.tvNumber.setText(String.valueOf(position + 1));
        holder.tvNamaBrg.setText(item.getPartDesc());
        holder.tvIDBarang.setText(item.getPartId());
        holder.tvStokBrg.setText(item.getPartTotalStock());
        holder.tvIdr.setText(item.getPartDtrPrice());

        holder.layBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetailDialog(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class BarangViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layBarang;
        TextView tvStokBrg, tvIdr, tvNamaBrg, tvIDBarang, tvNumber;

        BarangViewHolder(View itemView) {
            super(itemView);

            layBarang = itemView.findViewById(R.id.layoutBarang);
            tvIDBarang = itemView.findViewById(R.id.tvIDBarang);
            tvNamaBrg = itemView.findViewById(R.id.tvNamaBarang);
            tvStokBrg = itemView.findViewById(R.id.tvStokBarang);
            tvIdr = itemView.findViewById(R.id.tvIdr);
            tvNumber = itemView.findViewById(R.id.tvNumber);
        }
    }

    private void showDetailDialog(SparePart data) {
        PartDetail pd = new PartDetail(c);
        pd.show();
        pd.setPartDetail(data);
    }
}