package id.tajima.tajima.menu.absent;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.absent.add.IjinActivity;
import id.tajima.tajima.menu.absent.model.Data;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbsentFragment extends Fragment implements AbsentView.View, View.OnClickListener {
    AbsentView.Presenter presenter;
    RelativeLayout progressBar;
    FloatingActionButton fabAbsent;
    RecyclerView rvAbsent;
    AbsentAdapter adapter;

    public AbsentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_absent, container, false);
        presenter = new AbsentPresenter(getActivity(), this);
        _initView(view);
        return view;
    }

    private void _initView(View view) {
        getActivity().setTitle("ABSENCE");
        progressBar = view.findViewById(R.id.progressbarLayout);
        fabAbsent = view.findViewById(R.id.fabAbsent);
        fabAbsent.setOnClickListener(this);
        rvAbsent = view.findViewById(R.id.rvAbsent);
        rvAbsent.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAbsent.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onDataExist(Data data) {
        adapter = new AbsentAdapter(getActivity(), data.getListing());
        rvAbsent.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
        fabAbsent.setEnabled(true);
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        fabAbsent.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabAbsent:
                startActivity(new Intent(getActivity(), IjinActivity.class));
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        fabAbsent.setEnabled(false);
        presenter.showAllAbsent();
    }
}
