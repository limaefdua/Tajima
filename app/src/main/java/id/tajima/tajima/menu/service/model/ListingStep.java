package id.tajima.tajima.menu.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListingStep {
    @SerializedName("serstat_status_id")
    @Expose
    private String serstatStatusId;
    @SerializedName("status_name")
    @Expose
    private String statusName;
    @SerializedName("logstep_id")
    @Expose
    private String logstepId;
    @SerializedName("logstep_service_id")
    @Expose
    private String logstepServiceId;
    @SerializedName("logstep_accept")
    @Expose
    private String logstepAccept;
    @SerializedName("logstep_1")
    @Expose
    private String logstep1;
    @SerializedName("logstep_2")
    @Expose
    private String logstep2;
    @SerializedName("logstep_3")
    @Expose
    private String logstep3;
    @SerializedName("logstep_4")
    @Expose
    private String logstep4;
    @SerializedName("logstep_5")
    @Expose
    private String logstep5;
    @SerializedName("logstep_6")
    @Expose
    private String logstep6;
    @SerializedName("logstep_7")
    @Expose
    private String logstep7;
    @SerializedName("logstep_8")
    @Expose
    private String logstep8;
    @SerializedName("logstep_9")
    @Expose
    private String logstep9;
    @SerializedName("logstep_message_payment")
    @Expose
    private String logstepMessagePayment;
    @SerializedName("logstep_message_return")
    @Expose
    private String logstepMessageReturn;
    @SerializedName("logstep_message_notready")
    @Expose
    private String logstepMessageNotready;
    @SerializedName("time_1")
    @Expose
    private String time1;
    @SerializedName("time_2")
    @Expose
    private Object time2;
    @SerializedName("time_3")
    @Expose
    private Object time3;
    @SerializedName("time_4")
    @Expose
    private String time4;
    @SerializedName("time_5")
    @Expose
    private String time5;
    @SerializedName("time_6")
    @Expose
    private String time6;
    @SerializedName("time_7")
    @Expose
    private String time7;
    @SerializedName("time_8")
    @Expose
    private String time8;
    @SerializedName("time_9")
    @Expose
    private Object time9;

    public String getLogstepId() {
        return logstepId;
    }

    public String getLogstepServiceId() {
        return logstepServiceId;
    }

    public String getLogstepAccept() {
        return logstepAccept;
    }

    public String getLogstep1() {
        return logstep1;
    }

    public String getLogstep2() {
        return logstep2;
    }

    public String getLogstep3() {
        return logstep3;
    }

    public String getLogstep4() {
        return logstep4;
    }

    public String getLogstep5() {
        return logstep5;
    }

    public String getLogstep6() {
        return logstep6;
    }

    public String getLogstep7() {
        return logstep7;
    }

    public String getLogstep8() {
        return logstep8;
    }

    public String getLogstep9() {
        return logstep9;
    }

    public String getLogstepMessagePayment() {
        return logstepMessagePayment;
    }

    public String getLogstepMessageReturn() {
        return logstepMessageReturn;
    }

    public String getLogstepMessageNotready() {
        return logstepMessageNotready;
    }

    public String getSerstatStatusId() {
        return serstatStatusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public String getTime1() {
        return time1;
    }

    public Object getTime2() {
        return time2;
    }

    public Object getTime3() {
        return time3;
    }

    public String getTime4() {
        return time4;
    }

    public String getTime5() {
        return time5;
    }

    public String getTime6() {
        return time6;
    }

    public String getTime7() {
        return time7;
    }

    public String getTime8() {
        return time8;
    }

    public Object getTime9() {
        return time9;
    }
}
