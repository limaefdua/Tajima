package id.tajima.tajima.menu.service.dialog.service;

import java.util.List;

import id.tajima.tajima.menu.service.step.model.LogStep;
import id.tajima.tajima.menu.service.step.model.McnData;

public interface ServiceDialogView {
    interface View {
        void onServiceComplete(LogStep logStep);

        void onServiceInComplete(LogStep logStep);

        void onError(String message);

        void onMcnExist(List<McnData> mcnData);

        void onAmountExist(int amount);
    }

    interface Presenter {
        void listMcn();

        void getServiceAmount(String serviceId);

        void serviceIncomplete(String serviceId, String reason, String continue_at);

        void serviceComplete(String serviceId, String garansi, boolean foc, String total_charge, String payment_method);
    }
}
