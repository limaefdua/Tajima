package id.tajima.tajima.menu.service.step.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class McnData {
    @SerializedName("mesin_id")
    @Expose
    private String mesinId;
    @SerializedName("mesin_code")
    @Expose
    private String mesinCode;
    @SerializedName("mesin_name")
    @Expose
    private String mesinName;
    @SerializedName("mesin_desc")
    @Expose
    private String mesinDesc;
    @SerializedName("mesin_active")
    @Expose
    private String mesinActive;
    @SerializedName("mesin_created_by")
    @Expose
    private String mesinCreatedBy;
    @SerializedName("mesin_created_at")
    @Expose
    private String mesinCreatedAt;
    @SerializedName("mesin_updated_by")
    @Expose
    private String mesinUpdatedBy;
    @SerializedName("mesin_updated_at")
    @Expose
    private String mesinUpdatedAt;
    @SerializedName("mesin_status_delete")
    @Expose
    private String mesinStatusDelete;
    @SerializedName("mesin_deleted_by")
    @Expose
    private Object mesinDeletedBy;
    @SerializedName("mesin_deleted_at")
    @Expose
    private Object mesinDeletedAt;
    @SerializedName("mesin_restored_by")
    @Expose
    private Object mesinRestoredBy;
    @SerializedName("mesin_restored_at")
    @Expose
    private Object mesinRestoredAt;

    public String getMesinId() {
        return mesinId;
    }

    public void setMesinId(String mesinId) {
        this.mesinId = mesinId;
    }

    public String getMesinCode() {
        return mesinCode;
    }

    public void setMesinCode(String mesinCode) {
        this.mesinCode = mesinCode;
    }

    public String getMesinName() {
        return mesinName;
    }

    public void setMesinName(String mesinName) {
        this.mesinName = mesinName;
    }

    public String getMesinDesc() {
        return mesinDesc;
    }

    public void setMesinDesc(String mesinDesc) {
        this.mesinDesc = mesinDesc;
    }

    public String getMesinActive() {
        return mesinActive;
    }

    public void setMesinActive(String mesinActive) {
        this.mesinActive = mesinActive;
    }

    public String getMesinCreatedBy() {
        return mesinCreatedBy;
    }

    public void setMesinCreatedBy(String mesinCreatedBy) {
        this.mesinCreatedBy = mesinCreatedBy;
    }

    public String getMesinCreatedAt() {
        return mesinCreatedAt;
    }

    public void setMesinCreatedAt(String mesinCreatedAt) {
        this.mesinCreatedAt = mesinCreatedAt;
    }

    public String getMesinUpdatedBy() {
        return mesinUpdatedBy;
    }

    public void setMesinUpdatedBy(String mesinUpdatedBy) {
        this.mesinUpdatedBy = mesinUpdatedBy;
    }

    public String getMesinUpdatedAt() {
        return mesinUpdatedAt;
    }

    public void setMesinUpdatedAt(String mesinUpdatedAt) {
        this.mesinUpdatedAt = mesinUpdatedAt;
    }

    public String getMesinStatusDelete() {
        return mesinStatusDelete;
    }

    public void setMesinStatusDelete(String mesinStatusDelete) {
        this.mesinStatusDelete = mesinStatusDelete;
    }

    public Object getMesinDeletedBy() {
        return mesinDeletedBy;
    }

    public void setMesinDeletedBy(Object mesinDeletedBy) {
        this.mesinDeletedBy = mesinDeletedBy;
    }

    public Object getMesinDeletedAt() {
        return mesinDeletedAt;
    }

    public void setMesinDeletedAt(Object mesinDeletedAt) {
        this.mesinDeletedAt = mesinDeletedAt;
    }

    public Object getMesinRestoredBy() {
        return mesinRestoredBy;
    }

    public void setMesinRestoredBy(Object mesinRestoredBy) {
        this.mesinRestoredBy = mesinRestoredBy;
    }

    public Object getMesinRestoredAt() {
        return mesinRestoredAt;
    }

    public void setMesinRestoredAt(Object mesinRestoredAt) {
        this.mesinRestoredAt = mesinRestoredAt;
    }
}
