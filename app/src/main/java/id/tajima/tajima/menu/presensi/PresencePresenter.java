package id.tajima.tajima.menu.presensi;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.menu.presensi.model.PresenceResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PresencePresenter implements PresenceView.Presenter {
    private Context context;
    private PresenceView.View view;
    private Api _api;
    private Subscription presence, checkIn, checkOut;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private AppPreferences preferences;

    PresencePresenter(Context context, PresenceView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }


    @Override
    public void showAllPresence() {
        presence = _api.getPresenceMine(preferences.getUserToken(), preferences.getUserId()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<PresenceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (presence.isUnsubscribed()) {
                            presence.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(PresenceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(presence);
    }

    @Override
    public void checkIn(String task) {
        checkIn = _api.checkIn(preferences.getUserToken(), preferences.getUserId(), task).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<PresenceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (checkIn.isUnsubscribed()) {
                            checkIn.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(PresenceResponse data) {
                        Log.w("log_response", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            view.onCheckIn(data.getData().getSuccessMessage());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(checkIn);
    }

    @Override
    public void checkOut() {
        checkOut = _api.checkOut(preferences.getUserToken(), preferences.getUserId()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<PresenceResponse>() {
                    @Override
                    public void onCompleted() {
                        if (checkOut.isUnsubscribed()) {
                            checkOut.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(PresenceResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onCheckOut(data.getData().getSuccessMessage());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(checkOut);
    }
}
