package id.tajima.tajima.menu.service.dialog.depart;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.custom.ClearEdit;
import id.tajima.tajima.menu.service.step.model.LogStep;
import id.tajima.tajima.utility.GpsUtils;
import lib.kingja.switchbutton.SwitchMultiButton;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class DepartDialog extends Dialog implements View.OnClickListener, DepartView.View {
    private Context context;
    private RecyclerView rvDriver;
    private DriverAdapter adapter;
    private Button btnCancel, btnSubmit;
    private LinearLayout lytCar, lytOther, lytFooter;
    private ClearEdit ceOther, ceCar;
    private OnMyDialogResult mDialogResult;
    private DepartView.Presenter presenter;
    private EditText clear_edit;
    private String driverId = "";
    private String serviceId;
    private ProgressBar progressBar;
    private SwitchMultiButton switchDepart;
    private String vehicle = "";

    public DepartDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_driver);
        adapter = new DriverAdapter(context);
        initView();
        presenter = new DepartPresenter(context, this);
    }

    private void initView() {
        final int lightGreen = Color.parseColor("#8bc34a");
        btnCancel = findViewById(R.id.btnRefuse);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        rvDriver = findViewById(R.id.rvDriver);
        rvDriver.setLayoutManager(new LinearLayoutManager(context));
        rvDriver.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        lytCar = findViewById(R.id.lytCar);
        lytOther = findViewById(R.id.lytOther);
        lytFooter = findViewById(R.id.lytFooter);
        ceOther = findViewById(R.id.ceOther);
        ceCar = findViewById(R.id.ceCar);
        ceOther.setButtonVisibility(false);
        ceCar.setButtonVisibility(false);
        ceCar.setHintText("Other");
        ceOther.setHintText("Other");
        progressBar = findViewById(R.id.progressBar);
        switchDepart = findViewById(R.id.switchDepart);
        switchDepart.setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
            @Override
            public void onSwitch(int position, String tabText) {
                vehicle = tabText;
                switch (position) {
                    case 0:
                        lytCar.setVisibility(View.VISIBLE);
                        lytOther.setVisibility(View.GONE);
                        lytFooter.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        lytFooter.setVisibility(View.VISIBLE);
                        lytCar.setVisibility(View.GONE);
                        lytOther.setVisibility(View.GONE);
                        break;
                    case 2:
                        lytOther.setVisibility(View.VISIBLE);
                        lytCar.setVisibility(View.GONE);
                        lytFooter.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
        switchDepart.setSelectedTab(0);

        clear_edit = findViewById(R.id.clear_edit);
        clear_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    rvDriver.setVisibility(View.GONE);
                } else {
                    rvDriver.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRefuse:
                presenter.getDriver(serviceId);
                break;
            case R.id.btnSubmit:
                submitDepart();
                break;
        }
    }

    private void submitDepart() {
        Boolean gps = GpsUtils.checkGpsVisibility(context);
        if (gps) {
            if (vehicle.equals("Car")) {
                if (driverId.equals("") && ceCar.getText().toString().equals("")) {
                    Toast.makeText(context, "Please select driver first, or type manually", Toast.LENGTH_SHORT).show();
                } else {
                    presenter.departStep(serviceId, vehicle.toLowerCase(), driverId);
                }
            } else {
                presenter.departStep(serviceId, vehicle.toLowerCase(), "");
            }
        } else {
            Toast.makeText(context, "Please Turn On Your GPS", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDriverExist(final List<DriverModel> driver) {
        adapter.setDriverData(driver);
        rvDriver.setAdapter(adapter);
        adapter.setDialogResult(new DriverAdapter.OnMyDialogResult() {
            @Override
            public void setDriverId(String idDriver) {
                driverId = idDriver;
            }
        });
    }

    @Override
    public void onDepartSuccess(LogStep logStep) {
        dismiss();
        mDialogResult.onDepart("yes", logStep.getLogstep3());
    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(false);
        btnCancel.setEnabled(false);
    }

    @Override
    public void doneLoading() {
        progressBar.setVisibility(View.GONE);
        btnSubmit.setEnabled(true);
        btnCancel.setEnabled(true);
    }

    public interface OnMyDialogResult {
        void onDepart(String result, String logStep);
    }

    public void setDepartResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
        presenter.getDriver(serviceId);
    }
}
