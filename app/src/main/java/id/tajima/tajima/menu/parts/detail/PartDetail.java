package id.tajima.tajima.menu.parts.detail;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.parts.model.SparePart;
import id.tajima.tajima.menu.parts.request.RequestDialog;
import id.tajima.tajima.utility.FormUtils;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class PartDetail extends Dialog implements View.OnClickListener {
    private Context context;
    private TextView tvPartNo, tvDescription, tvsj, tvsb, tvss, tvsx, tvSrpCod, tvRevisionNumber, tvLatestNo, tvDiscontinue;
    private TextView tvPartLocation, tvSrpPrice, tvMachines, tvRemark;
    private String ca = "J";
    private SparePart data;

    public PartDetail(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_detail_barang);
        initView();
    }

    public void setPartDetail(SparePart data) {
        this.data = data;
        tvPartNo.setText(data.getPartNo());
        tvRevisionNumber.setText(data.getPartRevisedNo());
        tvLatestNo.setText(data.getPartLatestNo());
        tvDescription.setText(data.getPartDesc());
        tvSrpPrice.setText(FormUtils.toRpString(data.getPartSrp()));
        tvSrpCod.setText(FormUtils.toRpString(data.getPartSrpCod()) + " (cash on delivery)");

        tvPartLocation.setText(data.getPartLocation());
        tvDiscontinue.setText(data.getPartDiscontinued());

        tvsj.setText(data.getPartStockJ());
        tvsb.setText(data.getPartStockB());
        tvss.setText(data.getPartStockS());
        tvsx.setText(data.getPartStockM());

        tvMachines.setText(data.getPartMesinCode());
        tvRemark.setText(data.getPartKeterangan());
    }

    private void initView() {
        tvRemark = findViewById(R.id.tvRemark);
        tvPartNo = findViewById(R.id.tvPartsNo);
        tvDescription = findViewById(R.id.tvPartDesc);
        tvsj = findViewById(R.id.tvStockJ);
        tvsb = findViewById(R.id.tvStockB);
        tvss = findViewById(R.id.tvStockS);
        tvsx = findViewById(R.id.tvStockX);
        tvSrpCod = findViewById(R.id.tvSRPCod);

        Button btnRequestJ = findViewById(R.id.btnRequestJ);
        Button btnRequestS = findViewById(R.id.btnRequestS);
        Button btnRequestB = findViewById(R.id.btnRequestB);
        Button btnRequestJT = findViewById(R.id.btnRequestJT);

        btnRequestJ.setOnClickListener(this);
        btnRequestS.setOnClickListener(this);
        btnRequestB.setOnClickListener(this);
        btnRequestJT.setOnClickListener(this);

        tvRevisionNumber = findViewById(R.id.tvPartsRevNo);
        tvLatestNo = findViewById(R.id.tvPartsLateNo);
        tvPartLocation = findViewById(R.id.tvPartsLocation);
        tvDiscontinue = findViewById(R.id.tvDscntnd);
        tvSrpPrice = findViewById(R.id.tvSRPPrice);
        tvMachines = findViewById(R.id.tvMachines);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRequestB:
                showRequestDialog(ca);
                break;
            case R.id.btnRequestJ:
                showRequestDialog(ca);
                break;
            case R.id.btnRequestJT:
                showRequestDialog(ca);
                break;
            case R.id.btnRequestS:
                showRequestDialog(ca);
                break;
        }
    }

    private void showRequestDialog(String ca) {
        dismiss();
        RequestDialog rd = new RequestDialog(context);
        rd.show();
        rd.setDataRequest(ca, data);
    }
}
