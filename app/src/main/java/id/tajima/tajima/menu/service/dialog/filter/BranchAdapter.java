package id.tajima.tajima.menu.service.dialog.filter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.model.Branch;

/**
 * Created by Maftuhin on 25/10/2017.
 */

public class BranchAdapter extends Adapter<BranchAdapter.DriverViewHolder> {
    private List<Branch> data = new ArrayList<>();
    private Context context;
    private int lastCheckedPosition = -1;
    private OnMyDialogResult mDialogResult;

    BranchAdapter(Context c) {
        this.context = c;
    }

    public void setBranchData(List<Branch> data) {
        this.data = data;
    }

    @Override
    public DriverViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_branch, parent, false);
        return new DriverViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DriverViewHolder holder, final int position) {
        holder.rbBranch.setText(data.get(position).getCabangName());
        holder.rbBranch.setChecked(position == lastCheckedPosition);
        holder.lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastCheckedPosition = position;
                notifyDataSetChanged();
                mDialogResult.setBranchId(data.get(position).getCabangId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class DriverViewHolder extends RecyclerView.ViewHolder {
        RadioButton rbBranch;
        RelativeLayout lyt;

        DriverViewHolder(View view) {
            super(view);
            rbBranch = view.findViewById(R.id.rbBranch);
            lyt = view.findViewById(R.id.lytBranch);
        }
    }

    public interface OnMyDialogResult {
        void setBranchId(String branchId);
    }

    public void setDialogResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }
}
