package id.tajima.tajima.menu.service.dialog.progress;

import java.util.List;

import id.tajima.tajima.menu.service.model.LogDay;

public interface DaysView {
    interface View {
        void onDataExist(List<LogDay> logDay);

        void onError(String message);
    }

    interface Presenter {
        void getLogDays(String serviceId);
    }
}
