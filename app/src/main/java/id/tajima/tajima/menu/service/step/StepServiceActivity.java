package id.tajima.tajima.menu.service.step;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.dialog.arrive.ArriveDialog;
import id.tajima.tajima.menu.service.dialog.depart.DepartDialog;
import id.tajima.tajima.menu.service.dialog.finish.FinishDialog;
import id.tajima.tajima.menu.service.dialog.ordering.OrderingDialog;
import id.tajima.tajima.menu.service.dialog.part_ready.PartReadyDialog;
import id.tajima.tajima.menu.service.dialog.part_return.ReturnDialog;
import id.tajima.tajima.menu.service.dialog.part_return_staff.PartReturnDialog;
import id.tajima.tajima.menu.service.dialog.payment.PaymentDialog;
import id.tajima.tajima.menu.service.dialog.service.ServiceDialog;
import id.tajima.tajima.menu.service.model.Data;
import id.tajima.tajima.menu.service.model.ListingStep;
import id.tajima.tajima.menu.service.model.ServiceRe;
import id.tajima.tajima.menu.service.step.model.Step;
import id.tajima.tajima.utility.AppPreferences;
import id.tajima.tajima.utility.DateUtil;
import id.tajima.tajima.utility.General;

public class StepServiceActivity extends AppCompatActivity implements LogStepAdapter.onStepClicked, OrderingDialog.OnMyDialogResult, DepartDialog.OnMyDialogResult, ArriveDialog.OnMyDialogResult, ServiceDialog.OnMyDialogResult, FinishDialog.OnMyDialogResult, PaymentDialog.OnMyDialogResult, ReturnDialog.OnMyDialogResult, ServiceDetailView.View {
    RecyclerView rvStep;
    LogStepAdapter adapter;
    List<Step> data = new ArrayList<>();
    Toolbar toolbar;
    ServiceDetailView.Presenter presenter;
    TextView tvCustomerName, tvEngineer, tvWorkTime, tvLogDay, tvDateService, tvTimeService, tvCustomerAddress;
    AppPreferences preferences;
    String serviceId;
    RelativeLayout lytProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_service);
        initView();
        presenter = new ServiceDetailPresenter(this, this);
        preferences = new AppPreferences(this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            presenter.setServiceData(b.getString("data_service"));
        }
    }

    private void initView() {
        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Service");

        tvWorkTime = findViewById(R.id.tvWorkTime);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvLogDay = findViewById(R.id.tvLogDay);
        tvEngineer = findViewById(R.id.tvEngineer);
        tvDateService = findViewById(R.id.tvDateService);
        tvTimeService = findViewById(R.id.tvTimeService);
        tvCustomerAddress = findViewById(R.id.tvCustomerAddress);
        lytProgress = findViewById(R.id.progressbarLayout);

        rvStep = findViewById(R.id.rvStep);
        rvStep.setLayoutManager(new LinearLayoutManager(this));
        adapter = new LogStepAdapter(this);
        adapter.setOnStepClicked(this);
        data.add(new Step("Part Ordering", "Part ordering", "", "", 1));
        data.add(new Step("Part Ready", "Part Ready", "", "", 1));
        data.add(new Step("Depart", "Depart", "", "", 1));
        data.add(new Step("Arrive", "Arrive", "", "", 1));
        data.add(new Step("Service", "Service", "", "", 1));
        data.add(new Step("Finish And Return", "Finish and Return", "", "", 1));
        data.add(new Step("Arrive (Office)", "Arrive at office", "", "", 1));
        data.add(new Step("Payment", "Payment", "", "", 1));
        data.add(new Step("Part Return", "Part Return", "", "", 1));
        adapter.setStepData(data);
        rvStep.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void clickStep(int step) {
        switch (step) {
            case 0:
                OrderingDialog order = new OrderingDialog(this);
                order.show();
                order.setServiceId(serviceId);
                order.setPartOrderingResult(this);
                break;
            case 1:
                partReady();
                break;
            case 2:
                DepartDialog depart = new DepartDialog(this);
                depart.show();
                depart.setDepartResult(this);
                depart.setServiceId(serviceId);
                break;
            case 3:
                ArriveDialog arrive = new ArriveDialog(this);
                arrive.show();
                arrive.setTitleText("4 Arrive", 4);
                arrive.setArriveResult(this);
                arrive.setServiceId(serviceId);
                break;
            case 4:
                ServiceDialog service = new ServiceDialog(this);
                service.show();
                service.setServiceResult(this);
                service.setServiceId(serviceId);
                break;
            case 5:
                FinishDialog finish = new FinishDialog(this);
                finish.show();
                finish.setFinishResult(this);
                finish.setServiceId(serviceId);
                break;
            case 6:
                ArriveDialog office = new ArriveDialog(this);
                office.show();
                office.setTitleText("7 Arrive (Office)", 7);
                office.setArriveResult(this);
                office.setServiceId(serviceId);
                break;
            case 7:
                PaymentDialog payment = new PaymentDialog(this);
                payment.show();
                payment.setPaymentResult(this);
                payment.setServiceId(serviceId);
                break;
            case 8:
                partReturn();
                break;
            default:
                break;
        }
    }

    private void partReady() {
        if (preferences.getUserRole().equals("part_staff")) {
            PartReadyDialog prd = new PartReadyDialog(this);
            prd.show();
            prd.setServiceId(serviceId);
        } else {
            presenter.checkPartReady(serviceId);
        }
    }

    private void partReturn() {
        if (preferences.getUserRole().equals("part_staff")) {
            PartReturnDialog prd = new PartReturnDialog(this);
            prd.show();
            prd.setServiceId(serviceId);
        } else {
            ReturnDialog partReturn = new ReturnDialog(this);
            partReturn.show();
            partReturn.setPartReturnResult(this);
            partReturn.setServiceId(serviceId);
        }
    }

    @Override
    public void partOrdering(String finishing, String logStep) {
        if (finishing.equals("yes")) {
            data.get(0).setStepStatus(3);
            data.get(1).setStepStatus(2);
        } else {
            data.get(0).setStepStatus(3);
            data.get(1).setStepStatus(3);
            data.get(2).setStepStatus(2);
        }
        data.get(0).setStepTime(DateUtil.getTime(logStep));
        data.get(0).setStepDate(DateUtil.getDate(logStep));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPartReady(String message, Boolean ready) {
        if (ready) {
            data.get(1).setStepStatus(3);
            data.get(2).setStepStatus(2);
            adapter.notifyDataSetChanged();
        } else {
            Toast.makeText(this, "Part Not Ready", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDepart(String result, String logStep) {
        data.get(2).setStepStatus(3);
        data.get(2).setStepDate(DateUtil.getDate(logStep));
        data.get(2).setStepTime(DateUtil.getTime(logStep));
        data.get(3).setStepStatus(2);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onArrived(String result, String log) {
        data.get(3).setStepStatus(3);
        data.get(3).setStepTime(DateUtil.getTime(log));
        data.get(3).setStepDate(DateUtil.getDate(log));
        data.get(4).setStepStatus(2);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onService(String result, String logStep) {
        data.get(4).setStepStatus(3);
        data.get(4).setStepTime(DateUtil.getTime(logStep));
        data.get(4).setStepDate(DateUtil.getDate(logStep));
        data.get(5).setStepStatus(2);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFinish(String result, String logStep) {
        data.get(5).setStepStatus(3);
        data.get(5).setStepTime(DateUtil.getTime(logStep));
        data.get(5).setStepDate(DateUtil.getDate(logStep));
        data.get(6).setStepStatus(2);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onArrivedOffice(String result, String logStep) {
        data.get(6).setStepStatus(3);
        data.get(6).setStepTime(DateUtil.getTime(logStep));
        data.get(6).setStepDate(DateUtil.getDate(logStep));
        data.get(7).setStepStatus(2);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPayment(String result) {
        data.get(7).setStepStatus(3);
        data.get(8).setStepStatus(2);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPartReturn(String result) {
        data.get(8).setStepStatus(3);
        adapter.notifyDataSetChanged();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onError(String message) {
        lytProgress.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataService(ServiceRe data) {
        tvCustomerName.setText(data.getCustomerName());
        tvWorkTime.setText(data.getWaktuKerja());
        tvEngineer.setText(data.getServenginerFullname());
        tvLogDay.setText("D" + (data.getLogDay() != null ? data.getLogDay() : "0"));
        tvCustomerAddress.setText(data.getCustomerLocation());
        tvTimeService.setText(DateUtil.getTime(data.getServiceCreatedAt()));
        tvDateService.setText(DateUtil.getDate(data.getServiceCreatedAt()));
        setTitle(data.getServiceCode());
//        if (data.getSerstatStatusId() != null) {
//            progressAt(Integer.parseInt(data.getSerstatStatusId()));
//        } else {
//            progressAt(13);
//        }
        this.serviceId = data.getServiceId();
        presenter.getLogStep(serviceId, data.getServiceCustomerId());
        lytProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLogStep(Data log) {
        lytProgress.setVisibility(View.GONE);
        if (log.getListingStep() != null) {
            ListingStep ls = log.getListingStep();
            data.get(0).setStepDate(DateUtil.getDate(ls.getLogstep1()));
            data.get(0).setStepTime(DateUtil.getTime(ls.getLogstep1()));

            data.get(1).setStepDate(DateUtil.getDate(ls.getLogstep2()));
            data.get(1).setStepTime(DateUtil.getTime(ls.getLogstep2()));

            data.get(2).setStepDate(DateUtil.getDate(ls.getLogstep3()));
            data.get(2).setStepTime(DateUtil.getTime(ls.getLogstep3()));

            data.get(3).setStepDate(DateUtil.getDate(ls.getLogstep4()));
            data.get(3).setStepTime(DateUtil.getTime(ls.getLogstep4()));

            data.get(4).setStepDate(DateUtil.getDate(ls.getLogstep5()));
            data.get(4).setStepTime(DateUtil.getTime(ls.getLogstep5()));

            data.get(5).setStepDate(DateUtil.getDate(ls.getLogstep6()));
            data.get(5).setStepTime(DateUtil.getTime(ls.getLogstep6()));

            data.get(6).setStepDate(DateUtil.getDate(ls.getLogstep7()));
            data.get(6).setStepTime(DateUtil.getTime(ls.getLogstep7()));

            data.get(7).setStepDate(DateUtil.getDate(ls.getLogstep8()));
            data.get(7).setStepTime(DateUtil.getTime(ls.getLogstep8()));

            data.get(8).setStepDate(DateUtil.getDate(ls.getLogstep9()));
            data.get(8).setStepTime(DateUtil.getTime(ls.getLogstep9()));

            if (log.getListingStep().getSerstatStatusId() != null) {
                progressAt(Integer.parseInt(log.getListingStep().getSerstatStatusId()));
            } else {
                progressAt(13);
            }
        } else {
            progressAt(13);
        }
    }

    private void progressAt(int at) {
        if (at == 13) {
            data.get(0).setStepStatus(2);
            adapter.notifyDataSetChanged();
            return;
        }
        int pa = General.progressAt(at) + 1;
        for (int i = 0; i < pa; i++) {
            data.get(i).setStepStatus(3);
        }
        if (pa < 9) {
            data.get(pa).setStepStatus(2);
        } else {
            Step complete = new Step();
            complete.setStepName("SERVICE COMPLETE");
            complete.setStepSubName("Service already completed!");
            complete.setStepStatus(4);
            data.add(complete);
        }
        adapter.notifyDataSetChanged();
    }
}
