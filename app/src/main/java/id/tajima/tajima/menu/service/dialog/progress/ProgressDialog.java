package id.tajima.tajima.menu.service.dialog.progress;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.model.LogDay;
import id.tajima.tajima.menu.service.model.ServiceRe;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class ProgressDialog extends Dialog implements View.OnClickListener, DaysView.View {
    private Context context;
    private RecyclerView rvDays;
    private DayAdapter adapter;
    private DaysView.Presenter presenter;
    private ProgressBar progressBar;
    private Button btnClose;
    private TextView tvCustomerName, tvWorkTIme, tvLogDay, tvLocation, tvEngineer, tvDateService, tvTimeService;
    private TextView tvWarning, tvDescription;

    public ProgressDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_progress);
        initView();
        presenter = new DaysPresenter(context, this);
    }

    private void initView() {
        tvDescription = findViewById(R.id.tvDescription);
        rvDays = findViewById(R.id.rvProgress);
        rvDays.setHasFixedSize(true);
        rvDays.setLayoutManager(new GridLayoutManager(context, 3));
        adapter = new DayAdapter(context);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvLogDay = findViewById(R.id.tvLogDay);
        tvWorkTIme = findViewById(R.id.tvWorkTime);
        tvLocation = findViewById(R.id.tvLocation);
        tvEngineer = findViewById(R.id.tvEngineer);
        tvDateService = findViewById(R.id.tvDateService);
        tvTimeService = findViewById(R.id.tvTimeService);
        progressBar = findViewById(R.id.progressBar);
        btnClose = findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        tvWarning = findViewById(R.id.tvWarning);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    @Override
    public void onDataExist(List<LogDay> logDay) {
        rvDays.setVisibility(View.VISIBLE);
        adapter.setLogDay(logDay);
        rvDays.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
        tvWarning.setVisibility(View.GONE);
        btnClose.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String message) {
        rvDays.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        btnClose.setVisibility(View.VISIBLE);
        tvWarning.setVisibility(View.VISIBLE);
        tvWarning.setText(message);
    }

    public void setDetail(ServiceRe ds) {
        progressBar.setVisibility(View.VISIBLE);
        presenter.getLogDays(ds.getServiceId());
        btnClose.setVisibility(View.GONE);
        tvCustomerName.setText(ds.getCustomerName());
        tvLogDay.setText("D" + (ds.getLogDay() != null ? ds.getLogDay() : "0"));
        tvWorkTIme.setText(ds.getWaktuKerja());
        tvLocation.setText(ds.getCustomerLocation());
        tvEngineer.setText(ds.getServenginerFullname());
        tvDateService.setText(DateUtil.getDate(ds.getServiceCreatedAt()));
        tvTimeService.setText(DateUtil.getTime(ds.getServiceCreatedAt()));
        adapter.setServiceId(ds.getServiceId());
        tvDescription.setText("description :\n" + ds.getServiceDesc());
    }
}
