package id.tajima.tajima.menu.service;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.notif.dialog.accept.AcceptDialog;
import id.tajima.tajima.menu.service.dialog.progress.ProgressDialog;
import id.tajima.tajima.menu.service.model.ServiceRe;
import id.tajima.tajima.menu.service.step.StepServiceActivity;
import id.tajima.tajima.utility.DateUtil;


public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceHistoryHolder> {

    private Context c;
    private List<ServiceRe> data;
    private onAcceptDialog mAccept;

    ServiceAdapter(Context c) {
        this.c = c;
    }

    void setServiceData(List<ServiceRe> data) {
        this.data = data;
    }

    @Override
    public ServiceHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service, parent, false);
        return new ServiceHistoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHistoryHolder holder, final int position) {
        ServiceRe item = data.get(position);
        holder.tvId.setText(data.get(position).getServiceId());
        holder.name.setText(item.getCustomerName());
        holder.tvWorkTime.setText(item.getWaktuKerja());
        holder.date.setText(DateUtil.getDate(item.getServiceCreatedAt()));
        holder.time.setText(DateUtil.getTime(item.getServiceCreatedAt()));
        holder.tvTechName.setText(item.getServenginerFullname());
        holder.tvLocation.setText(item.getCustomerLocation());
        holder.tvLogDay.setText("D" + (item.getLogDay() != null ? item.getLogDay() : "0"));
        holder.tvCode.setText(data.get(position).getServiceCode());
        if (item.getServiceStatusId() == null) {
            return;
        }
        switch (item.getServiceStatusId()) {
            case "13"://accept
                holder.imgBell.setVisibility(View.GONE);
                holder.layout.setBackgroundResource(R.color.light_green);
                holder.lytRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startToStep(data.get(position));
                    }
                });
                holder.lytLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog(data.get(position));
                    }
                });
                break;
            case "14"://denied
                holder.imgBell.setVisibility(View.GONE);
                holder.layout.setBackgroundResource(R.color.grey);
                holder.layout.setOnClickListener(null);
                holder.lytRight.setOnClickListener(null);
                holder.lytLeft.setOnClickListener(null);
                break;
            case "15"://complete
                holder.imgBell.setVisibility(View.GONE);
                holder.layout.setBackgroundResource(R.color.grey);
                holder.lytRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startToStep(data.get(position));
                    }
                });
                holder.lytLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog(data.get(position));
                    }
                });
                break;
            case "16"://incomplete
                holder.imgBell.setVisibility(View.VISIBLE);
                holder.layout.setBackgroundResource(R.color.dark_green);
                holder.lytRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startToStep(data.get(position));
                    }
                });
                holder.lytLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog(data.get(position));
                    }
                });
                break;
            case "2"://not accepted yet
                holder.imgBell.setVisibility(View.GONE);
                holder.layout.setBackgroundResource(R.color.violet);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogAccept(data.get(position));
                    }
                });
                break;
            case "1"://not accepted yet
                holder.imgBell.setVisibility(View.GONE);
                holder.layout.setBackgroundResource(R.color.violet);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogAccept(data.get(position));
                    }
                });
                break;
            default://default
                holder.imgBell.setVisibility(View.GONE);
                holder.layout.setBackgroundResource(R.color.light_green);
                holder.lytRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startToStep(data.get(position));
                    }
                });
                holder.lytLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        progressDialog(data.get(position));
                    }
                });
                break;
        }
    }

    private void startToStep(ServiceRe ds) {
        Intent in = new Intent(c, StepServiceActivity.class);
        in.putExtra("data_service", new Gson().toJson(ds));
        c.startActivity(in);
    }

    private void progressDialog(ServiceRe ds) {
        ProgressDialog pd = new ProgressDialog(c);
        pd.show();
        pd.setDetail(ds);
    }

    private void dialogAccept(ServiceRe data) {
        AcceptDialog acceptDialog = new AcceptDialog(c);
        acceptDialog.show();
        acceptDialog.setService(data);
        acceptDialog.onAcceptDialog(new AcceptDialog.onAcceptDialog() {
            @Override
            public void setAccept(Boolean acc) {
                mAccept.setAccept(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ServiceHistoryHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout;
        TextView name, tvId;
        TextView date, tvLogDay, tvCode;
        TextView time, tvWorkTime, tvTechName, tvLocation;
        LinearLayout lytLeft, lytRight;
        ImageView imgBell;

        ServiceHistoryHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tvId);
            layout = view.findViewById(R.id.layoutHistoryService);
            name = view.findViewById(R.id.tvNamaCustomer);
            date = view.findViewById(R.id.tvTanggalRequest);
            time = view.findViewById(R.id.tvJamRequest);
            lytLeft = view.findViewById(R.id.lytLeft);
            lytRight = view.findViewById(R.id.lytRight);
            tvWorkTime = view.findViewById(R.id.tvWorkTime);
            tvTechName = view.findViewById(R.id.tvTechName);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvLogDay = view.findViewById(R.id.tvLogDay);
            imgBell = view.findViewById(R.id.imgBell);
            tvCode = view.findViewById(R.id.tvCode);
        }
    }

    public interface onAcceptDialog {
        void setAccept(Boolean acc);
    }

    void onAcceptDialog(onAcceptDialog acceptDialog) {
        mAccept = acceptDialog;
    }
}
