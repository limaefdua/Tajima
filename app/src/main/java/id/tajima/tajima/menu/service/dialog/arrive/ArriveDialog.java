package id.tajima.tajima.menu.service.dialog.arrive;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.step.model.LogStep;
import id.tajima.tajima.utility.GpsUtils;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class ArriveDialog extends Dialog implements View.OnClickListener, ArriveView.View, Listener {
    private Context context;
    private LinearLayout introArrive, takeImage;
    private Button btnSubmit, btnClaim;
    private ImageView ivTakePhoto;
    //private ServiceActivity serviceActivity;
    private static final int CAMERA_REQUEST = 1888;
    private OnMyDialogResult mDialogResult;
    private Handler mHandler;
    private ArriveView.Presenter presenter;
    private ProgressBar progressBar;
    private TextView tvTitle;
    private int step;
    private EasyWayLocation easyWayLocation;
    private Double latitude, longitude;
    private String serviceId;

    public ArriveDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_arrive);
        initView();
        presenter = new ArrivePresenter(context, this);
        easyWayLocation = new EasyWayLocation(context);
        easyWayLocation.setListener(this);
    }

    private void initView() {
        introArrive = findViewById(R.id.introArrive);
        takeImage = findViewById(R.id.takeImage);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        ivTakePhoto = findViewById(R.id.ivTakePhotos);
        ivTakePhoto.setOnClickListener(this);
        btnClaim = findViewById(R.id.btnClaim);
        btnClaim.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        tvTitle = findViewById(R.id.tvTitle);

        mHandler = new Handler();
        m_Runnable.run();
    }

    private void checkGpsOnOff() {
        if (GpsUtils.checkGpsVisibility(context)) {
            btnSubmit.setEnabled(true);
        } else {
            btnSubmit.setEnabled(false);
        }
    }


    private final Runnable m_Runnable = new Runnable() {
        public void run() {
            mHandler.postDelayed(m_Runnable, 4000);
            checkGpsOnOff();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                submitArrive();
                break;
            case R.id.ivTakePhotos:
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //serviceActivity.startActivityForResult(cameraIntent, CAMERA_REQUEST);
                break;
            case R.id.btnClaim:
//                mDialogResult.finishing("yes");
//                dismiss();
                break;
        }
    }

    private void submitArrive() {
        Double dist = GpsUtils.getJarak(-7.7565049, 110.3932727, latitude, longitude);
        if (dist < 0.3) {
            progressBar.setVisibility(View.VISIBLE);
            btnSubmit.setEnabled(false);
            if (step == 4) {
                presenter.arriveStep(serviceId);
            } else {
                presenter.arriveAtOffice(serviceId);
            }
        } else {
            Toast.makeText(context, "Jarak masih belum dalam jangka toleransi!", Toast.LENGTH_SHORT).show();
        }
    }

//    public void setForResult(ServiceActivity serviceActivity) {
//        //this.serviceActivity = serviceActivity;
//    }

    public void setArriveResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }

    public void setImage(Bitmap photo) {
        ivTakePhoto.setImageBitmap(photo);
    }

    @Override
    public void onArrive(LogStep logStep) {
        progressBar.setVisibility(View.GONE);
        btnSubmit.setEnabled(true);
        mDialogResult.onArrived("yes", logStep.getLogstep4());
        dismiss();
    }

    @Override
    public void onArriveAtOffice(LogStep logStep) {
        progressBar.setVisibility(View.GONE);
        btnSubmit.setEnabled(true);
        mDialogResult.onArrivedOffice("yes", logStep.getLogstep7());
        dismiss();
    }

    @Override
    public void onError(String message) {
        btnSubmit.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
        latitude = easyWayLocation.getLatitude();
        longitude = easyWayLocation.getLongitude();
    }

    @Override
    public void onPositionChanged() {
        latitude = easyWayLocation.getLatitude();
        longitude = easyWayLocation.getLongitude();
    }

    @Override
    public void locationCancelled() {

    }

    public interface OnMyDialogResult {
        void onArrived(String result, String logStep);

        void onArrivedOffice(String result, String logStep);
    }

    public void setTitleText(String title, int step) {
        tvTitle.setText(title);
        this.step = step;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}

