package id.tajima.tajima.menu.service.dialog.service.model;

public class Reasons {
    private Boolean bTimeNotEnough;
    private Boolean bAssistanceNeeded;
    private Boolean bPartUncarried;
    private Boolean bPartNoStock;
    private String sTimeNotEnough;
    private String sAssistanceNeeded;
    private String sPartUncarried;
    private String sPartNoStock;


    public Reasons() {
        this.sTimeNotEnough = "";
        this.sAssistanceNeeded = "";
        this.sPartUncarried = "";
        this.sPartNoStock = "";
        this.bTimeNotEnough = false;
        this.bAssistanceNeeded = false;
        this.bPartUncarried = false;
        this.bPartNoStock = false;
    }

    public Boolean getbTimeNotEnough() {
        return bTimeNotEnough;
    }

    public void setbTimeNotEnough(Boolean bTimeNotEnough) {
        this.bTimeNotEnough = bTimeNotEnough;
    }

    public Boolean getbAssistanceNeeded() {
        return bAssistanceNeeded;
    }

    public void setbAssistanceNeeded(Boolean bAssistanceNeeded) {
        this.bAssistanceNeeded = bAssistanceNeeded;
    }

    public Boolean getbPartUncarried() {
        return bPartUncarried;
    }

    public void setbPartUncarried(Boolean bPartUncarried) {
        this.bPartUncarried = bPartUncarried;
    }

    public Boolean getbPartNoStock() {
        return bPartNoStock;
    }

    public void setbPartNoStock(Boolean bPartNoStock) {
        this.bPartNoStock = bPartNoStock;
    }

    public String getsTimeNotEnough() {
        return sTimeNotEnough;
    }

    public void setsTimeNotEnough(String sTimeNotEnough) {
        this.sTimeNotEnough = sTimeNotEnough;
    }

    public String getsAssistanceNeeded() {
        return sAssistanceNeeded;
    }

    public void setsAssistanceNeeded(String sAssistanceNeeded) {
        this.sAssistanceNeeded = sAssistanceNeeded;
    }

    public String getsPartUncarried() {
        return sPartUncarried;
    }

    public void setsPartUncarried(String sPartUncarried) {
        this.sPartUncarried = sPartUncarried;
    }

    public String getsPartNoStock() {
        return sPartNoStock;
    }

    public void setsPartNoStock(String sPartNoStock) {
        this.sPartNoStock = sPartNoStock;
    }
}
