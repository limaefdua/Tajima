package id.tajima.tajima.menu.service.dialog.depart;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;

/**
 * Created by Maftuhin on 25/10/2017.
 */

public class DriverAdapter extends Adapter<DriverAdapter.DriverViewHolder> {
    private List<DriverModel> data = new ArrayList<>();
    private Context context;
    private int selected_position = 999;
    private OnMyDialogResult mDialogResult;

    public DriverAdapter(Context c) {
        this.context = c;
    }

    public void setDriverData(List<DriverModel> data) {
        this.data = data;
    }

    @Override
    public DriverViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_driver, parent, false);
        return new DriverViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DriverViewHolder holder, final int position) {
        holder.tvDriver.setText(data.get(position).getNamaTeknisi().toUpperCase());
        holder.itemView.setBackgroundColor(selected_position == position ? Color.parseColor("#8bc34a") : Color.TRANSPARENT);
        holder.tvDriver.setTextColor(selected_position == position ? Color.WHITE : Color.BLACK);
        holder.lytDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.lytDriver.setSelected(true);
                notifyItemChanged(selected_position);
                selected_position = position;
                notifyItemChanged(selected_position);
                mDialogResult.setDriverId(data.get(position).getServenginerId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class DriverViewHolder extends RecyclerView.ViewHolder {
        TextView tvDriver;
        LinearLayout lytDriver;

        public DriverViewHolder(View view) {
            super(view);
            tvDriver = view.findViewById(R.id.tvDriver);
            lytDriver = view.findViewById(R.id.lytDriver);
        }
    }

    public interface OnMyDialogResult {
        void setDriverId(String driverId);
    }

    public void setDialogResult(OnMyDialogResult dialogResult) {
        mDialogResult = dialogResult;
    }
}
