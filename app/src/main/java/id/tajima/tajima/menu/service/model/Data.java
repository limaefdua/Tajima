package id.tajima.tajima.menu.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("service_res")
    @Expose
    private List<ServiceRe> service = null;
    @SerializedName("listing_step")
    @Expose
    private ListingStep listingStep;

    @SerializedName("log_day")
    @Expose
    private List<LogDay> logDay = null;
    @SerializedName("daily_duration")
    @Expose
    private DailyDuration dailyDuration;
    @SerializedName("status_service")
    @Expose
    private String statusService;
    @SerializedName("remarks")
    @Expose
    private String remarks;

    @SerializedName("branch")
    @Expose
    private List<Branch> branch = null;

    public List<Branch> getBranch() {
        return branch;
    }

    public void setBranch(List<Branch> branch) {
        this.branch = branch;
    }

    public List<LogDay> getLogDay() {
        return logDay;
    }

    public void setLogDay(List<LogDay> logDay) {
        this.logDay = logDay;
    }

    public List<ServiceRe> getService() {
        return service;
    }

    public DailyDuration getDailyDuration() {
        return dailyDuration;
    }

    public String getStatusService() {
        return statusService;
    }

    public String getRemarks() {
        return remarks;
    }

    public ListingStep getListingStep() {
        return listingStep;
    }
}
