package id.tajima.tajima.custom;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import id.tajima.tajima.R;


/**
 * Created by maftuhin on 26/10/15.
 */
public class ClearEdit extends RelativeLayout implements View.OnClickListener {
    LayoutInflater inflater = null;
    EditText edit_text;
    ImageView btn_clear;
    ImageView btnSearch;
    SearchListener mSearch;

    public ClearEdit(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews();
    }

    public ClearEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();

    }

    public ClearEdit(Context context) {
        super(context);
        initViews();
    }

    void initViews() {
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.clearable_edit, this, true);
        }
        edit_text = findViewById(R.id.clear_edit);
        btn_clear = findViewById(R.id.bClear);
        btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);
        btn_clear.setVisibility(RelativeLayout.INVISIBLE);
        clearText();
        showHideClearButton();
    }

    void clearText() {
        btn_clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_text.setText("");
            }
        });
    }

    public void clearTextQuery() {
        edit_text.setText("");
    }

    void showHideClearButton() {
        edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    btn_clear.setVisibility(RelativeLayout.VISIBLE);
                else
                    btn_clear.setVisibility(RelativeLayout.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setHintText(String text) {
        edit_text.setHint(text);
    }

    public void setButtonVisibility(Boolean visibility) {
        if (visibility.equals(false)) {
            btnSearch.setVisibility(GONE);
        }
    }

    public Editable getText() {
        return edit_text.getText();
    }

    public void searchButton(SearchListener mSearch) {
        this.mSearch = mSearch;
    }

    public interface SearchListener {
        void searchListener(String text);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearch:
                mSearch.searchListener(edit_text.getText().toString());
                break;
        }
    }

    public void setError(String message){
        edit_text.setError(message);
    }

}
