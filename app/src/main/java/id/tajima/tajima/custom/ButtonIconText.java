package id.tajima.tajima.custom;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import id.tajima.tajima.R;


/**
 * Created by maftuhin on 26/10/15.
 */
public class ButtonIconText extends LinearLayout {
    LayoutInflater inflater = null;
    TextView buttonText;
    LinearLayout lytIconText;

    public ButtonIconText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews();
    }

    public ButtonIconText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();

    }

    public ButtonIconText(Context context) {
        super(context);
        initViews();
    }

    void initViews() {
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.button_icon_text, this, true);
        buttonText = findViewById(R.id.buttonText);
        lytIconText = findViewById(R.id.lytIconText);
    }

    public void setTextButton(String text) {
        buttonText.setText(text);
    }

    public void setButtonPressed(Boolean press) {
        if (press) {
            buttonText.setTextColor(Color.WHITE);
        } else {
            buttonText.setTextColor(Color.LTGRAY);
        }
    }
}
