package id.tajima.tajima.notif.dialog.accept;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.notif.dialog.accept.model.AcceptResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AcceptPresenter implements AcceptView.Presenter {
    private Context context;
    private AcceptView.View view;
    private AppPreferences preferences;
    private Api _api;
    private Subscription accept, refuse;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    AcceptPresenter(Context context, AcceptView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void acceptService(String serviceId) {
        view.onLoading();
        accept = _api.acceptService(preferences.getUserToken(), preferences.getUserId(), "13", serviceId).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<AcceptResponse>() {
                    @Override
                    public void onCompleted() {
                        if (accept.isUnsubscribed()) {
                            accept.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.doneLoading();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(AcceptResponse data) {
                        view.doneLoading();
                        if (data.getStatus().equals(200)) {
                            view.onAccepted();
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(accept);
    }

    @Override
    public void refuseService(String serviceId, String reason) {
        view.onLoading();
        refuse = _api.refuseService(preferences.getUserToken(), preferences.getUserId(), serviceId, reason).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<AcceptResponse>() {
                    @Override
                    public void onCompleted() {
                        if (refuse.isUnsubscribed()) {
                            refuse.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.doneLoading();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(AcceptResponse data) {
                        view.doneLoading();
                        if (data.getStatus().equals(200)) {
                            view.onRefused();
                        }else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        compositeSubscription.add(refuse);
    }
}
