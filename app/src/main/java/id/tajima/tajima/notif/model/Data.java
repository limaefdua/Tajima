package id.tajima.tajima.notif.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("notif")
    @Expose
    private List<Notif> notif = null;

    public List<Notif> getNotif() {
        return notif;
    }

    public void setNotif(List<Notif> notif) {
        this.notif = notif;
    }
}
