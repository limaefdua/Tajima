package id.tajima.tajima.notif;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.tajima.tajima.R;
import id.tajima.tajima.notif.dialog.accept.AcceptDialog;
import id.tajima.tajima.notif.model.Notif;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by Maftuhin on 19/10/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    public List<Notif> data = new ArrayList<>();
    private Context context;
    private OnNotificationClicked mNotification;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitle;
        TextView tvDate, tvNumber, tvTime, tvReceiver, tvSender;
        LinearLayout lyCont;
        ImageView imgBell;

        public ViewHolder(View v) {
            super(v);
            tvDate = v.findViewById(R.id.notifDate);
            tvTime = v.findViewById(R.id.notifTime);
            tvTitle = v.findViewById(R.id.tvTitle);
            tvNumber = v.findViewById(R.id.tvNumber);
            lyCont = v.findViewById(R.id.lyCont);
            imgBell = v.findViewById(R.id.imgBell);
            tvReceiver = v.findViewById(R.id.tvReceiver);
            tvSender = v.findViewById(R.id.tvSender);
        }
    }

    NotificationAdapter(Context context) {
        this.context = context;
    }

    void setNotificationData(List<Notif> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notif, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, int position) {
        final Notif item = data.get(position);
        switch (item.getNotifandroStatus()) {
            case "draf":
                holder.lyCont.setBackgroundColor(Color.parseColor("#ffd5b8"));
                holder.imgBell.setVisibility(View.VISIBLE);
                break;
            case "read":
                holder.lyCont.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.imgBell.setVisibility(View.INVISIBLE);
                break;
            case "empty":
                break;
        }
        holder.tvNumber.setText(String.valueOf(position + 1));
        holder.tvTitle.setText(item.getNotifandroText());
        holder.tvDate.setText(DateUtil.getDate(item.getNotifandroCreatedAt()));
        holder.tvTime.setText(DateUtil.getTime(item.getNotifandroCreatedAt()));
        holder.tvReceiver.append(data.get(position).getUserReceiver());
        holder.tvSender.append(data.get(position).getUserSender());
        if (item.getNotifandroClickable().equals("1")) {
            holder.lyCont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AcceptDialog ad = new AcceptDialog(context);
                    ad.show();
                    ad.setServiceId(data.get(holder.getAdapterPosition()).getNotifandroServiceId());
                    ad.onAcceptDialog(new AcceptDialog.onAcceptDialog() {
                        @Override
                        public void setAccept(Boolean acc) {
                            data.get(holder.getAdapterPosition()).setNotifandroStatus("read");
                            mNotification.setNotificationId(data.get(holder.getAdapterPosition()).getNotifandroId());
                        }
                    });
                }
            });
        } else {
            holder.lyCont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.get(holder.getAdapterPosition()).setNotifandroStatus("read");
                    mNotification.setNotificationId(data.get(holder.getAdapterPosition()).getNotifandroId());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnNotificationClicked {
        void setNotificationId(String notificationId);
    }

    void onNotificationClick(OnNotificationClicked notificationClicked) {
        mNotification = notificationClicked;
    }
}
