package id.tajima.tajima.notif.dialog.accept;

public class AcceptView {
    interface View {
        void onAccepted();

        void onError(String message);

        void onRefused();

        void onLoading();

        void doneLoading();
    }

    interface Presenter {
        void acceptService(String serviceId);

        void refuseService(String serviceId, String reason);
    }
}
