package id.tajima.tajima.notif;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.notif.model.Data;

public class NotificationActivity extends AppCompatActivity implements NotificationView.View, NotificationAdapter.OnNotificationClicked {
    Toolbar toolbar;
    RecyclerView rec;
    NotificationAdapter adapter;
    NotificationView.Presenter presenter;
    RelativeLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);
        _initView();
        progressBar.setVisibility(View.VISIBLE);
        presenter.showNotification();
    }

    private void _initView() {
        toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter = new NotificationAdapter(this);
        presenter = new NotificationPresenter(this, this);

        rec = findViewById(R.id.recycler);
        rec.setLayoutManager(new LinearLayoutManager(this));
        rec.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        progressBar = findViewById(R.id.progressbarLayout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDataExist(Data data) {
        progressBar.setVisibility(View.GONE);
        adapter.setNotificationData(data.getNotif());
        adapter.onNotificationClick(this);
        rec.setAdapter(adapter);
    }

    @Override
    public void onError(String message) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNotificationRead() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setNotificationId(String notificationId) {
        presenter.readNotification(notificationId);
        adapter.notifyDataSetChanged();
    }
}
