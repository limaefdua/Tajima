package id.tajima.tajima.notif;

import id.tajima.tajima.notif.model.Data;

public interface NotificationView {
    interface View {
        void onDataExist(Data data);

        void onError(String message);

        void onNotificationRead();
    }

    interface Presenter {
        void showNotification();

        void readNotification(String notificationId);
    }
}
