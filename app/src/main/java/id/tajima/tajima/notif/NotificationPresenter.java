package id.tajima.tajima.notif;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import id.tajima.tajima.application.TajimaApp;
import id.tajima.tajima.application.api.Api;
import id.tajima.tajima.notif.model.NotificationResponse;
import id.tajima.tajima.utility.AppPreferences;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class NotificationPresenter implements NotificationView.Presenter {
    private Context context;
    private NotificationView.View view;
    private CompositeSubscription cs = new CompositeSubscription();
    private Subscription notification, read;
    private Api _api;
    private AppPreferences preferences;

    NotificationPresenter(Context context, NotificationView.View view) {
        this.context = context;
        this.view = view;
        _api = TajimaApp.createService(Api.class);
        preferences = new AppPreferences(context);
    }

    @Override
    public void showNotification() {
        notification = _api.getNotification(preferences.getUserToken(), preferences.getUserId()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<NotificationResponse>() {
                    @Override
                    public void onCompleted() {
                        if (notification.isUnsubscribed()) {
                            notification.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        e.getCause();
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(NotificationResponse data) {
                        if (data.getStatus().equals(200)) {
                            view.onDataExist(data.getData());
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        cs.add(notification);
    }

    @Override
    public void readNotification(String notificationId) {
        read = _api.readNotification(preferences.getUserToken(), notificationId).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).
                subscribe(new Observer<NotificationResponse>() {
                    @Override
                    public void onCompleted() {
                        if (read.isUnsubscribed()) {
                            read.unsubscribe();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w("log_response_error", e.getMessage());
                        view.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(NotificationResponse data) {
                        Log.w("log_response", new Gson().toJson(data));
                        if (data.getStatus().equals(200)) {
                            view.onNotificationRead();
                        } else {
                            view.onError(data.getMessage());
                        }
                    }
                });
        cs.add(read);
    }
}
