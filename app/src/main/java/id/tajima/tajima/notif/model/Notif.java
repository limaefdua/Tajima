package id.tajima.tajima.notif.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notif {
    @SerializedName("notifandro_id")
    @Expose
    private String notifandroId;
    @SerializedName("notifandro_service_id")
    @Expose
    private String notifandroServiceId;
    @SerializedName("notifandro_sender_user_id")
    @Expose
    private String notifandroSenderUserId;
    @SerializedName("notifandro_receiver_user_id")
    @Expose
    private String notifandroReceiverUserId;
    @SerializedName("notifandro_text")
    @Expose
    private String notifandroText;
    @SerializedName("notifandro_clickable")
    @Expose
    private String notifandroClickable;
    @SerializedName("notifandro_status")
    @Expose
    private String notifandroStatus;
    @SerializedName("notifandro_created_at")
    @Expose
    private String notifandroCreatedAt;
    @SerializedName("notifandro_updated_at")
    @Expose
    private String notifandroUpdatedAt;
    @SerializedName("user_receiver")
    @Expose
    private String userReceiver;
    @SerializedName("user_sender")
    @Expose
    private String userSender;

    public String getNotifandroId() {
        return notifandroId;
    }

    public void setNotifandroId(String notifandroId) {
        this.notifandroId = notifandroId;
    }

    public String getNotifandroServiceId() {
        return notifandroServiceId;
    }

    public void setNotifandroServiceId(String notifandroServiceId) {
        this.notifandroServiceId = notifandroServiceId;
    }

    public String getNotifandroSenderUserId() {
        return notifandroSenderUserId;
    }

    public void setNotifandroSenderUserId(String notifandroSenderUserId) {
        this.notifandroSenderUserId = notifandroSenderUserId;
    }

    public String getNotifandroReceiverUserId() {
        return notifandroReceiverUserId;
    }

    public void setNotifandroReceiverUserId(String notifandroReceiverUserId) {
        this.notifandroReceiverUserId = notifandroReceiverUserId;
    }

    public String getNotifandroText() {
        return notifandroText;
    }

    public void setNotifandroText(String notifandroText) {
        this.notifandroText = notifandroText;
    }

    public String getNotifandroClickable() {
        return notifandroClickable;
    }

    public void setNotifandroClickable(String notifandroClickable) {
        this.notifandroClickable = notifandroClickable;
    }

    public String getNotifandroStatus() {
        return notifandroStatus;
    }

    public void setNotifandroStatus(String notifandroStatus) {
        this.notifandroStatus = notifandroStatus;
    }

    public String getNotifandroCreatedAt() {
        return notifandroCreatedAt;
    }

    public void setNotifandroCreatedAt(String notifandroCreatedAt) {
        this.notifandroCreatedAt = notifandroCreatedAt;
    }

    public String getNotifandroUpdatedAt() {
        return notifandroUpdatedAt;
    }

    public void setNotifandroUpdatedAt(String notifandroUpdatedAt) {
        this.notifandroUpdatedAt = notifandroUpdatedAt;
    }

    public String getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(String userReceiver) {
        this.userReceiver = userReceiver;
    }

    public String getUserSender() {
        return userSender;
    }

    public void setUserSender(String userSender) {
        this.userSender = userSender;
    }

}
