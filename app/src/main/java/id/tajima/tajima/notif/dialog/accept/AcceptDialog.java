package id.tajima.tajima.notif.dialog.accept;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import id.tajima.tajima.R;
import id.tajima.tajima.menu.service.model.ServiceRe;
import id.tajima.tajima.utility.DateUtil;

/**
 * Created by maftuhin on 17/04/18. 3:47
 */
public class AcceptDialog extends Dialog implements View.OnClickListener, AcceptView.View {
    private Context context;
    private EditText etOnGoing, etOther;
    private RadioGroup rbRefuse;
    private RadioButton rbSick, rbOther, rbOnGoing, rbIncompetent;
    private Button btnRefuse, btnAccept, btnSubmit;
    private LinearLayout lytRefuse;
    private AcceptView.Presenter presenter;
    private String reason, serviceId;
    private onAcceptDialog mAccept;
    private ProgressBar progressBar;
    TextView tvDescription;
    private ServiceRe service;

    public AcceptDialog(@NonNull Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_accept);
        initView();
    }

    private void initView() {
        presenter = new AcceptPresenter(context, this);
        tvDescription = findViewById(R.id.tvDescription);
        etOnGoing = findViewById(R.id.onGoingAt);
        etOther = findViewById(R.id.etOtherReason);
        rbRefuse = findViewById(R.id.rbRefuse);

        rbSick = findViewById(R.id.rbSick);
        rbIncompetent = findViewById(R.id.rbIncompetent);
        rbOnGoing = findViewById(R.id.rbOnGoing);
        rbOther = findViewById(R.id.rbOther);

        btnRefuse = findViewById(R.id.btnRefuse);
        btnAccept = findViewById(R.id.btnAccept);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        btnRefuse.setOnClickListener(this);
        btnAccept.setOnClickListener(this);

        progressBar = findViewById(R.id.progressBar);

        lytRefuse = findViewById(R.id.lytRefuse);

        rbRefuse.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()) {
                    case R.id.rbOther:
                        etOther.setVisibility(View.VISIBLE);
                        etOnGoing.setVisibility(View.GONE);
                        break;
                    case R.id.rbOnGoing:
                        etOther.setVisibility(View.GONE);
                        etOnGoing.setVisibility(View.VISIBLE);
                        break;
                    case R.id.rbSick:
                        etOther.setVisibility(View.GONE);
                        etOnGoing.setVisibility(View.GONE);
                        break;
                    case R.id.rbIncompetent:
                        etOther.setVisibility(View.GONE);
                        etOnGoing.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRefuse:
                lytRefuse.setVisibility(View.VISIBLE);
                break;
            case R.id.btnAccept:
                presenter.acceptService(serviceId);
                lytRefuse.setVisibility(View.GONE);
                mAccept.setAccept(true);
                break;
            case R.id.btnSubmit:
                submitReason();
                break;
        }
    }

    private void submitReason() {
        switch (rbRefuse.getCheckedRadioButtonId()) {
            case R.id.rbSick:
                reason = rbSick.getText().toString();
                break;
            case R.id.rbIncompetent:
                reason = rbIncompetent.getText().toString();
                break;
            case R.id.rbOnGoing:
                reason = rbOnGoing.getText().toString() + " " + etOnGoing.getText().toString();
                break;
            case R.id.rbOther:
                reason = etOther.getText().toString();
                break;
        }
        presenter.refuseService(serviceId, reason);
        mAccept.setAccept(true);
    }

    @Override
    public void onAccepted() {
        dismiss();
        mAccept.setAccept(true);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefused() {
        mAccept.setAccept(false);
        dismiss();
    }

    @Override
    public void onLoading() {
        progressBar.setVisibility(View.VISIBLE);
        btnAccept.setEnabled(false);
        btnRefuse.setEnabled(false);
    }

    @Override
    public void doneLoading() {
        progressBar.setVisibility(View.GONE);
        btnAccept.setEnabled(true);
        btnRefuse.setEnabled(true);
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public void setService(ServiceRe service) {
        this.serviceId = service.getServiceId();
        this.service = service;
        String text = "<p><b>" + service.getCustomerName() + "</b><br>" + service.getServenginerFullname() + "</p>";
        String date = "<p>" + service.getCustomerLocation() + "<br>" + DateUtil.getDate(service.getServiceCreatedAt())+" ("+DateUtil.getTime(service.getServiceCreatedAt()) +")</p>";
        String desc = "<p><b>Description:</b>" + service.getServiceDesc() + "</p>";
        tvDescription.setText(fromHtml(text + date + desc));
    }

    public interface onAcceptDialog {
        void setAccept(Boolean acc);
    }

    public void onAcceptDialog(onAcceptDialog acceptDialog) {
        mAccept = acceptDialog;
    }

    @SuppressWarnings("deprecation")
    private Spanned fromHtml(String s) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(s, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(s);
        }
    }
}
