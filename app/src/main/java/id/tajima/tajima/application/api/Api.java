package id.tajima.tajima.application.api;

import id.tajima.tajima.menu.absent.model.AbsentResponse;
import id.tajima.tajima.dashboard.model.MainResponse;
import id.tajima.tajima.intro.login.model.LoginResponse;
import id.tajima.tajima.notif.dialog.accept.model.AcceptResponse;
import id.tajima.tajima.notif.model.NotificationResponse;
import id.tajima.tajima.menu.parts.model.PartResponse;
import id.tajima.tajima.menu.presensi.model.PresenceResponse;
import id.tajima.tajima.intro.register.model.RegisterResponse;
import id.tajima.tajima.menu.service.step.model.ServiceDetailResponse;
import id.tajima.tajima.menu.service.model.ServiceResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

public interface Api {

    @GET("presensi/get_all_presensi")
    Observable<PresenceResponse> getAllPresence(
            @Header("Authorization") String token
    );

    @GET("service/getBranch")
    Observable<ServiceResponse> getBranch(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("auth/login")
    Observable<LoginResponse> login(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("auth/setToken")
    Observable<LoginResponse> setToken(
            @Header("Authorization") String auth,
            @Field("user_id") String user_id,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("auth/register")
    Observable<RegisterResponse> register(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("presensi/get_mind")
    Observable<PresenceResponse> getPresenceMine(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("presensi/IamCheckin")
    Observable<PresenceResponse> checkIn(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("task") String task
    );

    @FormUrlEncoded
    @POST("presensi/IamCheckout")
    Observable<PresenceResponse> checkOut(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("ijin/add")
    Observable<AbsentResponse> addAbsent(
            @Header("Authorization") String token,
            @Field("user") String user_id,
            @Field("start") String start,
            @Field("end") String end,
            @Field("alasan") String reason,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST("ijin/get_mind")
    Observable<AbsentResponse> showAllAbsent(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("service/get_ServiceByEnginer")
    Observable<ServiceResponse> serviceByEngineer(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    @GET("service/get_all_services")
    Observable<ServiceResponse> getAllService(
            @Header("Authorization") String token
    );

    @GET("service/listMcn")
    Observable<ServiceDetailResponse> listMcn(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("service/search")
    Observable<ServiceResponse> serviceSearch(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("search") String customer_name,
            @Field("sortby") String sortBy,
            @Field("sort") String sort
    );

    @FormUrlEncoded
    @POST("service/filter")
    Observable<ServiceResponse> filterService(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("branch_id") String branchId,
            @Field("payment") String payment,
            @Field("status") String status,
            @Field("start_date") String startDate,
            @Field("end_date") String endDate
    );

    @FormUrlEncoded
    @POST("service/get_detail")
    Observable<ServiceResponse> getLogDays(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/getDetailServiceTimes")
    Observable<ServiceResponse> detailLogDays(
            @Header("Authorization") String token,
            @Field("service_id") String customer_name,
            @Field("date") String date
    );

    @FormUrlEncoded
    @POST("service/searchAsPart")
    Observable<ServiceResponse> serviceSearchAsPart(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("search") String query
    );

    @FormUrlEncoded
    @POST("service/part_ordering")
    Observable<ServiceDetailResponse> orderPart(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("service_id") String service_id,
            @Field("ordering") String ordering
    );

    @FormUrlEncoded
    @POST("service/check_part_ready")
    Observable<ServiceDetailResponse> checkPartReady(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/get_driver")
    Observable<ServiceDetailResponse> getDriver(
            @Header("Authorization") String token,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/depart")
    Observable<ServiceDetailResponse> departStep(
            @Header("Authorization") String token,
            @Field("vehicle") String vehicle,
            @Field("service_id") String serviceId,
            @Field("driver_id") String driverId
    );

    @FormUrlEncoded
    @POST("service/arrived")
    Observable<ServiceDetailResponse> arriveStep(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/finishAndReturn")
    Observable<ServiceDetailResponse> finishAndReturn(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("vehicle") String vehicle,
            @Field("service_id") String serviceId,
            @Field("driver_id") String driverId
    );

    @FormUrlEncoded
    @POST("service/arriveAtOffice")
    Observable<ServiceDetailResponse> arriveAtOffice(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/payment")
    Observable<ServiceDetailResponse> paymentStep(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId,
            @Field("message") String message
    );

    @FormUrlEncoded
    @POST("service/checkPayment")
    Observable<ServiceDetailResponse> checkPayment(
            @Header("Authorization") String token,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/checkPartReturn")
    Observable<ServiceDetailResponse> checkPartReturn(
            @Header("Authorization") String token,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/parts")
    Observable<ServiceDetailResponse> partReturnStep(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/accpartReturn")
    Observable<ServiceDetailResponse> partReturnStaff(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/accPartIsReady")
    Observable<ServiceDetailResponse> accPartIsReady(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId,
            @Field("isReady") Boolean isReady,
            @Field("message") String message
    );

    @FormUrlEncoded
    @POST("service/servicing")
    Observable<ServiceDetailResponse> servicingComplete(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId,
            @Field("iscomplete") boolean isComplete,
            @Field("mcn") String garansi,
            @Field("foc") boolean foc,
            @Field("total_charge") String total_charge,
            @Field("payment_method") String payment_method
    );

    @FormUrlEncoded
    @POST("service/servicing")
    Observable<ServiceDetailResponse> serviceIncomplete(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId,
            @Field("iscomplete") boolean isComplete,
            @Field("reason") String reason,
            @Field("continue_at") String continue_at
    );

    @FormUrlEncoded
    @POST("service/get_service_amount")
    Observable<ServiceDetailResponse> getServiceAmount(
            @Header("Authorization") String token,
            @Field("service_id") String serviceId
    );

    //Spare Part
    @FormUrlEncoded
    @POST("spareparts/get_spareparts")
    Observable<PartResponse> showAllSparePart(
            @Header("Authorization") String token,
            @Field("sort") String sort
    );

    @FormUrlEncoded
    @POST("spareparts/search")
    Observable<PartResponse> searchSparepart(
            @Header("Authorization") String token,
            @Field("search") String sort
    );

    @FormUrlEncoded
    @POST("spareparts/request")
    Observable<PartResponse> requestSparePart(
            @Header("Authorization") String token,
            @Field("qty") String qty,
            @Field("customer_name") String customer,
            @Field("desc") String desc,
            @Field("req_to") String req_to,
            @Field("req_from") String req_from,
            @Field("part_id") String part_id
    );

    //Notification
    @FormUrlEncoded
    @POST("notifikasi/get_notif")
    Observable<NotificationResponse> getNotification(
            @Header("Authorization") String token,
            @Field("user_id") String userId
    );

    @FormUrlEncoded
    @POST("notifikasi/onclick")
    Observable<NotificationResponse> readNotification(
            @Header("Authorization") String token,
            @Field("notif_id") String notifId
    );

    @FormUrlEncoded
    @POST("notifikasi/notif_count")
    Observable<MainResponse> countNotification(
            @Header("Authorization") String token,
            @Field("user_id") String userId
    );

    @FormUrlEncoded
    @POST("service/Accept")
    Observable<AcceptResponse> acceptService(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("accept") String accept,
            @Field("service_id") String serviceId
    );

    @FormUrlEncoded
    @POST("service/Denied")
    Observable<AcceptResponse> refuseService(
            @Header("Authorization") String token,
            @Field("user_id") String userId,
            @Field("service_id") String serviceId,
            @Field("reason") String reason
    );

    @FormUrlEncoded
    @POST("service/get_step")
    Observable<ServiceResponse> getLogStep(
            @Header("Authorization") String token,
            @Field("customer_id") String userId,
            @Field("service_id") String serviceId
    );
}
