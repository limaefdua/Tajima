package id.tajima.tajima.application.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Map;

import id.tajima.tajima.R;
import id.tajima.tajima.intro.login.LoginActivity;

/**
 * Created by maftuhin on 30/09/17. 10:21
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String CHANNEL_NAME = "FCM";
    private static final String CHANNEL_DESC = "Firebase Cloud Messaging";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        sendNotification(data, remoteMessage.getNotification());
    }

    private void sendNotification(Map<String, String> data, RemoteMessage.Notification notification) {
        int requestId = (int) System.currentTimeMillis();
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder;
        Log.w("data", new Gson().toJson(data));

        Intent intent = new Intent(this, LoginActivity.class);
//        intent.putExtra("label", data.get("label"));
//        intent.putExtra("data", data.get("data"));
//        if (data.isEmpty()) {
//            body = notification.getBody();
//            title = notification.getTitle();
//        } else {
//            body = data.get("body");
//            title = data.get("title");
//            intent.putExtra("title", title);
//            intent.putExtra("body", body);
//        }

        PendingIntent pendingIntent = PendingIntent
                .getActivity(this, requestId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setSound(soundUri)
                .setContentIntent(pendingIntent)
                .setLights(Color.BLUE, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription(CHANNEL_DESC);
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.BLUE);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(requestId, mBuilder.build());
    }
}
